#include <iostream>

using namespace std;

int main(void) {
	int n;
	while (cin >> n) {
		long long bound = 9;
		int inBound = false;
		while (bound < n) {
			if (bound < n && n <= bound * 2) 
				inBound = true;
			bound *= 2 * 9;
		}

		cout << (inBound ? "Ollie wins." : "Stan wins.") << '\n';
	}
}
