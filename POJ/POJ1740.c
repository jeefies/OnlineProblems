#include <stdio.h>

int arr[101];

int main(void) {
	int n;
	while (~scanf("%d", &n) && n) {
		for (int i = 0; i <= 100; ++i) 
			arr[i] = 0;

		for (int x, i = 0; i < n; ++i) {
			scanf("%d", &x);
			++arr[x];
		}

		int x = 0;
		for (int i = 0; i <= 100; ++i) {
			x |= arr[i] & 1;
		}
		putchar('0' + x), putchar('\n');
	}
}

