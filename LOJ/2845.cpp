#include <iostream>
#include <algorithm>
#include <deque>
#include <cmath>

using namespace std;
const int N = 2e5;
typedef long long lint;

// BLO stands for block length, n is the points' count
int BLO, n;

struct P {
	int x, y;
	bool operator < (const P &p) {
		return x < p.x;
	}
} p[N];

int apps[N << 2], apc = 0;
void discrete() {
	static auto gt = greater<int>();
	
	for (int i = 1; i <= n; ++i) {
		apps[i] = p[i].y;
	}
	
	sort(apps + 1, apps + 1 + n, gt);
	int *ae = unique(apps + 1, apps + 1 + n);
	
	for (int i = 1; i <= n; ++i) {
		p[i].y = lower_bound(apps + 1, ae, p[i].y, gt) - apps;
	}
	
	apc = ae - apps - 1;
	BLO = sqrt(apc);
}

void read() {
	cin >> n;
	for (int x, y, i = 1; i <= n; ++i) {
		cin >> x >> y;
		p[i] = {x, y};
	}
	
	sort(p + 1, p + 1 + n);
}

// save the sum of one point
int s[N];
class BLOCK {
private:
	int l, r, tag;
	deque<int> Q; // for using slope optimize
public:
	void set(int l, int r) {
		this->l = l, this->r = r;
	}

	// rebuild this block.
	// with many points that can build a convex tull
	// which can help use maintain the max value.
	inline lint build() {
		lint ans = 0;

		// we calc every point of the BLOCK, to get the answer for whole block after rebuilding.
		for (int i = l; i <= r; ++i) {
			ans = max(ans, 1ll * (s[i] += tag) * apps[i]);
		} tag = 0;

		Q.clear();
		int siz = 0;
		for (int i = r; i >= l; --i) {
#define Y(i) (1ll * apps[i] * (s[i] + tag))
#define X(i) (1ll * apps[i])
			// build reversely, for using the convex hull to right.
			// now add point (X(i), Y(i)), We should make sure slope(i) >= slope(Q[last])
			// Consider Q[siz -1] as j, Q[siz - 2] as k
			// So we need 
			// ( Y(i) - Y(j) ) / ( X(i) - X(j) ) >= ( Y(j) - Y(k) ) / ( X(j) - X(k) )
			// as the followed line
			while (siz > 1 && ( Y(i) - Y(Q[siz -1]) ) * ( X(Q[siz - 1]) - X(Q[siz - 2]) ) >= ( Y(Q[siz - 1]) - Y(Q[siz - 2]) ) * ( X(i) - X(Q[siz - 1]) ))
				Q.pop_back(), --siz;
			Q.push_back(i), ++siz;
		}
		return ans;
	}

	lint add() {
		++tag;
		while (Q.size() > 1 && Y(Q[0]) <= Y(Q[1]))
			Q.pop_front();
		return Y(Q[0]);
	}
} blocks[700];

lint cans = 0;
// now we consider why the complexity is correct?
// we using accounting method to analyze it.
// First, every time we rebuild a block, we cost sqrt(n) to build
// and we build a deque, which at most have sqrt(n) items
// This cost 2*sqrt(n) (half prepayed for adding tag).
// so when we are adding tag, we cost nothing.
// Second, when querying, we use O(1) however.
// In total, we have n times add, which makes complexity to O(n \sqrt n)
inline void add(int y) {
	int b = y / BLO;
	int r = min((b + 1) * BLO, apc + 1); // without r

	// update one block's s and need rebuild!!
	for (int i = y; i < r; ++i) {
		++s[i];
	}

	cans = max(cans, blocks[b].build());
	for (int i = b + 1, ie = apc / BLO; i <= ie; ++i) {
		cans = max(cans, blocks[i].add());
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	read();
	discrete();
	for (int i = 0, l = 0, r = BLO - 1; l <= apc; ++i, l += BLO, r += BLO) {
		blocks[i].set(max(l, 1), min(r, apc));
		blocks[i].build();
	}

	lint res = 0;
	for (int i = 1; i <= n + 1; ++i) {
		res = max(res, 1ll * (n - i + 1) * p[i].x + cans); // update ans for (n + 1) times !!!
		if (i <= n) add(p[i].y);
	}

	cout << res << '\n';
	return 0;
}
