#include <iostream>
#include <algorithm>

using namespace std;
const int N = 5e4 + 3, BLO = 512;
typedef long long lint;

lint a[N], tag[N], but[N];
int lab[N], st[N], ed[N];

void modify(int l, int r, int c) {
	if (lab[l] == lab[r]) {
		for (int i = l; i <= r; ++i)
			a[i] += c;
		return;
	}

	for (int i = l, ie = ed[lab[l]]; i <= ie; ++i) {
		a[i] += c;
	}

	for (int i = st[lab[r]]; i <= r; ++i) {
		a[i] += c;
	}

	for (int i = lab[l] + 1; i < lab[r]; ++i)
		tag[i] += c;
}

void initBut(int n) {
	for (int b = 0, be = n / BLO; b <= be; ++b) {
		st[b] = b * BLO, ed[b] = min((b + 1) * BLO - 1, n);
		for (int i = st[b]; i <= ed[b]; ++i)
			lab[i] = b;
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	}

	initBut(n);
	int op, l, r, c;
	for (int i = 1; i <= n; ++i) {
		cin >> op >> l >> r >> c;
		if (op == 1) {
			cout << a[r] + tag[lab[r]] << '\n';
		} else {
			modify(l, r, c);
		}
	}
}
