#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>

using namespace std;
const int C = 302, K = 1e5;
typedef long long lint;

int n, k, t;
std::vector<long long> w[C];

lint tmp[3][K], *f = tmp[0], *g = tmp[1], *fr = tmp[2];

int cc;
void solve(int i, int j, int pl, int pr) {
	if (i > j) return;

	lint mid = (i + j) >> 1, fp = 0, fv = g[mid];
	for (lint p = pl; p <= pr && mid - p * cc >= 0; ++p) {
		if (fv < g[mid - p * cc] + w[cc][p]) {
			fv = g[mid - p * cc] + w[cc][p], fp = p;
		}
	} f[mid] = fv;
	// printf("solve in [%d, %d] (mid %lld) p [%d, %d] got %lld\n", i, j, mid, pl, pr, fp);
	solve(i, mid - 1, pl, fp); solve(mid + 1, j, fp, pr);
}

int main() {
	freopen("jewelry1.in", "r", stdin);
	freopen("jewelry.out", "w", stdout);
	freopen("jewelry.log", "w", stderr);
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> k;
	for (int c, v, i = 0; i ^ n; ++i) {
		cin >> c >> v; t = max(t, c);
		w[c].push_back((lint)v);
	}

	for (int i = 1; i <= t; ++i) {
		if (w[i].empty()) continue;
		sort(w[i].begin(), w[i].end());
		w[i].push_back(0), reverse(w[i].begin(), w[i].end());
		for (int s = 1, se = w[i].size(); s < se; ++s)
			w[i][s] += w[i][s - 1];
	}

	for (int i = 1; i <= t; ++i) {
		if (w[i].empty()) continue;
		cc = i, swap(f, g);
		for (int v = 1; v <= k; ++v) {
			lint cf = g[v], fp = 0;
			for (int p = 0; p < w[i].size(); ++p)
				if (v - p * i >= 0 && g[v - p * i] + w[i][p] >= cf) {
					cf = g[v - p * i] + w[i][p], fp = p;
				}
			f[v] = cf;
			// cout << cf << "(" << fp << ")" << " \n"[v == k];
			clog << fp << " \n"[v == k];
		}
	}

	for (int i = 1; i <= k; ++i) cout << f[i] << " \n"[i == k];
	return 0;
}
