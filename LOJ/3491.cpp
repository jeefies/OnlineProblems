#include <iostream>
#include <set>
#include <algorithm>

using namespace std;
const int N = 3e5 + 7;
typedef long long lint;

struct XY {
    lint x, y;
    bool operator < (const XY &xy) const {
        return x < xy.x;
    }
} xy[N];

lint dis(int i, int j) {
    return max(abs(xy[i].x - xy[j].x), abs(xy[i].y - xy[j].y));
}

int n, k;
int cnt;

lint val[N];
int check(lint m) {
    multiset<XY> s;
    cnt = 0;

    for (int i = 0, j = 0; i < n; ++i) {
        while (j < i && (lint)(xy[i].x - xy[j].x) > m) {
            s.erase(s.find({xy[j].y, j}));
            ++j;
        }

        auto itl = s.lower_bound({xy[i].y - m, 0});
        auto itr = s.upper_bound({xy[i].y + m, 0});

        for (; itl != itr; ++itl) {
            XY c = *itl;
            val[cnt++] = dis(i, c.y);

            if (cnt >= k)
                return cnt;
        }

        s.insert({xy[i].y, i});
    }

    return cnt;
}

int main() {
    cin.tie(0)->sync_with_stdio(false);

    cin >> n >> k;

    for (lint x, y, i = 0; i < n; ++i) {
        cin >> x >> y;
        xy[i] = {x - y, x + y};
    }

    sort(xy, xy + n);

    lint len = 0;

    for (lint w = 1ll << 35; w; w >>= 1) {
        if (check(len + w) < k)
            len += w;
    }

    check(len);

    sort(val, val + cnt);

    for (int i = 0; i < cnt; ++i) {
        cout << val[i] << '\n';
    }

    for (int i = cnt; i < k; ++i) {
        cout << len + 1 << '\n';
    }
}
