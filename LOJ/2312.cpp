#include <iostream>
#include <vector>
#include <algorithm>
#include <bitset>
#include <string>

using namespace std;
const int N = 600, M = 1500, L = 1001;

typedef bitset<L> bit;
class LinearBasis {
private:
	int time[L];
	bit xxj[L];
public:
	void insert(bit x, int lastTime) noexcept {
		for (int i = L - 1; ~i; --i) {
			if (x[i]) {
				if (time[i] < lastTime) {
					swap(time[i], lastTime);
					swap(x, xxj[i]);
				}
				if (lastTime == 0) break;
				x ^= xxj[i];
			}
		}
	}

	bit qmax(int tim) noexcept {
		bit x;
		for (int i = L - 1; ~i; --i) {
			if (!x[i] && tim < time[i])
				x ^= xxj[i];
		}
		return x;
	}
} lb;

void read(bit &x) noexcept {
	static string s; cin >> s;
	x = bit(s);
}

void output(const bit &x) noexcept {
	for (int flag = 0, i = L - 1; ~i; --i) {
		if (flag && !x[i]) cout << '0';
		else if (x[i]) flag = 1, cout << '1';
	}
}

struct Edge {
	bit w;
	int to;
}; vector<Edge> G[N];

bit vis, dis[N];
void dfs(int x) noexcept {
	vis.set(x);
	for (auto &e : G[x]) {
		if (vis[e.to]) lb.insert(dis[x] ^ dis[e.to] ^ e.w, 0x3FFFFFFF);
		else dis[e.to] = dis[x] ^ e.w, dfs(e.to);
	}
}

struct Qs {
	int x, y, l, r;
	bit v;
	bool operator < (const Qs &q) const noexcept {
		return l < q.l;
	}
} Q[M];
vector<Qs> tmp;

int main(void) {
	//cin.tie(0)->sync_with_stdio(false);

	int n, m, q; cin >> n >> m >> q;

	bit v;
	for (int x, y, i = 0; i < m; ++i) {
		cin >> x >> y; read(v);
		G[x].push_back({v, y});
		G[y].push_back({v, x});
		// output(v); cout << '\n';
	} dfs(1);
	output(lb.qmax(0)); cout << '\n';

	// get xxj last time
	string s;
	int aid = 0;
	for (int x, y, k, i = 1; i <= q; ++i) {
		cin >> s;
		if (s[1] == 'd') {
			cin >> x >> y; read(v);
			Q[++aid] = {x, y, i, q + 1, v};
		} else if (s[1] == 'a') {
			cin >> k; Q[k].r = i;
		} else {
			cin >> k; read(v);
			Q[k].r = i, tmp.push_back(Q[k]);
			Q[k].l = i, Q[k].r = q + 1, Q[k].v = v;
		}
	}

	for (int i = 1; i <= aid; ++i) {
		tmp.push_back(Q[i]);
	} sort(tmp.begin(), tmp.end());

	auto it = tmp.begin();
	for (int t = 1; t <= q; ++t) {
		while (it != tmp.end() && it->l <= t)
			lb.insert(dis[it->x] ^ dis[it->y] ^ it->v, it->r), ++it;
		output(lb.qmax(t)); cout << '\n';
	}
}
