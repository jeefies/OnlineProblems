#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>

using namespace std;
const int N = 6e5;

int h[N];
double lf[N], rf[N];
double sq[N];

void solveL(int i, int j, int pl, int pr) {
	if (i > j) return;

	int mid = (i + j) >> 1, fp = mid; // find best for mid
	double f = h[mid];
	for (int p = pl; p <= pr && p < mid; ++p) {
		if (f < h[p] + sq[mid - p])
			f = h[p] + sq[mid - p], fp = p;
	} lf[mid] = f;
	solveL(i, mid - 1, pl, fp);
	solveL(mid + 1, j, fp, pr);
}

void solveR(int i, int j, int pl, int pr) {
	if (i > j) return;
	int mid = (i + j) >> 1, fp = mid;
	double f = h[mid];
	for (int p = pr; p >= pl && p > mid; --p) {
		if (f < h[p] + sq[p - mid])
			f = h[p] + sq[p - mid], fp = p;
	} rf[mid] = f;
	solveR(i, mid - 1, pl, fp);
	solveR(mid + 1, j, fp, pr);
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> h[i]; sq[i] = std::sqrt(i);
	}

	solveL(1, n, 1, n);
	solveR(1, n, 1, n);

	for (int i = 1; i <= n; ++i) {
		// cout << std::ceil(max(lf[i], rf[i])) - h[i] << '\n';
		cout << (long long)(std::ceil(max(lf[i], rf[i])) - h[i]) << '\n';
	}

	return 0;
}
