#include <iostream>
#include <algorithm>

using namespace std;
using lint = long long;

const int N = 5e5 + 7, mod = 1e9;

lint n, a[N], tot = 0;

lint mx[N], mn[N];
long long mxpre[N], mnpre[N], mpre[N];
long long mxipre[N], mnipre[N], mipre[N];

lint calc(lint l, lint r) {
    if (l > r)
        return 0;

    if (l == r)
        return a[l] * a[l] % mod;

    lint mid = (l + r) >> 1;
    lint ans = (calc(l, mid) + calc(mid + 1, r)) % mod;

    mx[mid] = mn[mid] = a[mid];

    for (lint i = mid - 1; i >= l; --i) {
        mx[i] = max(mx[i + 1], a[i]);
        mn[i] = min(mn[i + 1], a[i]);
    }

    for (lint i = mid + 1; i <= r; ++i) {
        mx[i] = max(mx[i - 1], a[i]);
        mn[i] = min(mn[i - 1], a[i]);
    }

    mxpre[mid] = mnpre[mid] = mpre[mid] = mxipre[mid] = mnipre[mid] = mipre[mid] = 0;

    for (lint i = mid + 1; i <= r; ++i) {
        mxpre[i] = (mxpre[i - 1] + mx[i]) % mod;
        mnpre[i] = (mnpre[i - 1] + mn[i]) % mod;
        mpre[i] = (mpre[i - 1] + mn[i] * mx[i]) % mod;

        mxipre[i] = (mxipre[i - 1] + i * mx[i]) % mod;
        mnipre[i] = (mnipre[i - 1] + i * mn[i]) % mod;
        mipre[i] = (mipre[i - 1] + i * mn[i] % mod * mx[i]) % mod;
    }

    lint minpos = mid, maxpos = mid;

    for (lint i = mid; i >= l; --i) {
        while (minpos < r && mn[minpos + 1] >= mn[i])
            ++minpos;

        while (maxpos < r && mx[maxpos + 1] <= mx[i])
            ++maxpos;

        lint s1 = min(minpos, maxpos), s2 = max(minpos, maxpos);

        // [mid + 1, s1] mi[i], mx[i]
        lint tmp = (mid + 1 + s1) * (s1 - mid) / 2 % mod;
        (ans += (tmp - (s1 - mid) * (i - 1) % mod) % mod * mn[i] % mod * mx[i] % mod) %= mod;

        // [s1 + 1, s2]
        if (minpos <= maxpos) {
            (ans += ((mnipre[s2] - mnipre[s1]) % mod - (i - 1) * (mnpre[s2] - mnpre[s1]) % mod) % mod * mx[i] % mod) %=
                mod;
        } else {
            (ans += ((mxipre[s2] - mxipre[s1]) % mod - (i - 1) * (mxpre[s2] - mxpre[s1]) % mod) % mod * mn[i] % mod) %=
                mod;
        }

        // [s2 + 1, r]
        (ans += (mipre[r] - mipre[s2]) % mod - (i - 1) * (mpre[r] - mpre[s2]) % mod) %= mod;
    }

    return ans;
}

int main() {
    cin.tie(0)->sync_with_stdio(false);

    cin >> n;

    for (register int i = 1; i <= n; ++i)
        cin >> a[i];

    cout << (calc(1, n) % mod + mod) % mod << '\n';
}