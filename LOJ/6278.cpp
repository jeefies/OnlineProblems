#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 5e4 + 3, BLO = 256;
typedef long long lint ;

lint a[N];
lint lab[N], tag[N], st[N], ed[N];
vector<lint> but[N];

void build(int b) {
	but[b].clear();
	for (int i = st[b], ie = ed[b]; i <= ie; ++i) {
		but[b].push_back((a[i] += tag[b]));
	} tag[b] = 0;
	sort(but[b].begin(), but[b].end());
}

void initBut(int n) {
	for (int i = 0, ie = n / BLO; i <= ie; ++i) {
		st[i] = i * BLO;
		ed[i] = (i + 1) * BLO - 1;
		but[i].reserve(BLO);
		build(i);
	}
}

void modify(int l, int r, int c) {
	if (lab[l] == lab[r]) {
		for (int i = l; i <= r; ++i)
			a[i] += c;
		return build(lab[l]);
	}

	for (int i = l, ie = ed[lab[l]]; i <= ie; ++i) {
		a[i] += c;
	} build(lab[l]);

	for (int i = r, ie = st[lab[r]]; i >= ie; --i) {
		a[i] += c;
	} build(lab[r]);

	for (int b = lab[l] + 1; b < lab[r]; ++b) {
		tag[b] += c;
	}
}

int query(int l, int r, lint c) {
	int ans = 0;
	if (lab[l] == lab[r]) {
		int tg = tag[lab[l]];
		for (int i = l; i <= r; ++i)
			ans += (a[i] + tg) < c;
		return ans;
	}

	for (int i = l, tg = tag[lab[l]]; i <= ed[lab[l]]; ++i) {
		ans += (a[i] + tg) < c;
	}

	for (int i = st[lab[r]], tg = tag[lab[r]]; i <= r; ++i) {
		ans += (a[i] + tg) < c;
	}

	for (int b = lab[l] + 1; b < lab[r]; ++b) {
		auto it = lower_bound(begin(but[b]), end(but[b]), c);
		ans += it - begin(but[b]);
	}
	return ans;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	} initBut(n);

	int op, l, r, c;
	for (int i = 1; i <= n; ++i) {
		cin >> op >> l >> r >> c;
		if (op == 0) {
			modify(l, r, c);
		} else {
			cout << query(l, r, 1ll * c * c) << '\n';
		}
	}
}
