#include <iostream>
#include <queue>
#include <vector>
#include <cstring>

using namespace std;
const int N = 52;

int G[N][N][65];
int dis[N][N];

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n, m;
	cin >> n >> m;

	for (int u, v, i = 1; i <= m; ++i) {
		cin >> u >> v;
		G[u][v][0] = true;
	}

	memset(dis, 0x3F, sizeof(dis));
	for (int t = 1; t <= 64; ++t) {
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= n; ++j)
				for (int k = 1; k <= n; ++k)
					G[i][j][t] |= G[i][k][t - 1] & G[k][j][t - 1];
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= n; ++j) {
				if (G[i][j][t]) dis[i][j] = 1;
			}
	}

	for (int k = 1; k <= n; ++k) {
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= n; ++j) {
				dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
			}
		}
	}

	cout << dis[1][n] << '\n';
}
