#include <iostream>
#include <algorithm>

using namespace std;
const int N = 5e5 + 7;

int sa[N << 1], tmp[2][N], cnt[N];

int *getSA(string &str) {
	int n = str.size();

	int m = 128;
	int *x = tmp[0], *y = tmp[1];
	for (int i = 1; i <= n; ++i) 
		++cnt[x[i] = str[i - 1]];
	for (int i = 1; i <= m; ++i) 
		cnt[i] += cnt[i - 1];
	for (int i = n; i; --i)
		sa[cnt[x[i]]--] = i;
	
	for (int p, k = 1; k < n; k <<= 1) {
		p = 0;
		for (int i = n - k + 1; i <= n; ++i) y[++p] = i;
		for (int i = 1; i <= n; ++i) {
			if (sa[i] > k) y[++p] = sa[i] - k;
		}

		for (int i = 1; i <= m; ++i) 
			cnt[i] = 0;
		for (int i = 1; i <= n; ++i)
			++cnt[x[i]];
		for (int i = 1; i <= m; ++i)
			cnt[i] += cnt[i - 1];
		for (int i = n; i; --i)
			sa[cnt[x[y[i]]]--] = y[i], y[i] = 0;

		swap(x, y);

		p = 0;
		for (int i = 1; i <= n; ++i) {
			x[sa[i]] = (y[sa[i]] == y[sa[i - 1]] && y[sa[i] + k] == y[sa[i - 1] + k])
				? p : ++p;
		}

		if (p >= n) break;
		m = p;
	}

	return x;
}

int H[N];
void getH(string &s, int *rk) {
	int n = s.size();
	for (int k = 0, i = 1; i <= n; ++i) {
		if (k > 0) --k;
		int l = i - 1, r = sa[rk[i] - 1] - 1;
		while (s[l + k] == s[r + k])
			++k;
		H[rk[i]] = k;
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	string A, B;
	cin >> A >> B;
	int sep = A.size();
	A += '$';
	A += B;
	int ans = 0, n = A.size();

	int *rk = getSA(A);
	getH(A, rk);

	for (int i = 1; i <= n; ++i) {
		if ((sa[i] < sep && sa[i - 1] > sep) ||
				(sa[i] > sep && sa[i - 1] < sep))
			ans = max(ans, H[i]);
	}

	cout << ans << '\n';
}
