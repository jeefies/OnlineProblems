#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int N = 1e7 + 3, mod = 20101009;

vector<int> prms;
int f[N], notp[N];
void getF(int n) {
	f[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i])
			f[i] = 1 + mod - i, prms.push_back(i);
		for (int p : prms) {
			if (i * p > n) break;
			notp[i * p] = 1;
			if (i % p == 0) {
				f[i * p] = f[i];
				break;
			} else
				f[i * p] = 1ll * f[i] * f[p] % mod;
		}
	}

	for (int i = 2; i <= n; ++i) {
		f[i] = (f[i - 1] + 1ll * i * f[i]) % mod;
	}
}

int g[N];
inline void getG(int n) {
	for (int i = 1; i <= n; ++i) {
		g[i] = (g[i - 1] + i) % mod;
	}
}

inline void work(void) {
	int n, m; cin >> n >> m;

	int u = min(n, m);
	getF(u), getG(u);
	int res = 0;
	for (int l = 1, r; l <= u; l = r + 1) {
		r = min(n / (n / l), m / (m / l));
		res += 1ll * g[n / l] * g[m / l] % mod * (f[r] - f[l - 1]) % mod;
		res %= mod;
	}
	if (res < 0) res += mod;
	cout << res << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int T = 1;
	while (T--) work();
}
