#include <cstdint>
#include <vector>
#include <iostream>

using namespace std;
const int N = 400000 + 3;
typedef unsigned long long ull;
typedef int16_t i16;

ull myRand(ull &k1, ull &k2) {
    ull k3 = k1, k4 = k2;
    k1 = k4;
    k3 ^= (k3 << 23);
    k2 = k3 ^ k4 ^ (k3 >> 17) ^ (k4 >> 26);
    return k2 + k4;
}

i16 bitc[0xFFFF + 1];
void initBitc() {
	for (int i = 0; i <= 0xFFFF; ++i)
		bitc[i] = bitc[i >> 1] + (i & 1);
}

int flag = 0;
char HexList[] = "084C2A6E195D3B7F";
i16 Hex[128];
struct Word {
	i16 bits[16];
	void read() {
		static char s[65]; scanf("%s", s);
		for (int i = 63; ~i; --i)
			bits[i >> 2] = (bits[i >> 2] << 4) | Hex[s[i]];
		for (int i = 0; flag && i < 16; ++i) 
			bits[i] ^= 0xFFFF;
		// for (int i = 0; i < 16; ++i) {
		// 	printf("%X ", bits[i] & 0xFFFF);
		// } putchar('\n');
	}

	int diff(const Word &wd) {
		int d = 0;
		for (int i = 0; i < 16; ++i) 
			d += bitc[(bits[i] ^ wd.bits[i]) & 0xFFFF];
		return d;
	}
} words[N], Q;

#define genBit() ((myRand(a1, a2) & (1ull << 32) ? 1 : 0))
void gen(int n, ull a1, ull a2) {
	static bool rdbits[257];
	for (int i = 1; i <= n; ++i) {
		Word &wd = words[i];
		for (int j = 0; j < 256; ++j) 
			rdbits[j] = genBit();
		for (int j = 255; ~j; --j)
			wd.bits[j >> 4] = (wd.bits[j >> 4] << 1) | rdbits[j];
		// fprintf(stdout, "GEN: "); for (int j = 0; j < 16; ++j) {
		// 	fprintf(stdout, "%0X ", wd.bits[j] & 0xFFFF);
		// } putchar('\n');
	}
}

struct Dictionary {
	vector<int> bitMap[16][0xFFFF + 1];
	void init(int n) {
		for (int i = 1; i <= n; ++i) {
			const Word &wd = words[i];
			for (int bit = 0; bit < 16; ++bit)
				bitMap[bit][wd.bits[bit] & 0xFFFF].push_back(i);
		}
	}

	int find(int c) {
		for (int i = 0; i < 16; ++i) {
			for (auto w : bitMap[i][Q.bits[i] & 0xFFFF])
				if (Q.diff(words[w]) <= c) return 1;
		}
		return 0;
	}
} dict;

int n, m;
int main() {
	freopen("qi3.in", "r", stdin);
	freopen("qi.out", "w", stdout);
	initBitc();
	for (int i = 0; i < 16; ++i) Hex[HexList[i]] = i;

	ull a1, a2;
	scanf("%d%d%llu%llu", &n, &m, &a1, &a2);
	gen(n, a1, a2);
	dict.init(n);
	for (int c, i = 1; i <= m; ++i) {
		Q.read(), scanf("%d", &c);
		flag = dict.find(c);
		printf("%u\n", flag);
	}
    return 0;
}
