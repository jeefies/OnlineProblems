#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e3;

vector<long long> v[N];

int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	int n, m, q;
	cin >> n >> m >> q;
	
	long long cur = 0;
	for (int i = 1; i <= n; ++i) {
		v[i].push_back(0);
		for (int j = 1; j <= m; ++j) v[i].push_back(++cur);
	}
	
	for (int x, y, id, i = 1; i <= q; ++i) {
		cin >> x >> y;
		cout << (id = v[x][y]) << '\n';
		v[x].erase(v[x].begin() + y);
		for (int k = x + 1; k <= n; ++k) {
			v[k - 1].push_back(v[k].back());
			v[k].pop_back();
		} v[n].push_back(id);
	}
	return 0;
}

/*
20 20 12
5 5
2 4
7 10
6 20
4 2
7 7
1 9
10 13
5 6
12 14
5 20
13 14
*/
