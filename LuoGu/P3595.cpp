#include <iostream>
#include <algorithm>
#include <bitset>
#include <cstring>

using namespace std;
const int N = 13, M = 1 << 13, INF = 0x01010101;

int n, m;
int dis[N][N], trans[M][M], f[N][M];
int nxt[M];

int BIT(int x) {
	static int rest[40], mod = 37, init = 0;
	if (!init) {
		init = 1;
		for (int i = 0; i < 30; ++i)
			rest[(1 << i) % mod] = i;
	}
	return rest[x % mod];
}

#define bit(x, i) (((x) >> (i)) & 1)

string bitop(int x) {
	bitset<4> bs(x);
	return string(bs.to_string());
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	memset(dis, INF, sizeof(dis));
	// memset(trans, INF, sizeof(trans));
	memset(f, INF, sizeof(f));

	cin >> n >> m;
	for (int u, v, w, i = 0; i < m; ++i) {
		cin >> u >> v >> w; --u, --v;
		if (w < dis[u][v]) {
			dis[u][v] = dis[v][u] = w;
		}
	}

	int sits = 1 << n;

	// prepare for trans array
	for (int S = 0; S < sits; ++S) {
		int C = S ^ (sits - 1), t = 0;
		for (int T = C; T; T = (T - 1) & C) {
			nxt[t++] = T;
		}

		while (t--) {
			int T = nxt[t], mdis = INF;
			int x = BIT(T & -T);
			for (int y = 0; y < n; ++y) {
				if (bit(S, y)) mdis = min(mdis, dis[x][y]);
			}
			if (mdis == INF)
				trans[S][T] = INF;
			else
				trans[S][T] = mdis + trans[S][T ^ (T & -T)];
			// cout << "Trans " << bitop(C) << " to " << bitop(T) << " = " << trans[C][T] << '\n';
		}
	}

	for (int i = 0; i < n; ++i) {
		f[0][1 << i] = 0;
	}

	for (int d = 1; d < n; ++d) {
		for (int S = 1; S < sits; ++S) {
			for (int T = S; T; T = (T - 1) & S) {
				int C = S ^ T;
				if (trans[C][T] == INF || f[d - 1][C] == INF) continue;
				f[d][S] = min(f[d][S], f[d - 1][C] + trans[C][T] * d);
			}
		}
	}

	int ans = INF;
	for (int l = 0; l < n; ++l)
		ans = min(ans, f[l][sits - 1]);

	cout << ans << '\n';
}
