#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;
const int N = 4e6 + 3, M = 102, mod = 998244353;

int qpow(int a, int x) {
	int r = 1;
	for (; x; x >>= 1, a = 1ll * a * a % mod)
		if (x & 1) r = 1ll * r * a % mod;
	return r;
}

int fac[N] = {1}, ifac[N];
void prepare(int n)  {
	for (int i = 1; i <= n; ++i) fac[i] = 1ll * fac[i - 1] * i % mod;
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i) ifac[i - 1] = 1ll * ifac[i] * i % mod;
}

int C(int n, int m) {
	return 1ll * fac[n] * ifac[m] % mod * ifac[n - m] % mod;
}

int mat[M][M];
int det(int n) {
	int ans = 1;
	for (int i = 1; i < n; ++i) {
		// swap
		if (!mat[i][i]) for (int j = i + 1; j <= n; ++j) {
			if (mat[j][i]) {
				swap(mat[i], mat[j]), ans = -ans;
				break;
			}
		}

		// clear
		int inv = qpow(mat[i][i], mod - 2);
		for (int j = i + 1; j <= n; ++j) {
			int div = 1ll * mat[j][i] * inv % mod;
			for (int k = i; k <= n; ++k) {
				mat[j][k] -= 1ll * mat[i][k] * div % mod;
				mat[j][k] %= mod;
			}
		}
	}

	for (int i = 1; i <= n; ++i) {
		ans = 1ll * ans * mat[i][i] % mod;
	}
	return ans < 0 ? ans + mod : ans;
}

int a[M], b[M];
void work(void) {
	int n, m; cin >> n >> m;

	for (int i = 1; i <= m; ++i)
		cin >> a[i] >> b[i];

	for (int i = 1; i <= m; ++i) {
		for (int j = 1; j <= m; ++j)
			mat[i][j] = a[i] <= b[j] ? C(b[j] - a[i] + n - 1, n - 1) : 0;
	}

	int dt = det(m) % mod;
	cout << dt << '\n';
}

signed main(void) {
	cin.tie(0)->sync_with_stdio(false);

	prepare(4e6);
	int T; cin >> T;
	while (T--) work();
}
