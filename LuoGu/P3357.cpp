#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 5e4 + 3;
typedef long long lint;

vector<int> prm;
int mob[N], notp[N];
lint f[N], pre[N];

void getMob(int n) {
	mob[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i])
			mob[i] = -1, prm.push_back(i);

		for (int p : prm) {
			if (i * p > n) break;
			notp[i * p] = 1;
			mob[i * p] = -mob[i];
			if (i % p == 0) {
				mob[i * p] = 0;
				break;
			}
		}
	}

	for (int i = 2; i <= n; ++i)
		mob[i] += mob[i - 1];
}

lint F(int x) {
	lint v = 0;
	for (int l = 1, r; l <= x; l = r + 1) {
		r = x / (x / l);
		v += (r - l + 1) * (x / l);
	}
	return v;
}

inline void initF(int n) {
	for (int i = 1; i <= n; ++i)
		pre[i] = pre[i - 1] + (f[i] = F(i));
}

inline void work() {
	int n, m; cin >> n >> m;
	int u = min(n, m);
	lint v = 0;
	for (int l = 1, r; l <= u; l = r + 1) {
		r = min(n / (n / l), m / (m / l));
		v += (mob[r] - mob[l - 1]) * f[n / l] * f[m / l];
	}
	cout << v << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	initF(5e4), getMob(5e4);
	int T; cin >> T;
	while (T--) work();
}
