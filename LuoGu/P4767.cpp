#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 3002, INF = 0x70FFFFFF;

int d[N];
int w[N][N], dp[N][N], fr[N][N];

int main() {
	cin.tie(0)->sync_with_stdio(false);
	int n, p; cin >> n >> p;

	for (int i = 1; i <= n ; ++i) {
		cin >> d[i];
	}

	// init w
	for (int i = 1; i <= n; ++i) {
		w[i][i] = 0;
		for (int j = i + 1; j <= n; ++j) {
			w[i][j] = w[i][j - 1] + d[j] - d[(i + j) >> 1];
		}
	}

	memset(dp, 0x7F, sizeof(dp));
	dp[0][0] = 0;
	// dp
	for (int j = 1; j <= p; ++j) {
		fr[n + 1][j] = n;
		for (int i = n; i; --i) {
			int mw = INF, mi = i;
			for (int k = fr[i][j - 1]; k <= fr[i + 1][j]; ++k)
				if (dp[k][j - 1] + w[k + 1][i] < mw) mw = dp[k][j - 1] + w[k + 1][i], mi = k;
			dp[i][j] = mw, fr[i][j] = mi;
		}
	}

	cout << dp[n][p] << '\n';
}
