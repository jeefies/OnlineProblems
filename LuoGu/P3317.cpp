#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;
const int N = 55;
const double eps = 1e-7;

double M[N][N];

double det(int n) {
	double res = 1;
	for (int i = 1; i < n; ++i) {
		// swap none empty line here.
		for (int j = i + 1; j <= n; ++j)
			if (fabs(M[i][i]) < fabs(M[j][i])) swap(M[i], M[j]), res = -res;

		if (fabs(M[i][i]) < eps) return 0;
		// clear the row below
		for (int j = i + 1; j <= n; ++j) {
			double div = M[j][i] / M[i][i];
			for (int k = i; k <= n; ++k)
				M[j][k] -= M[i][k] * div;
		}
	}
	for (int i = 1; i <= n; ++i)
		res *= M[i][i];
	return res;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	double fact = 1.0;
	int n; cin >> n;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= n; ++j) {
			double v; cin >> v;
			if (v < eps) v = eps;
			else if (v > 1 - eps) v = 1 - eps;
			if (i > j) fact *= (1 - v);
			M[i][j] = v / (1 - v);
		}
	}

	for (int i = 1; i <= n; ++i) {
		M[i][i] = 0;
		for (int j = 1; j <= n; ++j) {
			if (i ^ j)
				M[i][i] += M[i][j],
				M[i][j] = -M[i][j];
		}
	}

	cout << fact * det(n - 1) << '\n';
}
