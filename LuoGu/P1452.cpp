#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;
using lint = long long;

const int N = 5e4 + 7;
const double eps = 1e-7;

template<typename T = lint>
struct _Point {
	T x, y;

	_Point<T> operator - (const _Point<T> &p) const {
		return {x - p.x, y - p.y};
	}

	_Point<T> operator + (const _Point<T> &p) const {
		return {x + p.x, y + p.y};
	}

	double operator * (const _Point<T> &p) const {
		return (double)x * p.y - (double)y * p.x;
	}
};
using Point = _Point<>;
using cPoint = const Point &;

int dc(double a, double b) {
	if (a - b > eps) return 1;
	if (b - a > eps) return -1;
	return 0;
}

lint dis(cPoint a, cPoint b) {
	lint dx = a.x - b.x, dy = a.y - b.y;
	return dx * dx + dy * dy;
}

bool slopeCmp(cPoint r, cPoint m, cPoint l) {
	return dc((r - l) * (m - l), 0) > 0;
	// return (r - l) * (m - l) > 0;
}

void convexHull(vector<Point> &ps) {
	int n = ps.size();
	printf("convex n: %d\n", n);
	for (int i = 0; i < n; ++i) {
		if (ps.at(i).x < ps.front().x || (ps.at(i).x == ps.front().x && ps.at(i).y < ps.at(0).y))
			swap(ps.at(i), ps.front());
	}

	double sx = ps[0].x, sy = ps[0].y;
	auto angleCmp = [&](const Point &l, const Point &r) {
		double al = atan2(l.y - sy, l.x - sx),
			   ar = atan2(r.y - sy, r.x - sx);
		return !dc(al, ar) ? (l.y == r.y ? l.x < r.x : l.y < r.y) : al < ar;
	};

	sort(ps.begin() + 1, ps.end(), angleCmp);

	vector<Point> stk;
	stk.clear();
	stk.reserve(n + 2);
	int top = 0;
	for (int i = 0; i < n; ++i) {
		while (top > 1 && slopeCmp(ps[i], stk[top - 1], stk[top - 2]))
			--top, stk.pop_back();
		stk.push_back(ps[i]);
		printf("tp %d\n", top);
		++top;
	}

	ps.assign(stk.begin(), stk.end());
}

void Minkowski(vector<Point> &A, vector<Point> &B, vector<Point> &C) {
	puts("In minkow");
	int n = A.size(), m = B.size();
	vector<Point> tA(n), tB(m);

	for (int i = 0; i + 1 < n; ++i)
		tA[i] = A[i + 1] - A[i];
	for (int i = 0; i + 1 < m; ++i)
		tB[i] = B[i + 1] - B[i];
	tA[n - 1] = A[0] - A[n - 1], tB[n - 1] = B[0] - B[n - 1];

	int idx = 0, i = 0, j = 0;
	while (i < n || j < m) {
		if (j >= m || (i < n && tA[i] * tB[j] < 0))
			C[idx] = (idx ? C[idx - 1] + tA[i] : tA[i]), ++i;
		else
			C[idx] = (idx ? C[idx - 1] + tB[j] : tB[j]), ++j;
		printf("%d, {%d, %d}\n", idx, i, j);
		++idx;
	} puts("");
}

vector<Point> A, B, minkow;

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n, m, p;
	cin >> n >> m >> p;

	A.reserve(n), B.reserve(m);
	
	int x, y;
	for (int i = 0; i < n; ++i) {
		cin >> x >> y;
		A.push_back({x, y});
	}

	for (int i = 0; i < m; ++i) {
		cin >> x >> y;
		B.push_back({-x, -y});
	}

	convexHull(A);
	convexHull(B);

	minkow.resize(A.size() + B.size());
	Minkowski(A, B, minkow);

	for (auto p : minkow) {
		printf("{%lld, %lld} ", p.x, p.y);
	} puts("");

	convexHull(minkow);

	return 0;
}
