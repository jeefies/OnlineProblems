#include <iostream>
#include <algorithm>
#include <stack>
#include <cstring>

using namespace std;
const int N = 2e6 + 7;

int sa[N << 1], tmp[2][N], cnt[N];

int *getSA(string &str) {
	int n = str.size();

	int m = 128;
	int *x = tmp[0], *y = tmp[1];
	for (int i = 1; i <= m; ++i) cnt[i] = 0;
	for (int i = 1; i <= n; ++i) ++cnt[x[i] = str[i - 1]];
	for (int i = 1; i <= m; ++i) cnt[i] += cnt[i - 1];
	for (int i = n; i; --i) sa[cnt[x[i]]--] = i;
	
	for (int p, k = 1; k < n; k <<= 1) {
		p = 0;
		for (int i = n - k + 1; i <= n; ++i) y[++p] = i;
		for (int i = 1; i <= n; ++i) {
			if (sa[i] > k) y[++p] = sa[i] - k;
		}

		for (int i = 1; i <= m; ++i) cnt[i] = 0;
		for (int i = 1; i <= n; ++i) ++cnt[x[i]];
		for (int i = 1; i <= m; ++i) cnt[i] += cnt[i - 1];
		for (int i = n; i; --i) sa[cnt[x[y[i]]]--] = y[i], y[i] = 0;

		swap(x, y);

		p = 0;
		for (int i = 1; i <= n; ++i) {
			x[sa[i]] = (y[sa[i]] == y[sa[i - 1]] && y[sa[i] + k] == y[sa[i - 1] + k])
				? p : ++p;
		}

		if (p >= n) break;
		m = p;
	}

	return x;
}

int H[N];
void getH(string &s, int *rk) {
	int n = s.size();
	for (int k = 0, i = 1; i <= n; ++i) {
		if (k > 0) --k;
		int l = i - 1, r = sa[rk[i] - 1] - 1;
		while (s[l + k] == s[r + k])
			++k;
		H[rk[i]] = k;
	}
}

int lg[N];
int st[N][21];
void initST(int n) {
	for (int i = 2; i <= n; ++i)
		lg[i] = lg[i >> 1] + 1;
	for (int i = 1; i <= n; ++i)
		st[i][0] = H[i];

	int t = lg[n];
	for (int k = 1; k <= t; ++k) {
		for (int i = 1; i + (1 << k) - 1 <= n; ++i)
			st[i][k] = min(st[i][k - 1], st[i + (1 << (k - 1))][k - 1]);
	}
}

inline int qst(int l, int r) {
	int k = lg[r - l + 1];
	return min(st[l][k], st[r - (1 << k) + 1][k]);
}

int n, m;
string s;

int *rk;

int sum[N << 5], lc[N << 5], rc[N << 5], use = 0;
int rt[N];

inline int clone(int x) {
	++use;
	sum[use] = sum[x], lc[use] = lc[x], rc[use] = rc[x];
	return use;
}

inline void insert(int &x, int i, int l, int r) {
	x = clone(x);
	if (l == r)
		return ++sum[x], (void)0;
	int mid = (l + r) >> 1;
	if (i <= mid) insert(lc[x], i, l, mid);
	if (i > mid) insert(rc[x], i, mid + 1, r);
	sum[x] = sum[lc[x]] + sum[rc[x]];
}

inline int qsum(int p, int q, int l, int r, int L, int R) {
	if (l <= L && R <= r)
		return sum[q] - sum[p];
	int mid = (L + R) >> 1, tot = 0;
	if (l <= mid) tot = qsum(lc[p], lc[q], l, r, L, mid);
	if (r > mid) tot += qsum(rc[p], rc[q], l, r, mid + 1, R);
	return tot;
}

inline bool isValid(int len, int a, int b, int c, int d) {
	int rkc = rk[c];

	int lsh = 0;
	for (int w = 1 << lg[rkc]; w; w >>= 1) {
		if (lsh + w <= rkc && qst(lsh + w, rkc) < len)
			lsh += w;
	}

	int rsh = rkc;
	for (int w = 1 << lg[n - rkc + 1]; w; w >>= 1) {
		if (rsh + w <= n && qst(rkc + 1, rsh + w) >= len)
			rsh += w;
	}

	return qsum(rt[lsh - 1], rt[rsh], a, b - len + 1, 1, n) > 0;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> m >> s;

	rk = getSA(s);
	getH(s, rk);
	initST(n);

	for (int i = 1; i <= n; ++i)
		insert(rt[i] = rt[i - 1], sa[i], 1, n);
	
	int a, b, c, d;
	for (int i = 1; i <= m; ++i) {
		cin >> a >> b >> c >> d;
		
		int len = 0, top = min(b - a + 1, d - c + 1);
		for (int w = 1 << lg[top]; w; w >>= 1) {
			if (len + w <= top && isValid(len + w, a, b, c, d))
				len += w;
		}

		cout << len << '\n';
	}
}
