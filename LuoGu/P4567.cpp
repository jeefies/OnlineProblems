#include <iostream>
#include <algorithm>
#include <vector>
#include <random>
#include <stack>
#include <ctime>

using namespace std;
const int N = 1e6;

std::mt19937 gen(time(NULL));
std::uniform_int_distribution<int> dist(0, 0x7FFFFFFF);

class Treap {
private:
	int lc[N], rc[N], siz[N], val[N], pri[N], rev[N];
	int usage, root;
	inline int _new(int v) {
		int p = ++usage; pri[p] = dist(gen);
		lc[p] = rc[p] = rev[p] = 0, val[p] = v, siz[p] = 1;
		return p;
	}

	inline void sync(int p) {
		if (rev[p]) {
			swap(lc[p], rc[p]);
			if (lc[p]) rev[lc[p]] ^= 1;
			if (rc[p]) rev[rc[p]] ^= 1;
			rev[p] = 0;
		}
	}

	inline void update(int p) {
		siz[p] = siz[lc[p]] + siz[rc[p]] + 1;
	}

public:
	void splitByRank(int p, int k, int &x, int &y) {
		if (!p) return (void)(x = y = 0);
		sync(p);
		if (siz[lc[p]] < k)
			x = p, splitByRank(rc[p], k - siz[lc[p]] - 1, rc[x], y);
		else
			y = p, splitByRank(lc[p], k, x, lc[y]);
		update(p);
	}

	int merge(int x, int y) {
		if (!x | !y) return x | y;
		if (pri[x] > pri[y]) {
			sync(x), rc[x] = merge(rc[x], y);
			return update(x), x;
		} else {
			sync(y), lc[y] = merge(x, lc[y]);
			return update(y), y;
		}
	}

	void mark(int k, int n) {
		int x, y, z;
		splitByRank(root, k, x, y);
		splitByRank(y, n, y, z);
		rev[y] ^= 1;
		root = merge(merge(x, y), z);
	}

	void remove(int l, int r) {
		int x, y, z;
		splitByRank(root, l - 1, x, y);
		splitByRank(y, r - l + 1, y, z);
		root = merge(x, z);
	}

	char get(int k) {
		int x, y, z;
		splitByRank(root, k, x, y);
		splitByRank(y, 1, y, z);
		char v = (char)val[y];
		return merge(x, merge(y, z)), v;
	}

	int stk[N];
	int build(const string &s) {
		int top = 0;
		for (auto c : s) {
			int p = _new(c), lst = 0;
			while (top > 0 && pri[stk[top]] < pri[p])
				update(lst = stk[top--]);
			if (top) rc[stk[top]] = p;
			lc[p] = lst, stk[++top] = p;
		}
		while (top) update(stk[top--]);
		return stk[1];
	}

	void insert(int k, const string &s) {
		int x, y, p = build(s);
		splitByRank(root, k, x, y);
		root = merge(x, merge(p, y));
	}

	void mitprt(int p) { if (!p) return; mitprt(lc[p]); cout << (char)val[p]; mitprt(rc[p]); }
	void prt(int p) {
		printf("In node (%d), val = %c, siz = %d, rev = %d, pri = %d\n", p, val[p], siz[p], rev[p], pri[p]);

		if (lc[p]) {
			printf("(%d) left in:\n", p);
			prt(lc[p]);
			printf("(%d) left out\n", p);
		}
		if (rc[p]) {
			printf("(%d) right in:\n", p);
			prt(rc[p]);
			printf("(%d) right out\n", p);
		}
	}
} treap;

string str;
int main() {
	cin.tie(0);
	int n; cin >> n;

	string op;
	int pos = 0;
	for (int i = 1; i <= n; ++i) {
		cin >> op;
		if (op[0] == 'I') {
			int len = 0; cin >> len;
			str.resize(len), getchar();
			for (int i = 0; i < len; ++i) str[i] = getchar();
			treap.insert(pos, str);
		} else if (op[0] == 'M') {
			cin >> pos;
		} else if (op[0] == 'P') {
			--pos;
		} else if (op[0] == 'N') {
			++pos;
		} else if (op[0] == 'R') {
			int n; cin >> n;
			treap.mark(pos, n);
		} else if (op[0] == 'D') {
			int n; cin >> n;
			treap.remove(pos + 1, pos + n);
		} else if (op[0] == 'G') {
			cout << treap.get(pos) << '\n';
		}
	}
}
