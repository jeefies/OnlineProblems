#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>

using namespace std;

struct P {
	int x, y, z;
};

void order(int &a, int &b, int &c) {
	if (a > b) swap(a, b);
	if (b > c) swap(b, c);
	if (a > b) swap(a, b);
}

int totJump;
P upStep(int x, int y, int z, int step) {
	totJump = 0;
	while (step) {
		order(x, y, z);
		int n = y - x, m = z - y;
		if (n == m) break;
		if (n > m) {
			int jump = min((n - 1) / m, step);
			z -= jump * m, y -= jump * m;
			step -= jump;
			totJump += jump;
		} else if (n < m) {
			int jump = min((m - 1) / n, step);
			x += jump * n, y += jump * n;
			step -= jump;
			totJump += jump;
		}
	}
	return (P){x, y, z};
}

bool operator == (const P a, const P b) {
	return a.x == b.x && a.y == b.y && a.z == b.z;
}

int main() {
	int a, b, c, x, y, z;
	scanf("%d%d%d%d%d%d", &a, &b, &c, &x, &y, &z); 
	order(a, b, c), order(x, y, z);

	P rtx = upStep(a, b, c, 0x7FFFFFFF);
	int dx = totJump;
	P rty = upStep(x, y, z, 0x7FFFFFFF);
	int dy = totJump;
	if (!(rtx == rty)) {
		printf("NO\n");
		return 0;
	}

	int dh = abs(dx - dy);
	if (dx > dy) {
		P j = upStep(a, b, c, dx - dy);
		a = j.x, b = j.y, c = j.z;
		order(a, b, c);
	} else if (dx < dy) {
		P j = upStep(x, y, z, dy - dx);
		x = j.x, y = j.y, z = j.z;
		order(x, y, z);
	}

	fprintf(stderr, "After jump to {%d, %d, %d} and {%d, %d, %d}\n", a, b, c, x, y, z);

	int step = 0;
	for (int w = 1 << 30; w; w >>= 1) {
		if (!(upStep(a, b, c, step + w) == upStep(x, y, z, step + w))) step += w;
	}
	printf("YES\n%d\n", dh + step * 2);
}
