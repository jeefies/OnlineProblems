#include <iostream>
#include <algorithm>

using namespace std;
const int mod = 998244353, inv2 = 499122177, inv6 = 166374059;
typedef long long lint;

struct Ques {
	lint f, g, h;
	Ques(lint f, lint g, lint h) : f(f), g(g), h(h) {}
};

#define p2(i) ((i) * (i) % mod)
#define n1 ((n + 1) % mod)
#define n2 (n * (n + 1) % mod * inv2 % mod)
#define n3 (n * (n + 1) % mod * (2 * n + 1) % mod * inv6 % mod)
Ques solve(lint n, lint a, lint b, lint c) {
	if (a == 0) {
		return Ques(
				(n + 1) * (b / c) % mod,
				n2 * (b / c) % mod,
				p2(b / c) * (n + 1) % mod
				);
	} else if (a >= c || b >= c) {
		Ques down = solve(n, a % c, b % c, c);
		return Ques(
				((n1 * (b / c) % mod + n2 * (a / c) % mod) % mod
				 + down.f) % mod,
				(((a / c) * n3 % mod + (b / c) * n2 % mod) % mod
				 + down.g) % mod,
				((p2(a / c) * n3 % mod + p2(b / c) * n1 % mod + (a / c) * (b / c) % mod * n2 * 2 % mod) % mod
				 + down.h + down.g * 2 % mod * (a / c) % mod + down.f * 2 * (b / c) % mod) % mod
				);
	}

	lint m = ((a * n + b) / c);
	Ques down = solve(m - 1, c, c - b - 1, a);
	m %= mod;
	return Ques(
			((m * n % mod) + mod - down.f) % mod,
			(m * n2 % mod + mod - inv2 * (down.f + down.h) % mod) % mod,
			((n * m % mod * (m + 1) % mod) + mod -
				(((m * n % mod) + mod - down.f) % mod
				+ 2 * (down.f + down.g) % mod) % mod
			 ) % mod
		);
}

lint f(lint n, lint a, lint b, lint c) {
	if (a == 0) // recursive boundary
		return (n + 1) * (b / c) % mod;
	if (n == 0)
		return (b / c) % mod;
	if (a >= c || b >= c)
		return (((n + 1) * (b / c) % mod + n * (n + 1) % mod * inv2 % mod * (a / c) % mod) % mod
			+ f(n, a % c, b % c, c)) % mod;
	lint m = (a * n + b) / c;
	return (n * m % mod + mod - f(m - 1, c, c - b - 1, a)) % mod;
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int T, n, a, b, c; cin >> T;
	while (T--) {
		cin >> n >> a >> b >> c;
		Ques q = solve(n, a, b, c);
		cout << q.f << ' ' << q.h << ' ' << q.g << '\n';
		// cout << f(n, a, b, c) << " 0 0" << '\n';
	}
}

