#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e5, F = N, MX = N + N + N;

int bi[MX + 1];
#define lowbit(i) (i & -i)
void update(int i, int x = 1) {
	for (; i <= MX; i += lowbit(i)) bi[i] += x;
}
int query(int i) {
	int r = 0; for (; i; i -= lowbit(i)) r += bi[i];
	return r;
}

int findk(int k) {
	int idx = 0;
	for (int w = 1 << 20; w; w >>= 1) {
		if (idx + w <= MX && query(idx + w) < k) idx += w;
	} return idx + 1;
}

int id[MX], pla[MX];
int main() {
	int n, m; cin >> n >> m;
	for (int x, i = 1; i <= n; ++i) {
		cin >> x; id[F + i] = x, pla[x] = F + i;
		update(F + i);
	}

	string op;
	for (int s, t, i = 1; i <= m; ++i) {
		cin >> op >> s;
		if (op[0] == 'T') {
			int p = findk(1); 
			id[pla[s]] = 0; update(pla[s], -1);
			pla[s] = p - 1;
			id[pla[s]] = s; update(pla[s]);
		} else if (op[0] == 'B') {
			int p = findk(n); 
			id[pla[s]] = 0; update(pla[s], -1);
			pla[s] = p + 1;
			id[pla[s]] = s; update(pla[s]);
		} else if (op[0] == 'I') {
			cin >> t; 
			int q = query(pla[s]), p = findk(q + t);
			int r = id[p];
			swap(id[pla[s]], id[p]), swap(pla[s], pla[r]);
		} else if (op[0] == 'A') {
			cout << query(pla[s]) - 1 << '\n';
		} else {
			cout << id[findk(s)] << '\n';
		}
	}
	return 0;
}
