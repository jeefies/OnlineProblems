#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 5e4 + 3, M = 1e5 + 3;

struct Edge {
	int from, to;
	long long w;
	int next, tree;
} edge[M << 1];
int head[N], tot = 1;
inline void add(int u, int v, long long w) {
	edge[++tot] = {u, v, w, head[u], 0}, head[u] = tot;
	edge[++tot] = {v, u, w, head[v], 0}, head[v] = tot;
}

long long vis[N], dis[N];
inline void dfs(int x) {
	vis[x] = 1;
	for (int i = head[x]; i; i = edge[i].next) {
		int y = edge[i].to;
		if (!vis[y]) {
			edge[i].tree = edge[i ^ 1].tree = 1;
			dis[y] = dis[x] ^ edge[i].w;
			dfs(y);
		}
	}
}

#define bit(x, i) (((x) >> (i)) & 1)
class LinearBasis {
private:
	long long xxj[64];
public:
	void insert(long long x) {
		for (int i = 63; ~i; --i) {
			if (bit(x, i))
				x ^= xxj[i];
			if (bit(x, i))
				return (void)(xxj[i] = x);
		}
	}

	void qmax(long long &x) {
		for (int i = 63; ~i; --i) {
			if (!bit(x, i))
				x ^= xxj[i];
			// if ((x ^ xxj[i]) > x)
			// 	x ^= xxj[i];
		}
	}
} lb;

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n, m; cin >> n >> m;
	long long w;
	for (int u, v, i = 0; i < m; ++i) {
		cin >> u >> v >> w;
		add(u, v, w);
	} dfs(1);

	// build cycles
	for (int i = 2; i <= tot; ++i) {
		if (!edge[i].tree) {
			auto e = edge[i];
			lb.insert(dis[e.to] ^ dis[e.from] ^ e.w);
		}
	}

	long long ans = dis[n];
	lb.qmax(ans);
	cout << ans << '\n';
	return 0;
}
