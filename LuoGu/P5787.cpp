#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <vector>

using namespace std;
const int N = 2e5 + 3;

int n, m, k;

class MergeFindSet {
private:
	int grp[N], rk[N];
	stack<int> MY, MX, MR;
public:
	void init(int n) {
		for (int i = 1; i <= n; ++i) grp[i] = i, rk[i] = 1;
	}

	int find(int x) {
		while (x != grp[x]) x = grp[x];
		return x;
	}

	void merge(int x, int y) {
		x = find(x), y = find(y);
		if (x == y) {
			MY.push(0), MX.push(0), MR.push(1);
			return;
		}
		if (rk[x] < rk[y]) swap(x, y); // make sure rkx >= rky
		MY.push(y), MX.push(x), MR.push(rk[x]);
		grp[y] = x;
		if (rk[x] == rk[y]) ++rk[x];
	}

	void cancle() {
		grp[MY.top()] = MY.top(); MY.pop();
		rk[MX.top()] = MR.top(); MX.pop(), MR.pop();
	}
} mfs;

struct SegTree {
	vector<int> sx, sy;
} tree[N << 4];

#define lc(p) (p<<1)
#define rc(p) (lc(p)|1)
void update(int l, int r, int x, int y, int L, int R, int p = 1) {
	if (l > r) return;
	// printf("Updating [%d, %d] with {%d, %d} in [%d, %d](p %d)\n", l, r, x, y, L, R, p);
	if (l <= L && R <= r) {
		tree[p].sx.push_back(x), tree[p].sy.push_back(y);
		return;
	} int mid = (L + R) >> 1;
	if (l <= mid) update(l, r, x, y, L, mid, lc(p));
	if (r > mid) update(l, r, x, y, mid+1, R, rc(p));
}

bool ok[N];
void dfs(int l, int r, int p = 1) {
	int siz = tree[p].sx.size();
	for (int i = 0; i < siz; ++i) {
		int x = tree[p].sx[i], y = tree[p].sy[i];
		mfs.merge(x, y + n); mfs.merge(y, x + n);
		if (mfs.find(x) == mfs.find(x + n) || mfs.find(y) == mfs.find(y + n)) {
			for (int j = 0; j <= i; ++j) mfs.cancle(), mfs.cancle();
			for (int j = l; j <= r; ++j) ok[j] = false;
			return;
		}
	}

	if (l == r) ok[l] = true;
	else { int mid = (l + r) >> 1;
		dfs(l, mid, lc(p)), dfs(mid + 1, r, rc(p));
	}

	for (int i = 0; i < siz; ++i) mfs.cancle(), mfs.cancle();
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	cin >> n >> m >> k;
	for (int x, y, l, r, i = 0; i < m; ++i) {
		cin >> x >> y >> l >> r;
		if (l ^ r) update(l + 1, r, x, y, 1, k);
	} mfs.init(n + n);
	dfs(1, k);
	for (int i = 1; i <= k; ++i) cout << (ok[i] ? "Yes\n" : "No\n");
	return 0;
}
