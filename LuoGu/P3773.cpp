#include <iostream>
#include <algorithm>

using namespace std;
const int N = 3e5, mod = 1000000007;

int f[N];
int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	int ans = 0;
	for (int S, i = 1; i <= n; ++i) {
		cin >> S;
		(ans += f[S]) %= mod;

#define nxt(sub) ((sub - 1) & S)
		for (int sub = nxt(S); sub; sub = nxt(sub)) 
			(f[sub] += f[S] + 1) %= mod;
	} 
	
	cout << ans << '\n';
	return 0;
}
