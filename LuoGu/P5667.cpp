#include <iostream>
#include <algorithm>

using namespace std;
const int N = (1 << 19), mod = 998244353, g = 3, ig = (mod + 1) / 3;
typedef long long lint;

inline lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

lint A[N], B[N], C[N];
int rev[N];

lint fac[N] = {1}, ifac[N], invx[N] = {1};
lint mfac[N] = {1}, imfac[N], invm[N] = {1};
void prepare(int lim, int n, int m) {
	for (int i = 1; i <= lim; ++i) {
		fac[i] = fac[i - 1] * i % mod;
		mfac[i] = mfac[i - 1] * (m - n + i - 1) % mod;
	}

	ifac[lim] = qpow(fac[lim], mod - 2);
	imfac[lim] = qpow(mfac[lim], mod - 2);
	for (int i = lim; i; --i) {
		invx[i] = ifac[i] * fac[i - 1] % mod;
		invm[i] = imfac[i] * mfac[i - 1] % mod;
		ifac[i - 1] = ifac[i] * i % mod;
		imfac[i - 1] = imfac[i] * (m - n + i - 1) % mod;
	}
}

void NTT(lint *v, int n, int inv) {
	for (int i = 0; i < n; ++i)
		if (i < rev[i]) swap(v[i], v[rev[i]]);

	for (int u = 2; u <= n; u <<= 1) {
		lint omega = qpow((~inv ? g : ig), (mod - 1) / u);
		for (int i = 0, k = u >> 1; i < n; i += u) {
			lint w = 1;
			for (int j = 0; j < k; ++j, w = w * omega % mod) {
				lint s = v[i + j], t = v[i + j + k] * w % mod;
				v[i + j] = (s + t) % mod, v[i + j + k] = (s - t + mod) % mod;
			}
		}
	}

	if (inv == -1) {
		lint iv = qpow(n, mod - 2);
		for (int i = 0; i < n; ++i)
			v[i] = v[i] * iv % mod;
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n, m; cin >> n >> m;
	prepare(n * 2 + 1, n, m);
	for (int i = 0; i <= n; ++i) 
		cin >> A[i];
	for (int i = 0; i <= n; ++i) {
		A[i] = A[i] * ifac[i] % mod * ifac[n - i] % mod;
		if ((n - i) & 1) A[i] = mod - A[i];
	}
	for (int i = 0; i <= (n << 1); ++i) B[i] = invm[i + 1];

	int lg = 0, O = 2, mn = n * 3;
	for (; O <= mn; O <<= 1) ++lg;

	for (int i = 0; i < O; ++i)
		rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << lg);

	NTT(A, O, 1), NTT(B, O, 1);
	for (int i = 0; i < O; ++i)
		A[i] = A[i] * B[i] % mod;
	NTT(A, O, -1);
	for (int i = n, ie = (n << 1); i <= ie; ++i) {
		cout << mfac[i + 1] * imfac[i - n] % mod * A[i] % mod << ' ';
	}
}
