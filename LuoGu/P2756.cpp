#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <limits>

using namespace std;
const int N = 200;

typedef long long lint;
class ISAP {
private:
	struct Edge {
		int to, rev;
		lint flow;
	};
	vector<Edge> G[N];
	int dep[N], gap[N];
public:
	int n, s, t;
	inline void add(int u, int v, lint f) {
		G[u].push_back({v, (int)G[v].size(), f});
		G[v].push_back({u, (int)G[u].size() - 1, 0});
	}
	
	inline void init(int n, int s, int t) {
		this->n = n, this->s = s, this->t = t;
		
		fill_n(dep, n + 1, -1), fill_n(gap, n + 1, 0);
		gap[(dep[t] = 0)] = 1;
		queue<int> q; q.push(t);
		
		while (q.size()) {
			int x = q.front(); q.pop();
			for (auto e : G[x]) {
				if (dep[e.to] == -1)
					++gap[(dep[e.to] = dep[x] + 1)], q.push(e.to);
			}
		}
	}
	
	inline lint sap(int x, lint flow) {
		if (x == t) return flow;
		lint rest = flow;
		for (auto &e : G[x]) {
			if (e.flow > 0 && dep[e.to] + 1 == dep[x]) {
				lint push = sap(e.to, min(e.flow, rest));
				if (push) {
					e.flow -= push, G[e.to][e.rev].flow += push;
					rest -= push; 
				}
				if (!rest) return flow;
			}
		}
		
		if (--gap[dep[x]] == 0) dep[s] = n + 1;
		return ++gap[++dep[x]], flow - rest;
	}
	
	inline lint maxflow() {
		lint flow = 0, INF = numeric_limits<lint>::max() >> 1;
		while (dep[s] <= n) flow += sap(s, INF);
		return flow;
	}
	
	inline void search(int x) {
		for (auto e : G[x]) {
			if (e.to < t && e.flow > 0) {
				cout << e.to << " " << x << '\n';
				return;
			}
		}
	}
} isap; 

int main() {
//	cin.tie(0)->sync_with_stdio(false);
	
	int m, n;
	cin >> m >> n;
	int s = 0, t = n + 1;
	for (int i = 1; i <= m; ++i) isap.add(s, i, 1);
	for (int i = m + 1; i <= n; ++i) isap.add(i, t, 1);
	
	for (int u = 0, v = 0; cin >> u >> v, ~u && ~v;) {
		isap.add(u, v, 1);
	}
	
	clog << "Run\n";
	isap.init(t, s, t);
	cout << isap.maxflow() << '\n';
	for (int i = m + 1; i <= n; ++i) isap.search(i);
	return 0;
}
