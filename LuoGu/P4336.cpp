#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 20, mod = 1e9 + 7;

int qpow(int a, int x) noexcept {
	int r = 1;
	for (; x; x >>= 1, a = 1ll * a * a % mod)
		if (x & 1) r = 1ll * r * a % mod;
	return r;
}

int M[N][N]; // M = D - A
int det(int n) noexcept {
	int ans = 1;
	for (int i = 1; i < n; ++i) {
		// get none zero line
		for (int j = i + 1; j <= n; ++j) {
			if (!M[i][i] && M[j][i]) 
				ans = -ans, swap(M[i], M[j]);
		}

		int inv = qpow(M[i][i], mod - 2);
		for (int j = i + 1; j <= n; ++j) {
			// clear row i
			int fact = 1ll * M[j][i] * inv % mod;
			for (int k = i; k <= n; ++k) {
				M[j][k] -= 1ll * M[i][k] * fact % mod;
				M[j][k] %= mod;
			}
		}
	}

	for (int i = 1; i < n; ++i)
		ans = 1ll * ans * M[i][i] % mod;
	return ans < 0 ? ans + mod : ans;
}

struct P {
	int x, y;
};
vector<P> G[N];

int main(void) noexcept {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	for (int i = 0; i + 1 < n; ++i) {
		int cnt, x, y; cin >> cnt;
		while (cnt--) {
			cin >> x >> y;
			G[i].push_back({x, y});
		}
	}

#define bit(x, i) (((x) >> (i)) & 1)
	int ans = 0, sits = 1 << (n - 1);
	for (int S = 1; S < sits; ++S) {
		memset(M, 0, sizeof(M));
		int cnt = 0;
		for (int i = 0; i + 1 < n; ++i) {
			if (!bit(S, i)) continue;
			++cnt;
			for (auto p : G[i]) {
				++M[p.x][p.x], ++M[p.y][p.y];
				--M[p.y][p.x], --M[p.x][p.y];
			}
		}

		int dt = det(n);
		// ans += cnt & 1 ? mod - det(n) : det(n);
		ans += (n - cnt + 1) & 1 ? mod - dt : dt;
		ans %= mod;
	}
	cout << (ans < 0 ? ans + mod : ans) << '\n';
	return 0;
}
