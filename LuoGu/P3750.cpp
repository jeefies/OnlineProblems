#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e5 + 7, mod = 100003;
typedef long long lint;

lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

lint arr[N], f[N];
lint inv[N] = {1}, fac[N] = {1}, ifac[N];

void prepare(int n) {
	for (int i = 1; i <= n; ++i) fac[i] = fac[i - 1] * i % mod;
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i) {
		inv[i] = ifac[i] * fac[i - 1] % mod;
		ifac[i - 1] = ifac[i] * i % mod;
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n, k; cin >> n >> k;
	int tot = 0;
	for (int i = 1; i <= n; ++i) cin >> arr[i];
	for (int i = n; i; --i) {
		if (arr[i]) { ++tot;
			arr[i] ^= 1;
			for (int j = 1; j * j <= i; ++j) 
				if (i % j == 0)  {
					arr[j] ^= 1;
					if (j * j != i) arr[i / j] ^= 1;
				}
		}
	}

	prepare(n); f[n] = 1;
	for (int i = n; i; --i)
		f[i - 1] = (n + (n - i + 1) * f[i] % mod) % mod * inv[i - 1] % mod;

	lint ans = 0;
	for (int i = k + 1; i <= tot; ++i) {
		ans = (ans + f[i]) % mod;
	} ans = (ans + k) % mod;
	if (tot <= k) ans = tot;
	cout << ans * fac[n] % mod << '\n';

	return 0;
}

