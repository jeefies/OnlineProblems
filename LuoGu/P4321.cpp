#include <iostream>
#include <algorithm>

using namespace std;
const int N = 20, mod = 998244353;
typedef long long lint;

inline lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

int mp[N][N];
lint mat[N][N];

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n, e; cin >> n >> e;
	for (int u, v, i = 0; i < e; ++i) {
		cin >> u >> v;
		mp[u][v] = mp[v][u] = 1;
	}
}
