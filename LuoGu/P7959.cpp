#include <iostream>
#include <algorithm>
#include <assert.h>

using namespace std;
const int N = 3e3 + 1;

int n, r;
int A[N];
int tmp[N];
int pre[N], suf[N];
int pref[N], suff[N];
int f[N][N], trans[N][N];
int stk[N], siz = 0;

inline void rotate() {
	register int i;
	for (i = 1; i <= r; ++i)
		tmp[i] = A[n - r + i];
		
	for (i = r + 1; i <= n; ++i)
		tmp[i] = A[i - r];
		
	for (i = 1; i <= n; ++i)
		A[i] = tmp[i];
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	register int i, j, k;

	cin >> n >> r;
	for (i = 1; i <= n; ++i)
		cin >> A[i];
	
	for (i = 1; i <= n; ++i) {
		for (j = 1; j <= n; ++j)
			f[i][j] = -0x7FFFFFF;
	}
	
pre[0] = suf[n] = -1e9;
for (i = 1; i <= n; ++i, rotate()) {
	// prefix k
	for (k = 1; k < n; ++k) {
		// pre[k] = max(pre[k - 1], f[i - 1][k] + A[k]);
		if (pre[k - 1] >= f[i - 1][k] + A[k]) {
			pre[k] = pre[k - 1];
			pref[k] = pref[k - 1];
		} else {
			pre[k] = f[i - 1][k] + A[k];
			pref[k] = k;
		}
	}

	// suffix
	for (k = n - 1; k; --k) {
		// suf[k] = max(suf[k + 1], f[i - 1][k] - A[k + 1]);
		if (suf[k + 1] >= f[i - 1][k] - A[k + 1]) {
			suf[k] = suf[k + 1];
			suff[k] = suff[k + 1];
		} else {
			suf[k] = f[i - 1][k] - A[k + 1];
			suff[k] = k;
		}
	}

	for (j = 1; j < n; ++j) { // enum cur ID[i + 1]
		int p = pre[j] - A[j + 1], s = suf[j] + A[j];
		if (p >= s) {
			f[i][j] = p;
			trans[i][j] = pref[j];
		} else {
			f[i][j] = s;
			trans[i][j] = suff[j];
		}
	}
}
	
	int cur = 0, mv = -0x7FFFFFF;
	for (j = 1; j <= n; ++j) {
		if (f[n][j] > mv) mv = f[n][j], cur = j;
	}
	
	cout << mv << '\n';
	assert(mv >= 0);
	
	stk[siz++] = cur;
	for (i = n; i; --i) {
		cur = trans[i][cur];
		stk[siz++] = cur;
	}
	while (siz--) cout << stk[siz] << ' ';
}
