#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 2e5, BL = 600;

int n, len;
int apps[N], apc = 0;

struct P {
	int x, y;
	bool operator < (const P &p) {
		return x < p.x;
	}
} A[N];

class Block {
private:
	int bl, br, tag;
	deque<int> q;
public:
	inline void set(int l, int r) { bl = l, bl = r; }
} blocks[BL];

int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> A[i].x >> A[i].y;
		apps[++apc] = A[i].y;
	} sort(A + 1, A + 1 + n), sort(apps + 1, apps + 1 + apc);

	// discrete y
	int *ae = unique(apps + 1, apps + 1 + apc);
	for (int i = 1; i <= n; ++i) {
		A[i].y = lower_bound(apps + 1, ae, A[i].y) - apps;
	}

	len = sqrt(ae - apps);
	for (int i = 0, l = 1, r = len; l <= n; ++i, l += len, r += len) {
		blocks[i].set(l, r);
		blocks[i].init();
	}
}
