#include <iostream>
#include <algorithm>

using namespace std;
const int N = 603;
int mod;

int m[N][N];

int det(int n) {
	int ans = 1;
	for (int i = 1; i <= n; ++i) {
		for (int j = i + 1; j <= n; ++j) {
			// division i, j
			while (m[i][i]) {
				int div = m[j][i] / m[i][i];
				for (int k = i; k <= n; ++k) {
					m[j][k] -= 1ll * div * m[i][k] % mod;
					m[j][k] %= mod;
				}
				swap(m[i], m[j]);
				ans = -ans;
			}
			swap(m[i], m[j]);
			ans = -ans;
		}
	}

	for (int i = 1; i <= n; ++i)
		ans = 1ll * ans * m[i][i] % mod;
	return ans < 0 ? ans + mod : ans;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n >> mod;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= n; ++j) {
			cin >> m[i][j]; m[i][j] %= mod;
		}
	}

	cout << det(n) << '\n';
	return 0;
}
