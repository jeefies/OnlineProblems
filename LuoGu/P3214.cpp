#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e6 + 3, mod = 1e8 + 7;
typedef long long lint;

lint P[N] = {1}, f[N];

lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n, m; cin >> n >> m;
	
	lint n2 = qpow(2, n);
	for (int k = 1; k <= m; ++k) {
		P[k] = P[k - 1] * (n2 - k + mod) % mod;
	}

	lint fac = 1;
	f[0] = 1, f[1] = 0;
	for (int i = 2; i <= m; ++i) {
		fac = fac * i % mod;

		f[i] = P[i - 1] - f[i - 1] - f[i - 2] * (i - 1) % mod * ((n2 - i + 1) % mod) % mod;
		f[i] %= mod;
		if (f[i] < 0) f[i] += mod;
	}

	cout << qpow(fac, mod - 2) * f[m] % mod << '\n';
}
