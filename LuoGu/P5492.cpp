#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 21, M = (1 << 20), mod = 998244353;

int n, m;
int G[N];
int f[M], siz[M];

#define mark(x, i) ((x) | (1 << i))
#define unmark(x, i) ((x) & ~(1 << i))
#define bit(x, i) (((x) >> i) & 1)
#define lsh(i) (1 << (i))
#define lowbit(i) ((i) & -(i))

int qpow(int a, int x) {
	int r = 1;
	for (; x; x >>= 1, a = 1ll * a * a % mod)
		if (x & 1) r = 1ll * r * a % mod;
	return r;
}

int cnter[M];
int fac[N] = {1}, ifac[N], inv[N] = {1};
void prepare() {
	for (int i = 1; i <= n; ++i) {
		fac[i] = 1ll * fac[i - 1] * i % mod;
	}
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i) {
		inv[i] = 1ll * ifac[i] * fac[i - 1] % mod;
		ifac[i - 1] = 1ll * ifac[i] * i % mod;
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	cin >> n >> m;
	prepare();

	int sits = (1 << n);
	for (int i = 0; i < n; ++i)
		siz[lsh(i)] = 1, G[i] = lsh(i);

	for (int u, v, i = 0; i < m; ++i) {
		cin >> u >> v; --u, --v;
		G[u] = mark(G[u], v), G[v] = mark(G[v], u);
	}

	for (int S = 1; S < sits; ++S) {
		cnter[S] = cnter[S - lowbit(S)] + 1;
		for (int i = 0; i < n; ++i)
			if (bit(S, i)) {
				siz[S] = max(siz[S], siz[S & ~G[i]] + 1);
			}
	}

	f[0] = 1;
	for (int S = 1; S < sits; ++S) {
		for (int i = 0, T;  i < n; ++i) {
			T = S & ~G[i];
			if (bit(S, i) && siz[S] == siz[T] + 1) {
				(f[S] += f[T]) %= mod;
			}
		}
		f[S] = 1ll * f[S] * inv[cnter[S]] % mod;
	}

	cout << f[sits - 1] << '\n';
}
