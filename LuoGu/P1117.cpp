#include <iostream>
#include <algorithm>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;
using hashInt = unsigned long long;

const int N = 6e4 + 7, BASE = 131, FLB = 97, mod = 1e9 + 7;

string s;

int lg[N];
hashInt pre[N], ofs[N];
hashInt flt[N], flo[N];

void init() {
	srand(time(nullptr));
	
	for (int i = ofs[0] = 1; i < N; ++i)
		ofs[i] = ofs[i - 1] * BASE;
	for (int i = flo[0] = 1; i < N; ++i)
		flo[i] = flo[i - 1] * FLB % mod;

	for (int i = 2; i < N; ++i)
		lg[i] = lg[i >> 1] + 1;
}

void prepare() {
	int __ofs = rand() % 22;
	int n = s.size();
	for (int i = 1; i <= n; ++i) {
		(flt[i] = flt[i - 1] * FLB % mod + s[i - 1] - __ofs) %= mod;
		pre[i] = pre[i - 1] * BASE + s[i - 1] - __ofs;
	}
}

inline hashInt mhash(int l, int r) {
	hashInt fl = (flt[r] + mod - flt[l - 1] * flo[r - l + 1] % mod) % mod;
	return pre[r] - pre[l - 1] * ofs[r - l + 1] + fl;
}

int st[N], ed[N];

void work() {
	cin >> s;
	prepare();

	int n = s.size();
	fill_n(st, n + 5, 0);
	fill_n(ed, n + 5, 0);

	for (int len = 1; len * 2 <= n; ++len) {
		// printf("In when len = %d\n", len);
		for (int x = len + len / 2; x <= n; x += len) {
			if (s[x - 1] != s[x - 1 - len]) continue;
			int y = x - len;
			// printf("\tAt x %d, y %d\n", x, y);

			// find LCP
			int lcp = 0;
			for (int w = 1 << lg[len]; w; w >>= 1) {
				if (x + lcp + w - 1 <= n && mhash(y, y + lcp + w - 1) == mhash(x, x + lcp + w - 1))
					lcp += w;
			}

			// find LCS
			int lcs = 0;
			for (int w = 1 << lg[len]; w; w >>= 1) {
				if (y - lcs - w + 1 >= 1 && mhash(y - lcs - w + 1, y) == mhash(x - lcs - w + 1, x))
					lcs += w;
			}

			// printf("\t\tLCS %d, LCP %d\n", lcs, lcp);

			if (lcp + lcs <= len) continue;
			int L = max(x - len * 2 + 1, y - lcs + 1);
			int R = min(x + len - 1, x + lcp - 1);
			// printf("\t\tL %d, R %d\n", L, R);
			++st[L], ++ed[L + 2 * len - 1];
			--ed[R + 1], --st[R - 2 * len + 2];
			// printf("\t\tAdd %d, %d, Sub %d, %d\n", L, L + 2 * len - 1, R - 2 * len + 2, R + 1);
		}
	}

	long long ans = 0;
	for (int i = 1; i <= n; ++i) {
		st[i] += st[i - 1];
		ed[i] += ed[i - 1];
		// printf("st, ed [%d] = %d, %d\n", i, st[i], ed[i]);
		ans += 1ll * st[i] * ed[i - 1];
	}
	cout << ans << '\n';
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	init();

	int T; cin >> T;
	while (T--) work();
	return 0;
}
