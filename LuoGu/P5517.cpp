#include <iostream>
#include <algorithm>
#include <assert.h>
#include <cmath>

using namespace std;
const int N = 1 << 16, mod = 1e9 + 7;

namespace Mker {
//  Powered By Kawashiro_Nitori
//  Made In Gensokyo, Nihon
	#include<climits>
	#define ull unsigned long long
	#define uint unsigned int
	ull sd;int op;
	inline void init() {scanf("%llu %d", &sd, &op);}
	inline ull ull_rand()
	{
		sd ^= sd << 43;
		sd ^= sd >> 29;
		sd ^= sd << 34;
		return sd;
	}
	inline ull rand()
	{
		if (op == 0) return ull_rand() % USHRT_MAX + 1;
		if (op == 1) return ull_rand() % UINT_MAX + 1;
		if (op == 2) return ull_rand();
	}
}

typedef long long lint;
typedef unsigned long long ulint;

class LP {
private:
	ulint qp[N], sp[N];
	int c, p, q;
public:
	void init(int _c, int _p) {
		c = _c, p = _p;
		q = sqrt(_p) + 1;

		ulint x = 1, y = 1;
		for (int i = 0; i <= q; ++i, x = x * c % p)
			sp[i] = x;
		x = sp[q];
		for (int i = 0; i <= q; ++i, y = y * x % p)
			qp[i] = y;
	}

	ulint qpow(int x) {
		return sp[x % q] * qp[x / q] % p;
	}
} lp;

ulint qpow(ulint a, int x) {
	ulint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

int main(void) {
	int T; scanf("%d", &T);
	Mker::init();

	lp.init(3, mod);

	int inv32 = qpow(32, mod - 2);
	ulint ans = 0;
	while (T--) {
		ulint n = Mker::rand();
		ulint add = n & 1 ? 51 : 21;
		ulint mul = (n % mod * 4 - 13 + mod) % mod;
		ulint h = lp.qpow((n + 2) % (mod - 1)) * mul % mod + add;
		h = h % mod * inv32 % mod;
		ans ^= h;
	}
	cout << ans << '\n';
}
