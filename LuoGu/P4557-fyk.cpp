#include<bits/stdc++.h>
#define ll long long 
#define lf long double 
using namespace std;
const int N = 5e5 + 10;
const lf eps = 1e-7;
int n, m, q;
int s[N], top, tot = 0;
int l,r,mid;
ll x,y;
lf ra,rb;
struct Vec{
	ll x,y;
	Vec operator +(const Vec a)const{
		return (Vec){x + a.x,y + a.y};
	}
	Vec operator -(const Vec a)const{
		return (Vec){x - a.x,y - a.y};
	}
	ll operator *(const Vec a)const{
		return x * a.y - y * a.x;
	}
	ll _abs()const{
		return x * x + y * y; 
	}
	bool operator <(const Vec b)const{
		ra = atan2(y,x);
		rb = atan2(b.y,b.x);
		return ra < rb;
	}
}a[N],b[N],c[N], ans[N],an[N],qu,fi;
bool cmp(Vec a, Vec b, Vec f){
	if((a - f) * (b - f) > 0) return 1;
	else if((b - f) * (a - f) == 0) return (b - f)._abs() < (a - f)._abs();
	else return 0;
}
void tb(Vec p[],int n) {
	top = 0;
	for(int i = 1; i <= n; ++i){
		if(p[i].y < p[1].y || 
		(p[i].y - p[1].y == 0 && p[i].x < p[1].x)){
			swap(p[1],p[i]);
		}
	}
	auto icmp = [&](Vec a, Vec b) {
		return cmp(a, b, p[1]);
	};
	sort(p + 2, p + 1 + n, icmp);
	for(int i = 1; i <= n; ++i){
		while(top >= 2 && ((p[s[top]]) - p[s[top - 1]]) * ((p[i]) - p[s[top]]) < 0) 
			--top;
		s[++top] = i;
	}
	s[top + 1] = 1;
	for(int i = 1; i <= top; ++i){
		ans[++tot] = (p[s[i + 1]] - p[s[i]]);
	}
}
void input(){
	cin>>n>>m>>q;
	for(int i = 1; i <= n; ++i)
		cin>> a[i].x >> a[i].y;
	tb(a, n);
	for(int i = 1; i <= m; ++i){
		cin>> b[i].x >> b[i].y;
		b[i].x = -b[i].x;
		b[i].y = -b[i].y;
	}
	tb(b, n);
}
void pre(){
	sort(ans + 1, ans + 1 + tot);
	c[1] = a[1] + b[1];
	for(int i = 1; i <= tot; ++i){
		printf("ANS[%d] = {%lld, %lld}\n", i, ans[i].x, ans[i].y);
        c[i + 1] = ans[i] + c[i];
		printf("C[%d] = {%lld, %lld}\n", i, c[i].x, c[i].y);
	}

	int siz = tot;
    tot = 0, tb(c, siz);
	printf("tot: %d\n", tot);
	for(int i = 1; i <= tot; ++i){
		printf("C[%d] = {%lld, %lld}\n", i, c[i].x, c[i].y);
	}
}
void op(){
	Vec O = c[1];
	for (int i = 1; i <= tot; ++i)
		c[i] = c[i] - O;

	for(int i = 1; i <= q; ++i){
		cin>> qu.x >> qu.y;
		qu = qu - O;
		if(qu * ans[1] > 0 || qu * ans[tot - 1] < 0){
			cout<<0<<'\n';
			continue;
		} 
		l = 1,r = tot - 1;
		while(l < r){
			mid = (l + r) >> 1;
			if(qu * ans[mid] >= 0) r = mid;
			else l = mid + 1;
		}
		if((ans[l - 1] - qu) * (ans[l] - qu) >= 0){
			cout<<1<<'\n';
		}else{
			cout<<0<<'\n';
		}
	}
}
int main(){
    input();
    pre();
    op();
	return 0;
}
/*
3 3 1
4 -1
0 0
0 -2
0 -1
1 -2
-1 -3
-2 1
*/
