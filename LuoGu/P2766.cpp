#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;
const int N = 1003;

class ISAP {
private:
	struct Edge {
		int to, flow, next;
	} edge[100000];
	int head[N], tot;
	int n, s, t;
	int dep[N], gap[N << 1];
public:
	ISAP() : tot(1) {}
	void clear(int n) { tot = 1; fill_n(head, n + 1, 0); }

	inline void add(int u, int v, int f) {
		// printf("add %d <-%d-> %d\n", u, f, v);
		edge[++tot] = {v, f, head[u]}, head[u] = tot;
		edge[++tot] = {u, 0, head[v]}, head[v] = tot;
	}

	inline void init(int n, int s, int t) {
		this->n = n, this->s = s, this->t = t;
		fill(dep, dep + n + 1, -1);
		fill(gap, gap + n + 1, 0);
		
		gap[(dep[t] = 0)] = 1;
		queue<int> q; q.push(t);

		while (q.size()) {
			int x = q.front(); q.pop();
			for (int i = head[x]; i; i = edge[i].next) {
				int to = edge[i].to;
				if (dep[to] == -1) 
					++gap[(dep[to] = dep[x] + 1)], q.push(to);
			}
		}
	}

	inline int sap(int x, int flow) {
		if (x == t) return flow;

		int rest = flow;
		for (int i = head[x]; i; i = edge[i].next) {
			auto &e = edge[i];
			if (dep[e.to] + 1 == dep[x] && e.flow > 0) {
				int push = sap(e.to, min(e.flow, rest));
				if (push) {
					e.flow -= push, edge[i ^ 1].flow += push;
					rest -= push;
				}
				if (!rest) return flow;
			}
		}

		if (--gap[dep[x]] == 0) dep[s] = n + 1;
		return ++gap[++dep[x]], flow - rest;
	}

	inline int maxflow() {
		int flow = 0, inf = 0x7FFFFFF0;
		while (dep[s] <= n) flow += sap(s, inf);
		return flow;
	}
} isap;

int arr[N], len[N];
vector<int> idx[N];
int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	int n; cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> arr[i];
	}
	
	// calc, and build for question 2
	int s = 0, t = n + n + 1;
	int maxlen = 0;
	for (int i = 1; i <= n; ++i) {
		int l = 1;
		for (int j = 1; j < i; ++j) {
			if (arr[j] <= arr[i]) l = max(l, len[j] + 1);
		} len[i] = l, maxlen = max(maxlen, l);

		for (int j = 1; j < i; ++j)
			if (arr[j] <= arr[i] && len[j] + 1 == l)
				isap.add(j + n, i, 1);
		if (l == 1) isap.add(s, i, 1);
		isap.add(i, i + n, 1);
	} cout << maxlen << '\n';

	if (maxlen == 1) {
		cout << n << '\n'
			<< n << '\n';
		return 0;
	}

	for (int i = 1; i <= n; ++i) {
		if (len[i] == maxlen) isap.add(i + n, t, 1);
	} 
	
	isap.init(t, s, t);
	cout << isap.maxflow() << '\n';

	// build for question 3
	isap.clear(t + 1);
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j < i; ++j) {
			if (arr[j] <= arr[i] && len[j] + 1 == len[i])
				isap.add(j + n, i, 1);
		} isap.add(i, i + n, 1);
		if (len[i] == 1) isap.add(s, i, 0x7FFFFFF);
	}
	for (int i = 1; i <= n; ++i) {
		if (len[i] == maxlen) isap.add(i + n, t, 0x7FFFFFF);
	}
	// add extra flows
	isap.add(1, n + 1, 0x7FFFFFF);
	isap.add(n, n + n, 0x7FFFFFF);
	isap.init(t, s, t);
	cout << isap.maxflow() << '\n';
	return 0;
}

