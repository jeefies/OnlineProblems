#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e5;
typedef long long lint;

struct Equation {
	lint a, p;
};

inline lint gcd(lint x, lint y) {
	return y ? gcd(y, x % y) : x;
}

inline lint exgcd(lint x, lint y, lint &s, lint &t) {
	if (!y) {
		s = 1, t = 0;
		return x;
	}
	lint r = exgcd(y, x % y, t, s);
	t -= (x / y) * s;
	return r;
}

bool merge(const Equation &a, const Equation &b, Equation &res) {
	lint d = gcd(a.p, b.p), s, t;
	lint lcm = a.p / d * b.p;

	lint c = (b.a - a.a) % lcm;
	if (c < 0) c += lcm;
	if (c % d) return false;
	exgcd(a.p / d, b.p / d, s, t);

	s = s * (c / d) % lcm;
	if (s < 0) s += lcm;

	lint x = (a.a + s * a.p % lcm) % lcm;
	if (x < 0) x += lcm;
	res = {x, lcm};
	return true;
}

int main() {
	// cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	Equation e, wt;
	cin >> e.p >> e.a;
	for (int i = 1; i < n; ++i) {
		cin >> wt.p >> wt.a;
		if (!merge(e, wt, e)) {
			cout << "???? Merge Failed\n";
		}
	}

	cout << e.a << '\n';
}
