#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <random>
#include <ctime>
#include <string>

using namespace std;
const int N = 1e5 + 3;
typedef unsigned int uint;
std::mt19937 gen(time(NULL));
std::uniform_int_distribution<uint> rd(0, 0xFFFFFFFFu);

class Treap {
public:
	Treap() : usage(0) {}
	int lc[N], rc[N], val[N], rev[N], siz[N];
	int fa[N], root;
	uint pri[N], usage;
	inline int _new(int v) {
		int p = ++usage; pri[p] = rd(gen);
		lc[p] = rc[p] = rev[p] = 0, val[p] = v, siz[p] = 1;
		return p;
	}

	inline void sync(int p) {
		if (rev[p]) {
			swap(lc[p], rc[p]);
			if (lc[p]) rev[lc[p]] ^= 1;
			if (rc[p]) rev[rc[p]] ^= 1;
			rev[p] = 0;
		}
	}
#define update(p) (siz[p] = siz[lc[p]] + siz[rc[p]] + 1)

	void splitByRank(int p, int k, int &x, int &y, int fx = 0, int fy = 0) {
		if (!k) return y = p, (void)(x = 0);
		if (!p) return (void)(x = y = 0);
		sync(p);
		if (siz[lc[p]] < k)
			fa[p] = fy, x = p, splitByRank(rc[p], k - siz[lc[p]] - 1, rc[p], y, fx, x);
		else
			fa[p] = fx, y = p, splitByRank(lc[p], k, x, lc[p], x, fy);
		update(p);
	}

	int merge(int x, int y) {
		if (!x | !y) return x | y;
		if (pri[x] > pri[y]) {
			sync(x), rc[x] = merge(rc[x], y);
			fa[rc[x]] = x;
			return update(x), x;
		} else {
			sync(y), lc[y] = merge(x, lc[y]);
			fa[lc[y]] = y;
			return update(y), y;
		}
	}

	int push(int x) {
		if (!root) {
			root = _new(x);
			return root;
		} int p = _new(x);
		root = merge(root, p);
		return p;
	}

	int kth(int p) {
		int op = p, rk = siz[lc[p]];
		stack<int> stk; while (op) stk.push(op), op = fa[op];
		while (stk.size()) sync(stk.top()), stk.pop();
		while (fa[p]) rk += p == rc[fa[p]] ? siz[lc[fa[p]]] + 1 : 0, p = fa[p];
		return rk;
	}

	void debug() { debug(root); }
	void debug(int p) {
		if (!p) return;
		printf("In Node %d{val: %d, siz: %d, fa: %d}\n", p, val[p], siz[p], fa[p]);
		if (lc[p]) printf("(val %d)Left IN: \n", val[p]), debug(lc[p]);
		if (rc[p]) printf("(val %d)Right IN: \n", val[p]), debug(rc[p]);
	}
} treap;

int nodeIdx[N];
#define getXYZ() { treap.splitByRank(treap.root, kth, x, y); treap.splitByRank(y, 1, y, z); }
int main() {
	int n, m; cin >> n >> m;
	for (int x, i = 1; i <= n; ++i) {
		cin >> x; nodeIdx[x] = treap.push(x);
	} // treap.debug();

	// cout << treap.kth(nodeIdx[7]) << '\n';
	string op;
	for (int s, t, i = 0; i < m; ++i) {
		cin >> op >> s;
		int kth = treap.kth(nodeIdx[s]);
		int x, y, z;
		if (op == "Top") {
			getXYZ();
			treap.root = treap.merge(treap.merge(y, x), z);
		} else if (op == "Bottom") {
			getXYZ();
			treap.root = treap.merge(x, treap.merge(z, y));
		} else if (op == "Insert") {
			cin >> t;
			getXYZ();
			int w;
			if (t < 0) {
				treap.splitByRank(x, kth + t, x, w);
				treap.root = treap.merge(treap.merge(x, y), treap.merge(w, z));
			} else {
				treap.splitByRank(z, t, z, w);
				treap.root = treap.merge(treap.merge(x, w), treap.merge(y, z));
			}
		} else if (op == "Ask") {
			cout << kth << '\n';
		} else {
			kth = s - 1;
			getXYZ();
			cout << treap.val[y] << '\n';
			treap.root = treap.merge(x, treap.merge(y, z));
		}
	}
	return 0;
}
