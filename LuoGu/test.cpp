#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
const int maxn=100010;
int n,k,x,r,h,t,to[210][maxn],q[maxn];
ll sum[maxn],f[2][maxn];
inline ll pow(ll a){return a*a;}
inline double slope(int j,int k)
{
    if(sum[j]==sum[k]) return -1e18;
    return (f[r&1^1][k]-pow(sum[k])-f[r&1^1][j]+pow(sum[j]))*1.0/(sum[j]-sum[k]);
}
int main()
{
    scanf("%d%d",&n,&k);
    for(int i=1;i<=n;i++)scanf("%d",&x),sum[i]=sum[i-1]+x;
    for(r=1;r<=k;r++)
    {
        h=t=0;
        for(int i=1;i<=n;i++)
        {
            while(h<t&&slope(q[h],q[h+1])<=sum[i]) h++;
            f[r&1][i]=f[r&1^1][q[h]]+sum[q[h]]*(sum[i]-sum[q[h]]);
            to[r][i]=q[h];
			printf("T %d at %d from %d, val %lld\n", r, i, q[h], f[r & 1][i]);
            while(h<t&&slope(q[t-1],q[t])>=slope(q[t],i)) t--;
			printf("Q rest: ");
			for (int i = h; i <= t; ++i) printf("%d ", q[i]);
			puts("");
            q[++t]=i;
        }
    }
    printf("%lld\n",f[k&1][n]);
    for(int i=k,u=n;i>=1;i--)
    {
        u=to[i][u];
        printf("%d ",u);
    }
    return 0;
}
