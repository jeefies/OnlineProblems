#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;
using lint = long long;

const int N = 5e5 + 7;

struct Point {
    int x, y;
} p[N], tmp[N];

inline bool cmpx(const Point &a, const Point &b) {
    return a.x < b.x;
}

inline bool cmpy(const Point &a, const Point &b) {
    return a.y < b.y;
}

inline lint dis(const Point &a, const Point &b) {
    lint dx = a.x - b.x, dy = a.y - b.y;
    return dx * dx + dy * dy;
}

inline lint divide(int l, int r) {
    if (r - l < 2) return 1e18;

    int mid = (l + r) >> 1;
    lint d = min(divide(l, mid), divide(mid, r));

    int i = l, j = mid, k = l, sd = sqrt(d);
    while (i < mid || j < r) {
        if (j == r || (i < mid && p[i].y < p[j].y))
            tmp[k++] = p[i++];
        else
            tmp[k++] = p[j++];
    } copy(tmp + l, tmp + r, p);

    k = 0;
    for (int i = l, mx = p[mid].x; i < r; ++i) {
        if (abs(p[i].x - mx) <= sd) tmp[k++] = p[i];
    }

    int idx = 0;
    for (int i = 0; i < k; ++i) {
        for (int j = i + 1; j < k && tmp[i].y + sd > tmp[j].y; ++j) {
            d = min(d, dis(tmp[i], tmp[j]));
            sd = sqrt(d);
        }
    }

    return d;
}

int main() {
    freopen("test.in", "r", stdin);
    cin.tie(0)->sync_with_stdio(false);

    int n; cin >> n;
    for (int x, y, i = 0; i < n; ++i) {
        cin >> x >> y; p[i] = {x, y};
    } sort(p, p + n, cmpx);

    cout << divide(0, n) << '\n';
    return 0;
}