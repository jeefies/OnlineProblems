#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int N = 1e5 + 3;

static int n;

struct seg {
	int cnt;
	int lc, rc; // left, right color
	int lazy;
} tree[N << 2];
seg operator + (const seg &l, const seg &r) {
	return (seg){l.cnt + r.cnt + (l.rc == r.lc), l.lc, r.rc, 0};
}
ostream& operator << (ostream &os, const seg &s) {
	os << "{" << s.cnt << ", " << s.lc << ", " << s.rc << "}";
	return os;
}

#define lp(p) (p<<1)
#define rp(p) (lp(p)|1)
#define cnt(p) (tree[p].cnt)
#define lc(p) (tree[p].lc)
#define rc(p) (tree[p].rc)
#define lazy(p) (tree[p].lazy)

inline void update(int p) {
	// cerr << "Update " << p << " origin " << tree[p] << " by " << tree[lp(p)] << ' ' << tree[rp(p)] << ": ";
	tree[p] = tree[lp(p)] + tree[rp(p)];
	// cerr << tree[p] << '\n';
}

inline void sync(int p, int l, int r) {
	if (lazy(p)) {
		int mid = (l + r) >> 1, c = lazy(p);
        tree[lp(p)] = {mid - l, c, c, c}, tree[rp(p)] = {r - mid - 1, c, c, c};
		lazy(p) = 0;
	}
}

void rebuild(int L = 1, int R = n, int p = 1) {
	if (L == R) {
		tree[p] = {0, L, L, 0};
		return;
	} int mid = (L + R) >> 1; 
	rebuild(L, mid, lp(p)), rebuild(mid + 1, R, rp(p));
	update(p);
	// cerr << "At " << p << " [" << L << ", " << R << "]: " << tree[p] << '\n';
}

void wipe(int l, int r, int c, int L = 1, int R = n, int p = 1) {
	// cerr << "In wiping [" << l << ", " << r << "] at area [" << L << ", " << R << "]\n";
	if (l <= L && R <= r) {
		tree[p] = {R - L, c, c, c};
		// cerr << "Last wiping [" << L << ", " << R << "]: " << tree[p] << '\n';
		return;
	}

	sync(p, L, R); int mid = (L + R) >> 1;
	if (l <= mid) wipe(l, r, c, L, mid, lp(p));
	if (r > mid) wipe(l, r, c, mid + 1, R, rp(p));
	update(p);
}

seg query(int l, int r, int L = 1, int R = n, int p = 1) {
	if (l <= L && R <= r) {
		// cerr << "Just return in [" << L << ", " << R << "]:" << tree[p] << "\n";
		return tree[p];
	}

	sync(p, L, R); int mid = (L + R) >> 1;
	// cerr << "In query [" << l << ", " << r << "] at area [" << L << ", " << R << "], seg: " << tree[p] << '\n';
	seg ans;
	if (l <= mid && r > mid) ans = query(l, r, L, mid, lp(p)) + query(l, r, mid + 1, R, rp(p));
	else if (l <= mid) ans = query(l, r, L, mid, lp(p));
	else if (r > mid) ans = query(l, r, mid + 1, R, rp(p));
	return update(p), ans;
}

vector<int> G[N];
int dep[N], son[N], siz[N], fa[N];
void workSS(int x, int f) {
	fa[x] = f, dep[x] = dep[f] + 1;
	siz[x] = 1;
	for (int y : G[x]) {
		if (y == f) continue;
		workSS(y, x);
		siz[x] += siz[y];
		if (siz[son[x]] < siz[y]) son[x] = y;
	}
}

int dfn[N], top[N], cdfn = 0;
void workD(int x, int t) {
	dfn[x] = ++cdfn, top[x] = t;
	if (son[x]) workD(son[x], t);

	for (int y : G[x]) {
		if (y ^ son[x] && y ^ fa[x]) workD(y, y);
	}
}

void clear() {
	for (int i = 1; i <= n; ++i) G[i].clear();
	int* ca[] = {son, fa, dep, siz, dfn, top};
	for (int* arr : ca) fill_n(arr, n + 1, 0);
	cdfn = 0, rebuild();
}

void change(int x, int y, int c) {
	while (top[x] != top[y]) {
		if (dep[top[x]] > dep[top[y]]) {
			wipe(dfn[top[x]], dfn[x], c);
			x = fa[top[x]];
		} else {
			wipe(dfn[top[y]], dfn[y], c);
			y = fa[top[y]];
		}
	}
	if (dep[x] > dep[y]) swap(x, y); // dfn[x] <= dfn[y]
	wipe(dfn[x], dfn[y], c);
	// cerr << "LCA " << x << '\n';
}

int count(int x, int y) {
	seg xj = {0, 0, 0, 0}, yj = {0, 0, 0, 0};
	while (top[x] != top[y]) {
		if (dep[top[x]] > dep[top[y]]) {
			xj = query(dfn[top[x]], dfn[x]) + xj;
			// cerr << "xj to " << xj << '\n';
			x = fa[top[x]];
		} else {
			yj = query(dfn[top[y]], dfn[y]) + yj;
			// cerr << "yj tp " << yj << '\n';
			y = fa[top[y]];
		}
	}

	// cerr << "last x, y to " << x << ' ' << y << '\n';
	// if (x == y) return xj.cnt + yj.cnt + (xj.lc == yj.lc);
	if (dep[x] <= dep[y]) { // x is LCA
		seg sj = query(dfn[x], dfn[y]);
		swap(xj.lc, xj.rc);
		// cerr << "x is LCA, merging " << xj << ' ' << sj << ' ' << yj << '\n';
		return (xj + sj + yj).cnt;
	}
	seg sj = query(dfn[y], dfn[x]);
	swap(yj.lc, yj.rc);
	// cerr << "y is LCA, merging " << yj << ' ' << sj << ' ' << xj << '\n';
	return (yj + sj + xj).cnt;
} 

void work() {
	int m; cin >> n >> m;
	clear();
	for (int u, v, i = 1; i < n; ++i) {
		cin >> u >> v;
		G[u].push_back(v), G[v].push_back(u);
	}

	workSS(1, 0), workD(1, 1);
	// log
	// for (int i = 1; i <= n; ++i) {
	//	fprintf(stderr, "node %d: siz %d, son %d, dfn %d, top %d\n", i, siz[i], son[i], dfn[i], top[i]);
	// }

	// cerr << "init with " << query(1, n) << " weis\n";
	for (int op, x, y, i = 1; i <= m; ++i) {
		cin >> op >> x >> y;
		if (op == 1) change(x, y, i + n);
		else cout << count(x, y) << '\n';
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int T; cin >> T;
	while (T--) work();
}
