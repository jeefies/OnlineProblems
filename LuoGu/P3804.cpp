#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 3e6 + 7;

vector<int> G[N];

class SAM {
private:
	// link is equal to fa!
	int ch[N][26], link[N], len[N];
	int siz[N];
	int use, last;
public:
	SAM() { clear(); }

	void clear() {
		use = last = 1;
		fill_n(ch[use], 26, 0);
		link[1] = len[1] = 0;
	}

	void add(int c) {
		int p = last;
		int now = last = ++use;
		len[now] = len[p] + 1;
		siz[now] = 1;

		while (p && !ch[p][c]) {
			ch[p][c] = now;
			p = link[p];
		}

		if (!p) {
			link[now] = 1;
		} else if (len[p] + 1 == len[ch[p][c]]) {
			link[now] = ch[p][c];
		} else {
			int crt = ++use, pc = ch[p][c];
			len[crt] = len[p] + 1;
			link[crt] = link[pc];
			copy_n(ch[pc], 26, ch[crt]);
			while (p && ch[p][c] == pc) {
				ch[p][c] = crt;
				p = link[p];
			}

			link[pc] = link[now] = crt;
		}
	}

	void build() {
		for (int i = 1; i <= use; ++i)
			G[link[i]].push_back(i);
	}

	int dfs(int x = 1) {
		int ans = 0;
		for (auto y : G[x]) {
			ans = max(ans, dfs(y));
			siz[x] += siz[y];
		}

		return max(ans, siz[x] > 1 ? siz[x] * len[x] : 0);
	}
} sam;

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	string s;
	cin >> s;

	for (auto c : s) {
		sam.add(c - 'a');
	}

	sam.build();
	cout << sam.dfs() << '\n';
	return 0;
}
