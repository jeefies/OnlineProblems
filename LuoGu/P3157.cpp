#include <iostream>
#include <algorithm>

using namespace std;
using ling = long long;

const int N = 1e5 + 7;

#define lowbit(i) ((i) & -(i))
struct BIT {
    int b[N];
    inline void update(int i, int x) {
        for (; i < N; i += lowbit(i)) b[i] += x;
    }
    inline void clear(int i) {
        for (; i < N; i += lowbit(i)) b[i] = 0;
    }
    inline int query(int i) {
        int r = 0;
        for (; i; i ^= lowbit(i)) r += b[i];
        return r;
    }
} bit;

int n, m;
struct Num {
    int t, v, i;
} a[N];

inline bool cmpT(const Num &a, const Num &b) {
    return a.t ^ b.t ? a.t < b.t : a.i < b.i;
}

inline bool cmpI(const Num &a, const Num &b) {
    return a.i < b.i;
}

inline bool cmpX(const Num &a, const Num &b) {
    return a.v > b.v;
}

long long ans[N];
void cdq(int l, int r) {
    if (l >= r) return;

    int mid = (l + r) >> 1;
    cdq(l, mid), cdq(mid + 1, r);

    sort(a + l, a + mid + 1, cmpI), sort(a + mid + 1, a + r + 1, cmpI);
    int j = l;
    for (int i = mid + 1; i <= r; ++i) {
        while (j <= mid && a[j].i < a[i].i) {
            bit.update(a[j].t, 1), ++j;
        } ans[a[i].t] += bit.query(m + 1) - bit.query(a[i].t - 1);
    }
    for (int i = l; i <= j; ++i) bit.clear(a[i].t);

    j = r;
    for (int i = mid; i >= l; --i) {
    	while (j > mid && a[j].i > a[i].i) {
    		bit.update(a[j].t, 1), --j;
		} ans[a[i].t] += bit.query(m + 1) - bit.query(a[i].t - 1);
	}
	for (int i = j; i <= r; ++i) bit.clear(a[i].t);
}

int main() {
    cin.tie(0)->sync_with_stdio(false);

    cin >> n >> m;
    for (int x, i = 1; i <= n; ++i) {
        cin >> x;
        a[x] = {m + 1, x, i};
    }

    for (int x, i = 1; i <= m; ++i) {
        cin >> x;
        a[x].t = i;
    }

    sort(a + 1, a + 1 + n, cmpX);
    cdq(1, n);

    sort(a + 1, a + 1 + n, cmpX);
    for (int i = 1; i <= n; ++i) {
        ans[0] += bit.query(a[i].i);
        bit.update(a[i].i, 1);
    }

    for (int i = 0; i < m; ++i) {
        cout << ans[i] << '\n';
        ans[i + 1] = ans[i] - ans[i + 1];
    }

    return 0;
}
