#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_map>

using namespace std;
const int N = 1e6;
typedef long long lint;

vector<int> prms;
int notp[N + 1], mob[N + 1], phi[N + 1];
lint smob[N + 1], sphi[N + 1];

void init(int n) {
	mob[1] = phi[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i]) {
			prms.push_back(i);
			mob[i] = -1, phi[i] = i - 1;
		}

		for (int p : prms) {
			if (i * p > n) break;
			notp[i * p] = true;
			if (i % p) {
				mob[i * p] = -mob[i];
				phi[i * p] = phi[i] * phi[p];
			} else {
				mob[i * p] = 0;
				phi[i * p] = phi[i] * p;
				break;
			}
		}
	}

	for (int i = 1; i <= n; ++i) {
		smob[i] = mob[i] + smob[i - 1];
		sphi[i] = phi[i] + sphi[i - 1];
	}
}

std::unordered_map<int, lint> cmob, cphi;

lint sumMob(int n) {
	if (n <= N) 
		return smob[n];
	if (cmob[n]) 
		return cmob[n];
	lint res = 0;
	for (int l = 2, r; l <= n; l = r + 1) {
		r = n / (n / l);
		res += sumMob(n / l) * (r - l + 1);
	}
	return cmob[n] = 1 - res;
}

lint sumPhi(int n) {
	if (n <= N)
		return sphi[n];
	if (cphi[n])
		return cphi[n];
	lint res = n * (n + 1ll) / 2;
	for (int l = 2, r; l <= n; l = r + 1) {
		r = n / (n / l);
		res -= (r - l + 1) * sumPhi(n / l);
	}
	return cphi[n] = res;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	init(N);
	int T, n; cin >> T;
	while (T--) {
		cin >> n;
		cout << sumPhi(n) << ' ' << sumMob(n) << '\n';
	}
}
