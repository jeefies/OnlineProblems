#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;
const int N = 1e7;
typedef long long lint;
lint UPN, mod, inv4, inv6;

lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = a * r % mod;
	return r;
}

std::vector<int> prms;
int notp[N], phi[N];
lint sum[N];
void prepare() {
	phi[1] = 1;
	for (int i = 2; i <= UPN; ++i) {
		if (!notp[i]) phi[i] = i - 1, prms.push_back(i);

		for (int p : prms) {
			if (i * p > UPN) break;
			notp[i * p] = 1;
			if (i % p == 0) {
				phi[i * p] = phi[i] * p;
				break;
			} else
				phi[i * p] = phi[i] * phi[p];
		}
	}

	for (int i = 1; i <= UPN; ++i) {
		sum[i] = sum[i - 1] + 1ll * i * i % mod * phi[i] % mod;
		sum[i] %= mod;
	}
}

std::unordered_map<lint, lint> csum;

inline lint cn3(lint x) {
	x %= mod;
	return x * (x + 1) % mod * (2 * x % mod + 1) % mod * inv6 % mod;
}

inline lint cn4(lint x) {
	x %= mod;
	lint tmp = (x * (x + 1) / 2) % mod;
	return tmp * tmp % mod;
}

lint getSum(lint n) {
	if (n <= UPN) return sum[n];
	if (csum.count(n)) return csum[n];

	lint s = cn4(n);
	for (lint l = 2, r; l <= n; l = r + 1) {
		r = n / (n / l);
		s -= (cn3(r) - cn3(l - 1) + mod) % mod * getSum(n / l) % mod;
		s %= mod;
	}
	return csum[n] = (s + mod) % mod;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	lint n;
	cin >> mod >> n;

	UPN = 5e6;
	inv4 = qpow(4, mod - 2), inv6 = qpow(6, mod - 2);
	prepare();

	lint ans = 0;
	for (lint l = 1, r; l <= n; l = r + 1) {
		r = n / (n / l);
		ans += cn4((n / l)) * (getSum(r) - getSum(l - 1)) % mod;
		ans %= mod;
	}

	ans = (ans + mod) % mod;
	cout << ans << '\n';
}
