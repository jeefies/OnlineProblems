#include<bits/stdc++.h>
#define N 1350006
#define M 350005
#define inf 100000000008
#define ll long long
using namespace std;
int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,rt[M],siz2[M],ava,from,q,tot,head[N],dep[N],fa[N];
int top[N],siz[N],son[N];
ll w[N-M],dis[N-M]; 
struct tree
{
	int from,to,next,val;
}k[N*2];
ll st[M*3][20],kl[N-M];
void add(int from,int to,int val)
{
	k[++tot].from=from;
	k[tot].to=to;
	k[tot].next=head[from];
	k[tot].val=val;
	head[from]=tot;
}
int ID;
int ln[21];
void dfs(int now,int f)
{ 
	dep[now]=dep[f]+1;
	fa[now]=f;
	siz[now]=1;
	bool vis=0;
	dis[now]=inf;
	for(int i=head[now],to;i;i=k[i].next)
	{
		to=k[i].to;
		if(to==f)continue;vis=1;
		dfs(to,now);
		w[ID]=min(dis[now]+dis[to]+k[i].val,w[ID]);
		dis[now]=min(dis[now],dis[to]+k[i].val);
		siz[now]+=siz[to];
		if(siz[son[now]]<siz[to])son[now]=to;
	}
	if(!vis)dis[now]=0;
}
void hg(int now,int f,int w)
{
	dis[now]=min(dis[now],dis[f]+w);
	kl[now]=kl[f]+w;
	for(int i=head[now];i;i=k[i].next)
	{
		if(k[i].to==f)continue;
		hg(k[i].to,now,k[i].val);
	}
}
void pf(int now,int f)
{
	top[now]=f;
	if(son[now])pf(son[now],f);
	for(int i=head[now],to;i;i=k[i].next)
	{
		to=k[i].to;
		if(to==fa[now]||to==son[now])continue;
		pf(to,to);
	}
}
ll que(int l,int r)
{
	if(l>r)return 0;
	ll ans=0;
	for(int i=19;i>=0;i--)
	{
		if(l+ln[i]-1<=r)
		{
			ans+=st[l][i];
			l+=ln[i];
		}
	}
	return ans;
}
ll lca(int a,int b)
{
	ll ans=kl[a]+kl[b];
	while(top[a]!=top[b])
	{
		if(dep[top[a]]<dep[top[b]])swap(a,b);
		a=fa[top[a]];
	}
	if(dep[a]<dep[b])swap(a,b);
	
	ans-=kl[b]*2;
	return ans;
}
int main()
{
	freopen("1.in", "r", stdin);
	freopen("1.ans", "w", stdout);

	n=read();
	ava=1;
	for(int i=1;i<=n;i++)
	{
		siz2[i]=read();
		rt[i]=ava;
		for(int j=1,u,v,w;j<siz2[i];j++)
		{
			u=read()+ava;v=read()+ava;w=read();
			add(u,v,w);
			add(v,u,w);
		}
		ava+=siz2[i];
	}
	for(int i=1;i<=ava;i++)dis[i]=w[i]=inf;
	for(int i=1;i<=n;i++)
	{
		ID=i; 
		dfs(rt[i],0);
		hg(rt[i],0,inf);
		pf(rt[i],0);
	}
	for(int i=1;i<=n;i++)st[i][0]=w[i];
	ln[0]=1;
	for(int i=1;i<=19;i++)ln[i]=ln[i-1]*2;
	for(int j=1;j<=19;j++)
	{
		for(int i=1;i<=n;i++)
		st[i][j]=st[i][j-1]+st[i+ln[j-1]][j-1];
	}
	q=read();
	int s0,s1,e0,e1;
	ll ans;
	while(q--)
	{
		ans=0;
		s0=read()+1;s1=read();e0=read()+1;e1=read();
		s1=rt[s0]+s1;
		e1=rt[e0]+e1;
		if(s0!=e0)
		{
			if(s0>e0)swap(s0,e0);
			ans+=que(s0+1,e0-1)+(e0-s0);
			ans+=dis[s1]+dis[e1];
		}
		else
		{
			ans=inf;
			if(s0!=1)ans=min(ans,w[s0-1]+dis[s1]+dis[e1]+2ll);
			if(s0!=n)ans=min(ans,w[s0+1]+dis[s1]+dis[e1]+2ll);
			ans=min(ans,lca(s1,e1));
		}
		if(ans>inf)cout<<-1<<"\n";
		else cout<<ans<<"\n";
	}
	return 0;
}
