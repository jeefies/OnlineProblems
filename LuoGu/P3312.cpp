#include <iostream>
#include <vector>
#include <algorithm>
#include <iostream>

#define int long long

using namespace std;
typedef long long lint;
const int N = 1e5 + 3;
const lint mod = 1ll << 31;

vector<int> prms;
int mob[N], notp[N];
lint sum[N];
int but[N];

void getMD(int n) {
	mob[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i])
			mob[i] = -1, prms.push_back(i);

		for (int p : prms) {
			int ip = i * p;
			if (ip > n) break;
			notp[ip] = 1;
			if (i % p == 0) {
				mob[ip] = 0;
			} else {
				mob[ip] = -mob[i];
			}
		}
	}

	for (int i = 1; i <= n; ++i) {
		but[i] = i;
		for (int j = i; j <= n; j += i) (sum[j] += i);
	}
	sort(but + 1, but + 1 + n, [&](const int a, const int b) {
		return sum[a] < sum[b];
	});
}

// flush in f if (suma[d] <= a)
lint f[N];
#define lowbit(i) ((i) & -(i))
inline void add(int i, int x) {
	for (; i < N; i += lowbit(i))
		f[i] += x;
}

inline lint query(int i) {
	lint r = 0;
	for (; i; i -= lowbit(i))
		r = (r + f[i]) % mod;
	return r;
}

inline void flush(const int &d) {
	for (int i = 1; i * d < N; ++i) {
		add(i * d, sum[d] * mob[i]);
	}
}

inline lint work(int n, int m) {
	int u = min(n, m);
	lint res = 0;
	for (int l = 1, r; l <= u; l = r + 1) {
		r = min(n / (n / l), m / (m / l));
		res += (query(r) - query(l - 1)) * (n / l) * (m / l) % mod;
	}
	return res;
}

struct Q {
	int n, m, a, i;
	inline bool operator < (const Q &q) {
		return a < q.a;
	}
} ques[N];

int res[N];
signed main(void) {
	cin.tie(0)->sync_with_stdio(false);

	getMD(1e5);
	int T; cin >> T;
	for (int n, m, a, i = 1; i <= T; ++i) {
		cin >> n >> m >> a;
		ques[i] = {n, m, a, i};
	} sort(ques + 1, ques + 1 + T);

	int la = 1;
	for (int i = 1; i <= T; ++i) {
		const Q &q = ques[i];

		while (la <= N && sum[but[la]] <= q.a) 
			flush(but[la++]);

		res[q.i] = work(q.n, q.m) % mod;
		// cout << work(q.n, q.m) << '\n';
	}

	for (int i = 1; i <= T; ++i) {
		cout << (res[i] % mod) << '\n';
	}
	return 0;
}
