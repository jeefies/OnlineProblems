#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e5 + 7;

struct Edge {
    int from, to, w, col;
    bool operator < (const Edge &e) const {
        return w ^ e.w ? w < e.w : col < e.col;
    }
} e[N], tmp[N];

int grp[N], rk[N];
int find(int x) {
    return x == grp[x] ? x : grp[x] = find(grp[x]);
}

void merge(int x, int y) {
    x = find(x), y = find(y);
    if (rk[x] <= rk[y]) grp[x] = y;
    else grp[y] = x;
    if (rk[x] == rk[y] && x != y) ++rk[y];
}

int ans;
int check(int n, int m, int k) {
    for (int i = 0; i < m; ++i) {
        tmp[i] = e[i];
        if (tmp[i].col == 0) tmp[i].w -= k;
    } sort(tmp, tmp + m);

    for (int i = 1; i <= n; ++i)
        grp[i] = i, rk[i] = 1;

    ans = 0;
    int cnt = 0;
    for (int i = 0; i < m; ++i) {
        if (find(tmp[i].from) == find(tmp[i].to)) continue;
        cnt += !tmp[i].col;
        ans += tmp[i].w;
        merge(tmp[i].from, tmp[i].to);
    }

    // ans += cnt * k;
    return cnt;
}

int main() {
    cin.tie(0)->sync_with_stdio(false);

    int n, m, k; cin >> n >> m >> k;

    for (int x, y, w, c, i = 0; i < m; ++i) {
        cin >> x >> y >> w >> c;
        e[i] = {x + 1, y + 1, w, c};
    }

    int x = -101;
    for (int w = 1 << 8; w; w >>= 1) {
        if (check(n, m, x + w) < k) x += w;
    }

    check(n, m, x + 1);
    cout << ans + k * (x + 1) << '\n';
    return 0;
}
