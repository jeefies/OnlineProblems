#include <iostream>
#include <set>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 9e5 + 7, MP = 2e8 + 7;

#define INSERT 0
#define QUERY 1
#define REMOVE 2

bool mem_start;
int root = 0, usage = 0;
int ls[N * 20], rs[N * 20], mn[N * 20];
multiset<int> pre[N * 20];
// vector<int> ls, rs, mn;
// std::vector< multiset<int> > pre;

void operation(int pos, int val, int type, int &p, int l = 1, int r = MP) {
	if (!p) p = ++usage;
	if (l == r) {
		if (type == INSERT) pre[p].insert(val);
		else pre[p].erase(pre[p].find(val));
		mn[p] = pre[p].size() ? *pre[p].begin() : MP;
		return ;
	} int mid = (l + r) >> 1;
	if (pos <= mid) operation(pos, val, type, ls[p], l, mid);
	else operation(pos, val, type, rs[p], mid + 1, r);
	mn[p] = min(mn[ls[p]], mn[rs[p]]);
}

int getMin(int L, int R = MP, int p = root, int l = 1, int r = MP) {
	if (!p || l > r) return MP;
	if (L <= l && r <= R) return mn[p];
	int mid = (l + r) >> 1;

	int res = MP;
	if (L <= mid) res = getMin(L, R, ls[p], l, mid);
	if (R > mid) res = min(res, getMin(L, R, rs[p], mid + 1, r));
	return res;
}

#define Insert(p, v) operation((p), (v), INSERT, root)
#define Remove(p, v) operation((p), (v), REMOVE, root)

multiset<int> shops[N];

int query(int pos) {

	int len = -1;
	for (int w = 1 << 28; w; w >>= 1) {
		int l = pos - len - w, r = pos + len + w;
		if (r <= MP && getMin(r + 1) < l) len += w;
	}
	return len + 1 > (int)1e8 ? -1 : len + 1;
}

inline void newshop(int pos, int type) {
	auto it = shops[type].insert(pos);
	if (it != shops[type].begin()) {
		auto p = std::prev(it);
		Insert(pos, *p);
		if (++it != shops[type].end()) {
			Remove(*it, *p);
			Insert(*it, pos);
		}
	}
}

inline void delshop(int pos, int type) {
	auto it = shops[type].find(pos);
	if (it != shops[type].begin()) {
		auto p = std::prev(it);
		Remove(pos, *p);
		if (++it != shops[type].end()) {
			Remove(*it, pos);
			Insert(*it, *p);
		}
		shops[type].erase(--it);
	} else shops[type].erase(it);
}

struct Ques {
	int op, x, t, type;
	bool operator< (const Ques &q) {
		return (t ^ q.t) ? t < q.t : op < q.op;
	}
} qs[N + N];

bool mem_end;
auto main(void) -> int {
	fprintf(stderr, "memery: %lf MB\n", (double)(&mem_start - &mem_end) / 8 / 1024 / 1024);
	cin.tie(0)->sync_with_stdio(false);

	int n, k, q;
	cin >> n >> k >> q;

	mn[0] = MP;
	// int _mul = 57;
	// ls.assign(n * _mul, 0), rs.assign(n * _mul, 0), mn.assign(n * _mul, MP);
	// pre.resize(n * _mul);

	for (int i = 1; i <= k; ++i) {
		newshop(-MP, i), newshop(MP, i);
	}

	int qc = 0;

	for (int x, t, l, r, i = 0; i < n; ++i) {
		cin >> x >> t >> l >> r;
		qs[qc++] = {INSERT, x, l, t};
		qs[qc++] = {REMOVE, x, r, t};
	}

	for (int x, t, i = 0; i < q; ++i) {
		cin >> x >> t;
		qs[qc++] = {QUERY, x, t, i};
	}

	sort(qs, qs + qc);

	static int res[N << 1];
	for (int i = 0; i < qc; ++i) {
		Ques &q = qs[i];

		if (q.op == INSERT) {
			newshop(q.x, q.type);
		} else if (q.op == REMOVE) {
			delshop(q.x, q.type);
		} else {
			res[q.type] = query(q.x);
		}
	}

	for (int i = 0; i < q; ++i) {
		cout << res[i] << '\n';
	}

	return 0;
}
