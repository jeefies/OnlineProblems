#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;
using lint = long long;
using poly = vector<lint>;

const int N = 6e5 + 7, mod = 998244353, g = 3, ig = 332748118;

lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

lint invx[N], fac[N], ifac[N];
void init(int n) {
	fac[0] = invx[0] = 1;
	for (int i = 1; i <= n; ++i) fac[i] = fac[i - 1] * i % mod;
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i) {
		ifac[i - 1] = ifac[i] * i % mod;
		invx[i] = ifac[i] * fac[i - 1] % mod;
	}
}

int rev[N];
void initRev(int n) {
	int lg = log2(n) - 1;
	for (int i = 1; i < n; ++i)
		rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << lg);
}

void NTT(poly &v, int n, int inv) {
	for (int i = 0; i < n; ++i) {
		if (rev[i] < i) swap(v[i], v[rev[i]]);
	}

	for (int u = 2; u <= n; u <<= 1) {
		lint omega = qpow(~inv ? g : ig, (mod - 1) / u);
		for (int i = 0, k = u >> 1; i < n; i += u) {
			lint w = 1;
			for (int j = 0; j < k; ++j, w = w * omega % mod) {
				lint s = v[i + j], t = v[i + j + k] * w % mod;
				v[i + j] = (s + t) % mod, v[i + j + k] = (s + mod - t) % mod;
			}
		}
	}

	if (inv == -1) {
		lint iv = qpow(n, mod - 2);
		for (int i = 0; i < n; ++i) (v[i] *= iv) %= mod;
	}
}

void polyInv(poly &v, int n, poly &ret) {
	poly fst(n * 2), sec(n * 2);

	ret[0] = qpow(v[0], mod - 2);
	initRev(2);
	for (int p = 2; p < n * 2; p <<= 1) {
		for (int i = 0, ie = p / 2; i < ie; ++i) fst[i] = ret[i] * 2 % mod;

		initRev(p << 1);
		for (int i = 0; i < p; ++i) sec[i] = v[i];
		NTT(sec, p << 1, 1), NTT(ret, p << 1, 1);
		for (int i = 0, ie = p << 1; i < ie; ++i)
			(sec[i] *= ret[i] * ret[i] % mod) %= mod;
		NTT(sec, p << 1, -1);

		for (int i = 0; i < p; ++i) {
			ret[i] = (fst[i] + mod - sec[i]) % mod;
			ret[i + p] = 0;
		}
	}
}

void polyDer(const poly &v, int n, poly &ret) {
	for (int i = 1; i < n; ++i) {
		ret[i - 1] = v[i] * i % mod;
	} ret[n - 1] = 0;
}

void polyInvDer(const poly &v, int n, poly &ret) {
	for (int i = 1; i < n; ++i) {
		ret[i] = v[i - 1] * invx[i] % mod;
	} ret[0] = 0;
}

void polyLn(poly &v, int n, poly &ret) {
	poly inv(n * 2), der(n * 2);
	polyDer(v, n, der);
	polyInv(v, n, inv);

	int O = n * 2;
	NTT(der, O, 1), NTT(inv, O, 1);
	for (int i = 0; i < O; ++i) der[i] = der[i] * inv[i] % mod;
	NTT(der, O, -1);

	polyInvDer(der, n, ret);
	fill(begin(ret) + n, end(ret), 0);
}

void polyExp(poly &v, int n, poly &ret) {
	poly ln(n * 2), tmp(n * 2);

	ret[0] = 1;
	for (int p = 2, pe = n * 2; p < pe; p <<= 1) {
		polyLn(ret, p, ln);
		for (int i = 0; i < p; ++i) tmp[i] = v[i];

		NTT(ret, p << 1, 1), NTT(ln, p << 1, 1), NTT(tmp, p << 1, 1);
		for (int i = 0; i < (p << 1); ++i)
			ret[i] = ret[i] * ((1 + mod - ln[i] + tmp[i]) % mod) % mod;
		NTT(ret, p << 1, -1);

		fill_n(begin(ret) + p, p, 0);
	} fill(begin(ret) + n, end(ret), 0);
}

namespace Cipolla {	
lint p = mod, i2;

template<typename T, typename Num>
inline T qpow(T a, Num x, Num p) {
	T r(1);
	for (; x; x >>= 1, a = (a * a) % p) {
		if (x & 1) r = (a * r) % p;
	}
	return r;
}

class exNum {
public:
	lint a, b;
	exNum(lint a) : a(a % p), b(0) { };
	exNum(lint a, lint b) : a(a % p), b(b % p) { }
	inline exNum operator * (const exNum & e) const {
		return exNum(a * e.a % p + b * e.b % p * i2, b * e.a + a * e.b);
	}
	
	inline exNum operator % (const lint &mod) const {
		return exNum(a % mod, b % mod);
	}
};

inline lint legendre(lint a) {
	a = (a % p + p) % p;
	return qpow(a, (p - 1) / 2, p);
}

inline lint findA(lint n) {
	for (lint a = 1; a < p; ++a) {
		if (legendre(a * a - n) == p - 1) return a;
	} return -1;
}

inline lint Cipolla(lint n) {
	n %= p;
	if (legendre(n) != 1) return -1;
	lint a = findA(n);
	i2 = a * a - n;
	exNum en = qpow(exNum(a, 1), (p + 1) / 2, p);
	return en.a < 0 ? en.a + p : en.a;
}
}

void polySqrt(poly &v, int n, poly &ret) {
	poly inv(n * 2), tmp(n * 2);
	int inv2 = qpow(2, mod - 2);
	
	ret[0] = Cipolla::Cipolla(v[0]);
	ret[0] = min(ret[0], -ret[0] + mod);
	for (int p = 2, pe = n * 2; p < pe; p <<= 1) {
		fill_n(begin(inv), p * 2, 0);
		
		for (int i = 0; i < p; ++i) tmp[i] = v[i];
		polyInv(ret, p, inv);
		
		/*
		NTT(inv, p << 1, 1), NTT(tmp, p << 1, 1), NTT(ret, p << 1, 1); 
		for (int i = 0; i < (p << 1); ++i)
			ret[i] = (tmp[i] + ret[i] * ret[i] % mod) % mod * inv[i] % mod * inv2 % mod;
		NTT(ret, p << 1, -1);
		*/
		
		NTT(inv, p << 1, 1), NTT(tmp, p << 1, 1);
		for (int i = 0; i < (p << 1); ++i)
			tmp[i] = (tmp[i] * inv[i]) % mod;
		NTT(tmp, p << 1, -1);
		
		for (int i = 0; i < p; ++i) {
			ret[i] = (ret[i] + tmp[i]) % mod * inv2 % mod;
		}
		
		fill_n(begin(ret) + p, p, 0);
	}
}

void polyPow(poly &p, int n, int k, int pk, poly &ret) {
	poly tmp(n * 2), ln(n * 2);
	
	int exi = 0;
	for (; exi < n; ++exi) if (p[exi] != 0) break;
	// printf("Exi %d\n", exi);
	
	for (int i = 0; i + exi < n; ++i) tmp[i] = p[i + exi];
	for (int i = n - exi; i < n; ++i) tmp[i] = 0;
	
	lint fst = tmp[0], inv = qpow(fst, mod - 2);
	// printf("fst %lld, inv %lld\n", fst, inv);
	for (int i = 0; i < n; ++i) (tmp[i] *= inv) %= mod; 
	
	// for (int i = 0; i < n; ++i) printf("tmp[%d] init %lld\n", i, tmp[i]);
	
	polyLn(tmp, n, ln);
	for (int i = 0; i < n; ++i) (ln[i] = ln[i] * k) %= mod;
	
	fill(begin(tmp), end(tmp), 0);
	polyExp(ln, n, tmp);
	
	// for (int i = 0; i < n; ++i) printf("tmp[%d] powto %lld\n", i, tmp[i]);
	
	lint mul = qpow(fst, pk);
	for (int i = 0; i < n; ++i) (tmp[i] *= mul) %= mod;
	for (int i = 0; i + 1ll * exi * k < n; ++i) ret[i + exi * k] = tmp[i];
}

/*
int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	
	auto read = [](){
		lint x = 0; char c;
		do c = getchar(); while (c < '0' || c > '9');
		do x = ((x * 10) + (c - '0')) % mod, c = getchar(); while ('0' <= c && c <= '9');
		return x % mod;
	};

	lint n = read(), k1 = 0, k2 = 0, flag = 0;
	{ char c;
	do c = getchar(); while (c < '0' || c > '9');
	do {
		k1 = ((k1 * 10) + (c - '0'));
		k2 = ((k2 * 10) + (c - '0'));
		if (k1 >= mod) flag = true;
		k1 %= mod, k2 %= mod - 1;
		c = getchar(); 
	} while ('0' <= c && c <= '9');
	}
	
	if (flag == true && k1 == 0) k1 = mod;
	
	int O = 1;
	while (O < n) O <<= 1;
	init(O * 2);
	
	poly v(O * 2), res(O * 2); 
	
	for (int i = 0; i < n; ++i) v[i] = read();
	
	if (flag == true && v[0] == 0) {
		for (int i = 0; i < n; ++i) cout << "0 ";
		return 0; 
	}
	
//	printf("k1, k2 = %lld, %lld\n", k1, k2);
	polyPow(v, O, k1, k2, res);
	for (int i = 0; i < n; ++i) cout << res[i] << ' ';
}
*/

void dpolyMul(poly &x, poly &y, int n, int m, poly &ret) {
	int N = max(n, m) * 2;
	init(N);

	int O = 1;
	while (O <= 2 * N) O <<= 1;

	poly e(O), re(O); x.resize(O), y.resize(O), ret.resize(O);

	for (int i = 0; i <= N; ++i) {
		e[i] = ifac[i];
		re[i] = (i & 1) ? mod - ifac[i] : ifac[i];
	}

	initRev(O);
	NTT(x, O, 1), NTT(y, O, 1), NTT(e, O, 1), NTT(re, O, 1);
	for (int i = 0; i < O; ++i) {
		printf("[%d] = %lld, %lld, %lld, %lld\n", i, e[i], re[i], x[i], y[i]);
	}

	for (int i = 0; i < O; ++i) {
		(x[i] *= e[i]) %= mod, (y[i] *= e[i]) %= mod;
		printf("x, y [%d] = %lld, %lld\n", i, x[i], y[i]);
	} NTT(x, O, -1), NTT(y, O, -1);

	for (int i = 0; i <= N; ++i) {
		ret[i] = x[i] * y[i] % mod * fac[i] % mod;
	} NTT(ret, O, 1);
	for (int i = 0; i < O; ++i) {
		(ret[i] *= re[i]) %= mod;
	} NTT(ret, O, -1);	
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n, m; cin >> n >> m;

	poly x(n + 1), y(m + 1), ret;

	for (int i = 0; i <= n; ++i) cin >> x[i];
	for (int i = 0; i <= m; ++i) cin >> y[i];

	dpolyMul(x, y, n, m, ret);
	for (int i = 0; i <= n + m; ++i) cout << ret[i] << ' ';
}
