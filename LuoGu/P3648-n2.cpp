#include <iostream>
#include <algorithm>

using namespace std;
using lint = long long;
const int N = 1e5 + 7;

int n, k;
lint a[N], sum[N];
lint f[207][N], g[207][N];

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> k;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
		sum[i] = sum[i - 1] + a[i];
	}

	for (int i = 1; i <= n; ++i)
		f[1][i] = sum[i] * (sum[n] - sum[i]);

	for (int t = 2; t <= k; ++t) {
		printf("T %d\n", t);

		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j < i; ++j) {
				lint cv = f[t - 1][j] + (sum[i] - sum[j]) * (sum[n] - sum[i]);
				if (cv >= f[t][i]) f[t][i] = cv, g[t][i] = j;
			}

			printf("%lld ", f[t][i]);
		}
		puts("");
	}

	lint ans = -1; int cur = 0;
	for (int i = k; i < n; ++i)
		if (f[k][i] > ans) ans = f[k][i], cur = i;
	cout << ans << '\n';

	for (int idx = k; idx > 0; --idx) {
		cout << cur << ' ';
		cur = g[idx][cur];
	}
}
