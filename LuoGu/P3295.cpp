#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e5 + 7;

int lg[N], grp[19][N], rnk[19][N];

int *fa, *rk;
inline void hot(int k) { fa = grp[k], rk = rnk[k]; }
inline int find(int x) {
	return fa[x] == x ? x : fa[x] = find(fa[x]);
}

inline void merge(int x, int y) {
	fa[find(y)] = find(x);
}

int main(void) {
	int n, m; cin >> n >> m;

	for (int i = 2; i <= n; ++i)
		lg[i] = lg[i >> 1] + 1;
	cerr << lg[n] << '\n';

	int t = lg[n];
	for (int k = 0; k <= t; ++k) {
		hot(k);
		for (int i = 1; i <= n; ++i)
			fa[i] = i;
	}

	for (int a, b, c, d, i = 1; i <= m; ++i) {
		cin >> a >> b >> c >> d;
		for (int k = lg[b - a + 1]; ~k; --k) {
			if (a + (1 << k) - 1 <= b) {
				hot(k), merge(a, c);
				a += (1 << k), c += (1 << k);
			}
		}
	}

	for (int j = t; j; --j) {
		for (int i = 1; i + (1 << j) - 1 <= n; ++i) {
			hot(j); int up = find(i);
			hot(j - 1); merge(i, up);
			merge(i + (1 << (j - 1)), up + (1 << (j - 1)));
		}
	}

	const int mod = 1000000007;
	int ans = 0;
	for (int i = 1; i <= n; ++i) {
		hot(0);
		if (fa[i] == i) ans = !ans ? 9 : 10ll * ans % mod;
	} cout << ans << '\n';
	return 0;
}
