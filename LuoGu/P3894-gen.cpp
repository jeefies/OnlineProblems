#include<bits/stdc++.h>
using namespace std;
int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int f[100005],n,m,siz[100005]; 
int randm(int x){return ((rand()%x)*(rand()%x))%x+1;}
int find(int now)
{
    if(f[now]==now)return now;
    return f[now]=find(f[now]);
}
const int W=100,N=5,Q=1000,SIZE=5;
//W为边权最大值，N为国家数，Q为询问数，SIZE为国家城市数。 
int main()
{
    srand(time(0));
    n=N;
    cout<<n<<"\n";
    for(int i=1;i<=n;i++)siz[i]=randm(SIZE)+2;
    for(int i=1,cnt,u,v,w;i<=n;i++)
    {
        cout<<siz[i]<<"\n";
        for(int j=0;j<=siz[i];j++)f[j]=j;
        cnt=0;
        while(cnt!=siz[i]-1)
        {
            u=randm(siz[i])-1;
            v=randm(siz[i])-1;
            w=randm(W);
            u=find(u);v=find(v);
            if(u!=v)
            {
                cout<<u<<" "<<v<<" "<<w<<"\n";
                f[u]=v;
                cnt++;
            }
        }
    }
    int q=Q;
    int s1,e1,s0,e0;
    cout<<q<<"\n";
    while(q--)
    {
        s0=randm(n);
        e0=randm(n);
        s1=randm(siz[s0])-1;
        e1=randm(siz[e0])-1;
        cout<<s0-1<<" "<<s1<<" "<<e0-1<<" "<<e1<<"\n";
    }
    return 0;
}
