#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;
using lint = long long;

const int N = 2e5 + 7;
const double eps = 1e-7;

int dcmp(double x, double y) {
	if (x - y > eps) return 1;
	if (y - x > eps) return -1;
	return 0;
}

struct Point {
	double x, y;

	Point operator + (const Point &p) const {
		return {x + p.x, y + p.y};
	}

	Point operator - (const Point &p) const {
		return {x - p.x, y - p.y};
	}
	
	Point operator * (const double mul) const {
		return {x * mul, y * mul};
	}

	Point operator / (const double div) const {
		return {x / div, y / div};
	}

	double operator ^ (const Point &p) const {
		return x * p.y - y * p.x;
	}

	double angle() const {
		return ::atan2(y, x);
	}

	double abs() const {
		return sqrt(x * x + y * y);
	}
} A[N];

bool slopeCmp(const Point &l, const Point &m, const Point &r) {
	return ((r - l) ^ (m - l)) >= 0;
}

int convexHull(Point *ps, int n) {
	for (int i = 1; i < n; ++i) {
		if (ps[i].x < ps[0].x || (ps[i].x == ps[0].x && ps[i].y < ps[0].y))
			swap(ps[i], ps[0]);
	}

	sort(ps + 1, ps + n, [&](const Point &l, const Point &r) {
		double al = (l - ps[0]).angle(), ar = (r - ps[0]).angle();
		return !dcmp(al, ar) ? ((l.x != r.x) ? l.x < r.x : l.y < r.y) : al < ar;
	});

	ps[n] = ps[0];
	static Point stk[N]; int top = 0;
	for (int i = 0; i < n; ++i) {
		while (top > 1 && slopeCmp(stk[top - 2], stk[top - 1], ps[i]))
			--top;
		stk[top++] = ps[i];
	}

	for (int i = 0; i < top; ++i)
		ps[i] = stk[i];
	return top;
}

double height(const Point &dot, const Point &st, const Point &ed) {
	const Point vec = ed - st;
	return (vec ^ (dot - st)) / vec.abs();
}

Point foot(const Point &dot, const Point &st, const Point &ed) {
	const Point vec = ed - st;
	double h = (vec ^ (dot - st)) / vec.abs();
	return dot + (Point){vec.y, -vec.x} / vec.abs() * h;
}

struct Rect {
	int i, l, m, r;
} rct[N];

int main(void) {
	// freopen("1.in", "r", stdin);

	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;

	double x, y;
	for (int i = 0; i < n; ++i) {
		cin >> x >> y;
		A[i] = {x, y};
	}

	n = convexHull(A, n);
	for (int i = 0; i < n; ++i) {
		// printf("Cvx %d = {%lf, %lf}\n", i, A[i].x, A[i].y);
		A[i + n + n] = A[i + n] = A[i];
	}

	double ans = 1e18;
	int l = 0, m = 1, r = 1, mi;
	for (int i = 0; i < n; ++i) {
		// 维护最上端点
		while (height(A[m + 1], A[i], A[i + 1]) > height(A[m], A[i], A[i + 1]) + eps)
			++m;
		double h = height(A[m], A[i], A[i + 1]);

		const Point vec = A[i + 1] - A[i];
		Point ed = A[i] + (Point){-vec.y, vec.x};
		while (height(A[l + 1], A[i], ed) > height(A[l], A[i], ed) + eps)
			++l;

		while (height(A[(l - 1 + n) % n], A[i], ed) > height(A[l], A[i], ed) + eps)
			l = (l - 1 + n) % n;

		double w = height(A[l], A[i], ed);

		ed = A[i] + (Point){vec.y, -vec.x};
		while (height(A[r + 1], A[i], ed) > height(A[r], A[i], ed) + eps)
			++r;

		w += height(A[r], A[i], ed);

		// printf("At line {%lf, %lf} -> {%lf, %lf}:\n", A[i].x, A[i].y, A[i + 1].x, A[i + 1].y);
		// printf("\tL {%lf, %lf}; M {%lf, %lf}; R {%lf, %lf}\n", A[l].x, A[l].y, A[m].x, A[m].y, A[r].x, A[r].y);;

		rct[i] = {i, l, m, r};
		if (w * h + eps < ans) {
			ans = w * h;
			mi = i;
		}	
	}

#define rounder eps
#define P(p) (p).x+rounder,(p).y+rounder
	printf("%.5lf\n", ans + rounder);

	l = rct[mi].l, m = rct[mi].m, r = rct[mi].r;
	Point lfoot = foot(A[l], A[mi], A[mi + 1]);
	Point rfoot = foot(A[r], A[mi], A[mi + 1]);
	Point vech = A[m] - foot(A[m], A[mi], A[mi + 1]);

	Point rect[4] = {lfoot, rfoot, rfoot + vech, lfoot + vech};
	int starter = 0;
	for (int i = 0; i < 4; ++i) {
		if (rect[i].y < rect[starter].y || (dcmp(rect[i].y, rect[starter].y) == 0 && rect[i].x < rect[starter].x))
			starter = i;
	}

	for (int i = 0; i < 4; ++i) {
		double x = rect[(i + starter) % 4].x, y = rect[(i + starter) % 4].y;
		if (fabs(x) < 1e-5) x = 0;
		if (fabs(y) < 1e-5) y = 0;
		printf("%.5lf %.5lf\n", x, y);
	}
}
