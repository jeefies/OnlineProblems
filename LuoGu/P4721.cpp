#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
using lint = long long;
using poly = vector<lint>;

const int N = 4e5 + 7, mod = 998244353, g = 3, ig = 332748118;

lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

void DIF(poly &v, int n) {
	for (int u = n, k = n >> 1; k; u >>= 1, k >>= 1) {
		printf("U block %d\n", u);
		lint omega = qpow(g, (mod - 2) / u);
		for (int i = 0; i < n; i += u) {
			lint w = 1;
			for (int j = 0; j < k; ++j, w = w * omega % mod) {
				lint s = v[i + j], t = v[i + j + k];
				printf("=> {%d, %d}\n", i + j, i + j + k);
				v[i + j] = (s + t) % mod, v[i + j + k] = (s + mod - t) % mod * w % mod;
			}
		}
	}
}

void IDIF(poly &v, int n) {
	for (int u = 2; u <= n; u <<= 1) {
		lint omega = qpow(ig, (mod - 2) / u);
		for (int i = 0, k = u >> 1; i < n; i += u) {
			lint w = 1;
			for (int j = 0; j < k; ++j, w = w * omega % mod) {
				lint s = v[i + j], t = v[i + j + k] * w % mod;
				v[i + j] = (s + t) % mod, v[i + j + k] = (s + mod - t) % mod;
			}
		}
	}

	lint iv = qpow(n, mod - 2);
	for (int i = 0; i < n; ++i) (v[i] *= iv) %= mod;
}

int main() {
	std::string cA, cB;
	std::cin >> cA >> cB;
	
	int O = 1, logO = 0;
	int n = cA.size(), m = cB.size();
	while (O < n + m) O <<= 1, ++logO;
	
	// std::cout << n << ' ' << m << ' ' << O << ' ' << logO << '\n';
	
	poly A(O + O), B(O);
	for (int i(0); i < n; ++i) A[n - i - 1] = cA[i] - '0'; 
	for (int i(0); i < m; ++i) B[m - i - 1] = cB[i] - '0';
	
	// for (int i(0); i < O; ++i) std::cout << A[i].real << ' ' << B[i].real << '\n'; 
	
	DIF(A, O), DIF(B, O);

	cout << "A: "; for (int i = 0; i < O; ++i) {
		cout << A[i] << ' ';
	} cout << '\n';
	
	for (int i(0); i < O; ++i) A[i] = A[i] * B[i];
	IDIF(A, O);
	
	int nm(n + m);
	for (int i(0); i <= nm; ++i) {
		if (A[i] >= 10) A[i + 1] += A[i] / 10, A[i] %= 10, nm += (i == nm);
	}
	while (!A[nm] && nm) --nm;
	while (nm >= 0) std::cout << A[nm--];
	return 0;
}
