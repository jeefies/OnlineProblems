#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>

using namespace std;
const int N = 2e5, mod = 1e4;

int s[N];
int nxt[N];
char mark[N];

void getKMP(int *s, int *kmp, int n) {
	fill_n(kmp + 1, n, 0);
	for (int j = 0, i = 2; i <= n; ++i) {
		while (j && s[i] != s[j + 1])
			j = kmp[j];
		if (s[i] == s[j + 1]) ++j;
		kmp[i] = j;
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n, t; cin >> n >> t; n %= mod;
	for (int i = 1; i <= t; ++i) {
		int len; cin >> len;
		for (int j = 1; j <= len; ++j)
			cin >> s[j];
		getKMP(s, nxt, len);

		fill_n(mark + 1, len, 0);
		for (int k = len; k; k = nxt[k]) mark[k] = true;

		int ans = 0;
		for (int k = 1, pow = n; k <= len; ++k, pow = pow * n % mod)
			if (mark[k]) (ans += pow) %= mod;
		printf("%04d\n", ans);
	}
}
