#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>

using namespace std;
using ldb = long double;

const int N = 3300;

struct Edge {
    int to;
    ldb w;
};
vector<Edge> G[N];

int n, m, cnt[N], inq[N], vis[N];
ldb dis[N];

bool SPFA(int x, ldb mid) {
    vis[x] = true;
    for (auto e : G[x]) {
        if (dis[e.to] > dis[x] + e.w - mid) {
            dis[e.to] = dis[x] + e.w - mid;
            if (vis[e.to]) {
                vis[e.to] = false; return true;
            } else if (SPFA(e.to, mid)) {
                vis[e.to] = false; return true;
            }
        }
    }

    return vis[x] = false;
}

bool check(ldb mid) {
    fill(dis + 1, dis + 1 + n, 0);
    for (int i = 1; i <= n; ++i)
        if (SPFA(i, mid)) return true;
    return false;
}

int main() {
    // freopen("test.in", "r", stdin);
    cin.tie(0)->sync_with_stdio(false);

    cin >> n >> m;

    int u, v; ldb w;
    for (int i = 1; i <= m; ++i) {
        cin >> u >> v >> w;
        G[u].push_back({v, w});
    }

    const ldb eps = 1e-8;
    ldb l = -3e10, r = 3e10;
    while (l + eps < r) {
        ldb mid = (l + r) / 2;
        if (check(mid)) r = mid;
        else l = mid;
    }

    printf("%.8Lf\n", l);
    return 0;
}

/*
fill(dis + 1, dis + 1 + n, (ldb)1e18);
    fill(cnt + 1, cnt + 1 + n, 1);
    fill(it + 1, it + 1 + n, 0);
    fill(inq + 1, inq + 1 + n, 0);
    dis[1] = 0;

    queue<int> q; q.push(1);

    while (q.size()) {
        int x = q.front(); q.pop();
        inq[x] = 0;

        for (auto e : G[x]) {
            if (dis[e.to] > dis[x] + e.w - mid) {
                dis[e.to] = dis[x] + e.w - mid;
                // printf("dis[%d] update to %Lf (by %d)\n", e.to, dis[e.to], x);
                if ((cnt[e.to] = cnt[x] + 1) > n) return true;
                if (!inq[e.to]) {
                    if (++it[e.to] >= n) return true;
                    inq[e.to] = 1, q.push(e.to);
                }
            }
        }
    }

    // printf("Check %Lf no negative ring\n", mid);
    return false;
*/