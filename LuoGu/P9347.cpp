#include <iostream>
#include <sstream>

using namespace std;

const int N = 2e6 + 3;

int arr[N];
int pla[N];
stringstream ss;

void Swap(int i, int j, int k) {
	ss << i << ' ' << j << ' ' << k << '\n';
	if (arr[i] > arr[k]) {
		// swap(i, j);
		pla[arr[i]] = j, pla[arr[j]] = i;
		swap(arr[i], arr[j]);
	} else {
		// swap(j, k);
		pla[arr[j]] = k, pla[arr[k]] = j;
		swap(arr[j], arr[k]);
	}
}

void work() {
	ss.clear();

	int n, L;
	cin >> n >> L;
	for (int i = 1; i <= n; ++i) {
		cin >> arr[i];
		pla[arr[i]] = i;
	}

	if (n == 1) {
		cout << "0\n";
		return;
	} else if (n == 2) {
		cout << (arr[1] == 1 ? "0\n" : "-1\n");
		return;
	}

	// never can do that
	if (arr[n] == 1) {
		cout << "-1" << '\n';
	}

	if (arr[1] != 1) {
		int k = pla[1] + 1;
		for (; k <= n; ++k) {
			if (arr[1] > arr[k]) break;
		}
		if (k <= n) {
			Swap(1, pla[1], k);
		} else {
			for (k = 2; k < pla[1]; ++k) {
				if (arr[1] > arr[k]) break;
			}
			if (k < pla[1]) Swap(1, k, pla[1]);
		}
	}

	/*
	int tot = 0;
	// firstly put 1 at the first place
	// special situation!
	if (arr[1] == 2) {
		// swap(arr[1], arr[2]);
		// i = 1, j = 2, k = where 1 is
		if (arr[2] == 1) {
			// special situation, need change 1 to another place
			++tot;
			ss << "1 2 3\n"; // swap(2, 3)
			swap(arr[2], arr[3]);
			pla[arr[2]] = 2, pla[arr[3]] = 3;
		}

		++tot;
		ss << "1 2 " << pla[1] << '\n';
		swap(arr[1], arr[2]);
		pla[arr[1]] = 1, pla[arr[2]] = 2;
	}

	// now arr[1] > 2
	if (arr[1] != 1) {
		// find one smaller than arr[1]
		int k = pla[1] + 1;
		for (; k <= n; ++k) {
			if (arr[k] < arr[1]) break;
		}
	}
	*/

	for (int i = 1; i <= n; ++i) {
		if (arr[i] != i) {
			int x = arr[i];
			// need swap(arr[i], arr[x]);
			// and surely that x > arr[i]
		}
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	int T;
	cin >> T;
	while (T--) work();
}
