#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e5 + 3;
const int mx = 1e5;

#define lowbit(x) ((x)& -(x))
class BIT {
private:
	int b[N];
public:
	void update(int i, int x) {
		for (; i <= mx; i += lowbit(i)) b[i] = max(b[i], x);
	}
	int query(int i, int r = 0) {
		for (; i; i -= lowbit(i)) r = max(r, b[i]);
		return r;
	}
	void clear(int i) { for (; i <= mx; i += lowbit(i)) b[i] = 0; }
} bit;

struct Val {
	int v, mx, mn, ans, id;
} a[N];
inline bool cmpByMx(const Val &x, const Val &y) { return x.mx < y.mx; }
inline bool cmpByV(const Val &x, const Val &y) { return x.v < y.v; }
inline bool cmpByI(const Val &x, const Val &y) { return x.id < y.id; }

inline void cdq(int l, int r) {
	cout << "In CDQ " << l << ", " << r << '\n';
	if (l == r) return;
	int mid = (l + r) >> 1;
	cdq(l, mid);

	sort(a + l, a + mid + 1, cmpByMx), sort(a + mid + 1, a + r + 1, cmpByV);
	int j = l;
	for (int i = mid + 1; i <= r; ++i) {
		while (j <= mid && a[j].mx <= a[i].v) {
			bit.update(a[j].v, a[j].ans);
			++j;
		} a[i].ans = max(a[i].ans, bit.query(a[i].mn) + 1);
	}
	for (int i = l; i < j; ++i) bit.clear(a[i].v);
	sort(a + l, a + r + 1, cmpByI), cdq(mid + 1, r);
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	int n, m; cin >> n >> m;
	for (int x, i = 1; i <= n; ++i) {
		cin >> x; a[i] = {x, x, x, 1, i};
	}
	for (int x, y, i = 1; i <= m; ++i) {
		cin >> x >> y;
		a[x].mx = max(a[x].mx, y);
		a[x].mn = min(a[x].mn, y);
	}
	cdq(1, n);
	int res = 0;
	for (int i = 1; i <= n; ++i) {
		res = max(res, a[i].ans);
	} cout << res << '\n';
}
