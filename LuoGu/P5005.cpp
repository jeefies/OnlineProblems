#include <iostream>
#include <algorithm>

using namespace std;
const int N = 101, S = 0b111111, mod = 1e9 + 7;

int stat[N][S + 1][S + 1];

bool valid(int i, int j, int k) {
}

int main(void) {
	int x, y;
	cin >> x >> y;

	int sits = (1 << y) - 1;
	for (int i = 0; i <= sits; ++i)
		stat[1][i][0] = 1;

	for (int c = 2; c <= x; ++c) {
		for (int i = 0; i <= sits; i++) {
			for (int j = 0; j <= sits; ++j)
				for (int k = 0; k <= sits; ++k)
					if (valid(k, j, i)) (stat[c][i][j] += stat[c - 1][j][k]) %= mod;
		}
	}

	int sum = 0;
	for (int i = 0; i <= sits; ++i) {
		for (int j = 0; j <= sits; ++j)
			(sum += stat[x][i][j]) %= mod;
	}
	cout << sum << '\n';
	return 0;
}
