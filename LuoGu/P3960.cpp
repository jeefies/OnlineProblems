#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 3e5 + 2, M = 12e6;
typedef long long lint;

lint n, m, q;
lint flat(int i, int j) {
	return 1ll * (i - 1) * m + j;
}

int rc[M], lc[M], deln[M];
int usage = 0;
inline lint popkth(int k, int p, lint l, lint r) {
	if (l == r) return l;
	int mid = (l + r) >> 1, tmp = mid - l + 1 - deln[lc[p]];
	return k <= tmp ? popkth(k, lc[p], l, mid) : popkth(k - tmp, rc[p], mid + 1, r);
}

inline void update(int k, int &p, lint l, lint r) {
	if (!p) p = ++usage; ++deln[p];
	if (l == r) return; int mid = (l + r) >> 1;
	k <= mid ? update(k, lc[p], l, mid) : update(k, rc[p], mid + 1, r);
}

int rt[N];
vector<lint> back[N];
int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	cin >> n >> m >> q;
	int lim = max(n, m) + q;
	
	for (int x, y, i = 0; i < q; ++i) {
		cin >> x >> y;
		if (y == m) {
			lint pos = popkth(x, rt[n + 1], 1, lim); update(pos, rt[n + 1], 1, lim);
			lint res = pos <= n ? (pos * m) : back[n + 1][pos - n - 1];
			back[n + 1].push_back(res);
			cout << res << '\n';
		} else {
			lint pos = popkth(y, rt[x], 1, lim); update(pos, rt[x], 1, lim);
			lint res = pos < m ? (flat(x, pos)) : back[x][pos - m];
			pos = popkth(x, rt[n + 1], 1, lim); update(pos, rt[n + 1], 1, lim);
			lint res2 = pos <= n ? (pos * m) : back[n + 1][pos - n - 1];
			back[x].push_back(res2); back[n + 1].push_back(res);
			cout << res << '\n';
		}
	}
	return 0;
}
