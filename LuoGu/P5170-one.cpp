#include <iostream>
#include <algorithm>

using namespace std;
const int mod = 998244353, inv2 = 499122177;
typedef long long lint;

lint f(lint n, lint a, lint b, lint c) {
	if (a == 0) // recursive boundary
		return (n + 1) * (b / c) % mod;
	if (n == 0)
		return (b / c) % mod;
	if (a >= c || b >= c)
		return (((n + 1) * (b / c) % mod + n * (n + 1) % mod * inv2 % mod * (a / c) % mod) % mod
			+ f(n, a % c, b % c, c)) % mod;
	lint m = (a * n + b) / c;
	return (n * m % mod + mod - f(m - 1, c, c - b - 1, a)) % mod;
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int T, n, a, b, c; cin >> T;
	while (T--) {
		cin >> n >> a >> b >> c;
		cout << f(n, a, b, c) << " 0 0" << '\n';
	}
}

