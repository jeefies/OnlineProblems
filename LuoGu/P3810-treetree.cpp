#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 2e5 + 3, M = 1e7;

int lc[M], rc[M], cnt[M], usage = 0;
class SegTree {
private:
	int root;
public:
	void init() { root = ++usage; }
	void add(int v, int i, int l, int r) { add(v, i, l, r, root); }
	int add(int v, int i, int l, int r, int &p) {
		if (!p) p = ++usage;
		if (l == r) return cnt[p] += v;
		int mid = (l + r) >> 1; cnt[p] += v;
		return i <= mid ? add(v, i, l, mid, lc[p]) : add(v, i, mid + 1, r, rc[p]);
	}

	int query(int L, int R, int l, int r) { return query(L, R, l, r, root); }
	int query(int L, int R, int l, int r, int p) {
		if (!p) return 0;
		if (L <= l && r <= R) return cnt[p];
		int mid = (l + r) >> 1, res = 0;
		if (L <= mid) res = query(L, R, l, mid, lc[p]);
		if (R > mid) res += query(L, R, mid + 1, r, rc[p]);
		return res;
	}
} st[N];

#define lowbit(i) ((i) & -(i))
int n, k;
void update(int x, int y, int v) {
	for (; x <= k; x += lowbit(x)) st[x].add(v, y, 1, k);
}

int query(int x, int y, int v = 0) {
	for (; x; x -= lowbit(x))
		v += st[x].query(1, y, 1, k);
	return v;
}

struct Node {
	int x, y, z;
	inline bool operator < (const Node &n) const {
		return (x ^ n.x) ? (x < n.x) : (y ^ n.y ? y < n.y : z < n.z);
	}
	inline bool operator == (const Node &n) const {
		return x == n.x && y == n.y && z == n.z;
	}
} a[N];

int but[N];
int main() {
	cin.tie(0)->sync_with_stdio(false);
	cin >> n >> k;
	for (int i = 1; i <= k; ++i) st[i].init();
	for (int x, y, z, i = 1; i <= n; ++i) {
		cin >> x >> y >> z; a[i] = {x, y, z};
	} sort(a + 1, a + 1 + n);

	for (int cnt = 0, i = 1; i <= n; ++i) {
		++cnt;
		if (a[i] == a[i + 1]) continue;
		update(a[i].y, a[i].z, cnt);
		int res = query(a[i].y, a[i].z);
		but[res] += cnt;
		cnt = 0;
	}

	for (int i = 1; i <= n; ++i) {
		cout << but[i] << '\n';
	}
	return 0;
}
