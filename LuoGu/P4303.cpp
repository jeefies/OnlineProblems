#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>

using namespace std;
const int N = 1e5;
#define T 5

int n, nT; 
#define lowbit(i) ((i) & -(i))
class BIT {
private:
	int b[N];
public:
	void update(int i, int x) {
		for (; i <= nT; i += lowbit(i)) b[i] = max(b[i], x);
	}
	int query(int i, int r = 0) {
		for (; i; i -= lowbit(i)) r = max(r, b[i]);
		return r;
	}
} bit;

vector<int> pla[N];
int main() {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n; nT = n * T;
	for (int x, i = 1; i <= nT; ++i) {
		cin >> x; pla[x].push_back(i);
	}

	for (int i = 1; i <= n; ++i) {
		reverse(pla[i].begin(), pla[i].end());
	}

	for (int x, i = 1; i <= nT; ++i) {
		cin >> x;
		for (int p : pla[x]) {
			bit.update(p, bit.query(p - 1) + 1);
		}
	} cout << bit.query(nT) << '\n';
}
