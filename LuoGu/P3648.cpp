#include <iostream>
#include <algorithm>
#include <deque>

using namespace std;
using lint = long long;
const int N = 1e5 + 7;

int n, k;
lint a[N], sum[N];
lint f[207][N], g[207][N];

#define X(j) (sum[n] + sum[j])
#define Y(j) ((sum[j] * sum[n]) - f[t - 1][j])
#define V(j, k) (-X(j) * k + Y(j))

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> k;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
		sum[i] = sum[i - 1] + a[i];
	}

	for (int t = 1; t <= k; ++t) {
		deque<int> deq; deq.push_back(0);

		auto slope = [&](const int i, const int j) -> double {
			if (sum[i] == sum[j]) return -1e18;
			return (double)(f[t - 1][j] - sum[j] * sum[j] - f[t - 1][i] + sum[i] * sum[i]) / (double)(sum[i] - sum[j]);
		};

		for (int i = 1; i <= n; ++i) {
			while (deq.size() > 1 && slope(deq[0], deq[1]) <= sum[i]) deq.pop_front();
			f[t][i] = f[t - 1][deq[0]] + sum[deq[0]] * (sum[i] - sum[deq[0]]);
			g[t][i] = deq[0];
#define deqs (deq.size())
			while (deq.size() > 1 && slope(deq[deqs - 2], deq[deqs - 1]) >= slope(deq[deqs - 1], i)) deq.pop_back();
			deq.push_back(i);
		}
	}

	cout << f[k][n] << '\n';
	for (int idx = k, cur = n; idx > 0; --idx) {
		cur = g[idx][cur];
		cout << cur << ' ';
	}
}
