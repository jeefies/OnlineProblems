#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using namespace std;
const int N = 2e6, BASE = 131;
typedef unsigned long long hashInt;

hashInt ofs[N] = {1};
void prepare(int n) {
	for (int i = 1; i <= n; ++i) ofs[i] = ofs[i - 1] * BASE;
}

class WBLT {
private:
	hashInt val[N];
	int siz[N], ch[N][2];
	int root, usage;
#define lc(p) ch[p][0]
#define rc(p) ch[p][1]
public:
	inline int _new(int v) {
		int p = ++usage; siz[p] = 1;
		ch[p][0] = ch[p][1] = 0, val[p] = v;
		return p;
	}

	inline void update(int p) {
		siz[p] = siz[lc(p)] + siz[rc(p)];
		val[p] = val[lc(p)] * ofs[siz[rc(p)]] + val[rc(p)];
	}

	WBLT() : root(0), usage(0) {
	}

	void rotate(int p, int s) {
		if (!p) return;
		swap(lc(ch[p][s]), rc(ch[p][s]));
		swap(lc(p), rc(p));
		swap(ch[ch[p][s ^ 1]][s ^ 1], ch[p][s]);
		update(ch[p][s ^ 1]), update(p);
	}

	void fix(int p) {
		if (!p || siz[p] <= 3) return;
		constexpr float alcha = 0.29;
		int d;
		if (siz[lc(p)] < siz[p] * alcha) d = 1; // right too big
		else if (siz[rc(p)] < siz[p] * alcha) d = 0; // left too big
		else return; // balanced
		int k = ch[p][d];
		if (siz[ch[k][d ^ 1]] * (1 - alcha) >= siz[k] * (1 - 2 * alcha)) // child not balanced
			rotate(k, d ^ 1);
		return rotate(p, d);
	}

	void insertAtKth(int k, int x) {
		if (!root) {
			root = _new(x);
			return;
		}

		stack<int> stk;
		int p = root;
		while (true) {
			stk.push(p);
			if (siz[p] == 1) {
				lc(p) = _new(val[p]), rc(p) = _new(x); break;
			} else if (k <= siz[lc(p)]) p = lc(p);
			else k -= siz[lc(p)], p = rc(p);
		} while (stk.size()) update(stk.top()), fix(stk.top()), stk.pop();
	}

	void change(int k, int x) {
		stack<int> stk; int p = root;
		while (p) {
			stk.push(p);
			if (siz[p] == 1) val[p] = x, p = 0;
			else if (k <= siz[lc(p)]) p = lc(p);
			else k -= siz[lc(p)], p = rc(p);
		} stk.pop(); // pop p
		while (stk.size()) update(stk.top()), stk.pop();
	}

	hashInt query(int l, int r) { return query(l, r, 1, siz[root], root); }
	hashInt query(int l, int r, int L, int R, int p) {
		// printf("Query [%d, %d] in [%d, %d] (p %d)\n", l, r, L, R, p);
		if (l == L && R == r) return val[p];
		int ls = siz[lc(p)];
		if (r <= L + ls - 1) return query(l, r, L, L + ls - 1, lc(p));
		if (l >= L + ls) return query(l, r, L + ls, R, rc(p));
		hashInt res = 0;
		if (l <= L + ls - 1) res = query(l, L + ls - 1, L, L + ls - 1, lc(p));
		if (r >= L + ls) res *= ofs[r - L - ls + 1], res += query(L + ls, r, L + ls, R, rc(p));
		return res;
	}
} wblt;

int n;
int Qlen(int x, int y) {
	int len = 0;
	for (int w = 1 << 20; w; w >>= 1) {
		if (x + w - 1 > n || y + w - 1 > n) continue;
		// printf("Checking [%d, %d] and [%d, %d]: ", x, x + w - 1, y, y + w - 1);
		if (wblt.query(x, x + w - 1) == wblt.query(y, y + w - 1))
			len += w, x += w, y += w; // , printf("SUCCESS!\n");
		// else printf("FAILED\n");
	}
	return len;
}
int main() {
	prepare(110000);

	string s; cin >> s;
	n = s.size();
	for (int i = 0; i < n; ++i)
		wblt.insertAtKth(i + 1, s[i]);

	int q; cin >> q;
	char op, d;
	for (int x, y, i = 0; i < q; ++i) {
		cin >> op >> x;
		if (op == 'R') cin >> d, wblt.change(x, d);
		else if (op == 'I') cin >> d, wblt.insertAtKth(x, d), ++n;
		else cin >> y, cout << Qlen(x, y) << '\n';
	}
}
