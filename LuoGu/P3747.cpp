#include <iostream>
#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <assert.h>

using namespace std;
const int N = 5e4 + 3, BLOCK = 512;

int n, m, p, c;

class LP {
private:
	int v, mod, q;
	int sp[N], qp[N];
	void init(int _v, _mod) {
		v = _v, mod = _mod;
		q = (int)sqrt(mod * 2);
		// printf("BQ got %d\n", q);
		for (int i = 0, x = 1; i <= q; ++i, x = 1ll * x * v % mod) {
			qp[i] = x;
		}

		int b = qp[q];
		for (int i = 0, x = 1; i <= q; ++i, x = 1ll * x * b % mod) {
			sp[i] = x;;
		}
	}

	int qpow(int x) {
		return (int)(1ll * sp[x / q] * qp[x % q] % mod);
	}
} lp[50];

int getPhi(int x) {
	static unordered_map<int, int> cache;
	auto it = cache.find(x);
	if (it != cache.end())
		return it->second;

	int p = x, phi = x;
	for (int i = 2; i * i <= x; ++i) {
		if (x % i == 0) {
			phi = phi / i * (i - 1);
			do x /= i; while (x % i == 0);
		}
	}
	if (x > 1) phi = phi / x * (x - 1);
	return cache[p] = phi;
}

int MMC = 0;
int a[N], mcnt[N];
int nxt[N];
int lab[N], but[N / BLOCK + 1];

typedef pair<int, int> pii;
pii getAns(int x, int t, int p) {
	if (!t) 
		return make_pair(x % p, x >= p);
	if (p == 1) 
		return make_pair(0, 1);
	pii tmp = getAns(x, t - 1, phi[p]);
}

inline int jump(int x) {
	return nxt[x] == x ? x : nxt[x] = jump(nxt[x]);
}

void initBut() {
	for (int i = 1; i <= n; ++i) {
		nxt[i] = i;
		lab[i] = i / BLOCK;
		but[lab[i]] = (but[lab[i]] + a[i]) % p;
	} nxt[n + 1] = n + 1;
}

void modify(int l, int r) {
	printf("In modify [%d, %d]\n", l, r);
	for (int i = jump(l); i <= r; i = jump(i + 1)) {
		printf("\tModify at %d\n", i);
		int ori = a[i], cur = LP::qpow(a[i]);
		if (ori == cur) printf("\t\t!! Mark Unchanged\n"),
			nxt[i] = i + 1;
		else { printf("\t\tNeed update to %d!!\n", cur);
			int dif = (cur - ori + p) % p;
			but[lab[i]] = (but[lab[i]] + dif) % p;
			a[i] = cur;
		}
	}
}

int query(int l, int r) {
	int ans = 0;

	if (lab[l] + 1 >= lab[r]) {
		for (int i = l; i <= r; ++i)
			ans = (ans + a[i]) % p;
		return ans;
	}
	
	while (l % BLOCK) {
		ans = (ans + a[l++]) % p;
	}

	while ((r + 1) % BLOCK) {
		ans = (ans + a[r--]) % p;
	}

	for (int b = l / BLOCK, be = r / BLOCK; b <= be; ++b) {
		ans = (ans + but[b]) % p;
	}

	return ans;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> m >> p >> c;
	LP::init();

	int tmp = p;
	while (tmp != 1)
		lp[++MMC].init(c, tmp), tmp = getPhi(tmp);

	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	} initBut();

	int op, l, r;
	for (int i = 1; i <= m; ++i) {
		cin >> op >> l >> r;
		if (op == 0) {
			modify(l, r);
		} else {
			cout << query(l, r) << '\n';
		}
	}
}
