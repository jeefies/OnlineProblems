#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;
const int N = 1e4 + 3, M = 5e6 + 3, INF = 0x7FFFFFFF;

class ISAP {
private:
	struct Edge {
		int to, flow, next;
	} edge[M];
	int head[N], tot;
	int n, s, t;
	int dep[N], gap[N << 1];
public:
	ISAP() : tot(1) {}
	void clear(int n) { tot = 1; fill_n(head, n + 1, 0); }

	inline void add(int u, int v, int f) {
		// printf("add %d <-%d-> %d\n", u, f, v);
		edge[++tot] = {v, f, head[u]}, head[u] = tot;
		edge[++tot] = {u, 0, head[v]}, head[v] = tot;
	}

	inline void init(int n, int s, int t) {
		this->n = n, this->s = s, this->t = t;
		fill(dep, dep + n + 1, -1);
		fill(gap, gap + n + 1, 0);
		
		gap[(dep[t] = 0)] = 1;
		queue<int> q; q.push(t);

		while (q.size()) {
			int x = q.front(); q.pop();
			for (int i = head[x]; i; i = edge[i].next) {
				int to = edge[i].to;
				if (dep[to] == -1) 
					++gap[(dep[to] = dep[x] + 1)], q.push(to);
			}
		}
	}

	inline int sap(int x, int flow) {
		if (x == t) return flow;

		int rest = flow;
		for (int i = head[x]; i; i = edge[i].next) {
			auto &e = edge[i];
			if (dep[e.to] + 1 == dep[x] && e.flow > 0) {
				int push = sap(e.to, min(e.flow, rest));
				if (push) {
					e.flow -= push, edge[i ^ 1].flow += push;
					rest -= push;
				}
				if (!rest) return flow;
			}
		}

		if (--gap[dep[x]] == 0) dep[s] = n + 1;
		return ++gap[++dep[x]], flow - rest;
	}

	inline int maxflow() {
		int flow = 0;
		while (dep[s] <= n) flow += sap(s, INF);
		return flow;
	}
} isap;

int a[N], b[N];
signed main() {
	cin.tie(0)->sync_with_stdio(false);

	int n, sum = 0; cin >> n;
	for (int i = 1; i <= n; ++i) cin >> a[i], sum += a[i];
	for (int i = 1; i <= n; ++i) cin >> b[i], sum += b[i];

	int m; cin >> m;
	int s = 0, t = n + m + m + 1;
	for (int k, c1, c2, x, i = 1; i <= m; ++i) {
		cin >> k >> c1 >> c2;
		isap.add(s, n + i, c1), isap.add(n + m + i, t, c2);
		sum += c1 + c2;
		while (k--) {
			cin >> x;
			isap.add(n + i, x, INF), isap.add(x, n + m + i, INF);
		}
	}

	for (int i = 1; i <= n; ++i) {
		isap.add(s, i, a[i]), isap.add(i, t, b[i]);
	}

	isap.init(t, s, t);
	int mincut = isap.maxflow();
	// cout << "sum: " << sum << ", mincut: " << mincut << '\n';
	cout << sum - mincut << '\n';
	return 0;
}
