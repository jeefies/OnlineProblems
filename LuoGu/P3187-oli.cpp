#include<cstdio>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cctype>
#include<vector>
#include<stack>
#include<queue>
using namespace std;
#define enter puts("") 
#define space putchar(' ')
#define Mem(a, x) memset(a, x, sizeof(a))
#define rg register
typedef long long ll;
typedef double db;
const int INF = 0x3f3f3f3f;
const db eps = 1e-10;
const int maxn = 5e4 + 5;
inline ll read()
{
  ll ans = 0;
  char ch = getchar(), last = ' ';
  while(!isdigit(ch)) last = ch, ch = getchar();
  while(isdigit(ch)) ans = (ans << 1) + (ans << 3) + ch - '0', ch = getchar();
  if(last == '-') ans = -ans;
  return ans;
}
inline void write(ll x)
{
  if(x < 0) x = -x, putchar('-');
  if(x >= 10) write(x / 10);
  putchar(x % 10 + '0');
}

int n;
struct Point
{
  db x, y; int id;     //id跟题目无关,是为了方便调试用的
  Point operator - (const Point& oth)const
  {
    return (Point){x - oth.x, y - oth.y, 0};
  }
  Point operator + (const Point& oth)const
  {
    return (Point){x + oth.x, y + oth.y, 0};
  }
  db operator * (const Point& oth)const
  {
    return x * oth.y - oth.x * y;
  }
  db operator ^ (const Point& oth)const  //这是点积,不是位运算……
  {
    return x * oth.x + y * oth.y;
  }
  Point operator * (db d)
  {
    return (Point){x * d, y * d, 0};
  }
  friend inline db dis(const Point& A)
  {
    return A.x * A.x + A.y * A.y;
  }
  friend inline Point rot(const Point& A)    //顺时针旋转90度
  {
    return (Point){A.y, -A.x};
  }
}p[maxn], S;

bool cmp(Point& A, Point& B)
{
  db s = (A - S) * (B - S);
  if(fabs(s) > eps) return s > eps;
  return dis(A - S) < dis(B - S) - eps;
}
int st[maxn], top = 0;
void Graham()
{
  int id = 1;
  for(int i = 2; i <= n; ++i)
    if(p[i].x < p[id].x - eps || (fabs(p[i].x - p[id].x) < eps && p[i].y < p[id].y - eps)) id = i;
  if(id != 1) swap(p[id].x, p[1].x), swap(p[id].y, p[1].y), swap(p[id].id, p[1].id);
  S.x = p[1].x, S.y = p[1].y;
  sort(p + 2, p + n + 1, cmp);
  st[++top] = 1;
  for(int i = 2; i <= n; ++i)
    {
      while(top > 1 && (p[st[top]] - p[st[top - 1]]) * (p[i] - p[st[top - 1]]) < eps) top--;
      st[++top] = i;
    }
}

struct Rec
{
  Point R, U, L;
}r[maxn];
db area(Point A, Point B, Point C)
{
  return (B - A) * (C - A);
}
db pho(Point A, Point B, Point C, Point D)
{
  return (B - A) ^ (D - C);
}
inline int nxt(int x)
{
  if(++x > top) x = 1;
  return x;
}
void rota1()
{
  for(int i = 1, j = 3, k = 2; i <= top; ++i)
    {
      while(nxt(j) != i && area(p[st[i]], p[st[i + 1]], p[st[j]]) < area(p[st[i]], p[st[i + 1]], p[st[nxt(j)]]) - eps) j = nxt(j);
      r[i].U = p[st[j]];
      while(nxt(k) != i && pho(p[st[i]],  p[st[i + 1]], p[st[k]], p[st[nxt(k)]]) > eps) k = nxt(k);
      r[i].R = p[st[k]];
    }
}
inline int pre(int x)
{
  x--;
  if(!x) x = top;
  return x;
}
void rota2()   //单独维护最左点
{
  for(int i = top, j = top; i; --i)
    {
      while(pre(j) != i + 1 && pho(p[st[i]], p[st[i + 1]], p[st[j]], p[st[pre(j)]]) < eps) j = pre(j);
      r[i].L = p[st[j]];
    }
}

Point Ans[5];

int main()
{
	freopen("1.in", "r", stdin);

  n = read();
  for(int i = 1; i <= n; ++i) scanf("%lf%lf", &p[i].x, &p[i].y);
  for(int i = 1; i <= n; ++i) p[i].id = i;
  Graham(); st[top + 1] = st[1];
  rota1(); rota2();
  db ans = (db)INF * (db)INF;
  for(int i = 1; i <= top; ++i)
    {
      //printf("Line:%d %d R:%d U:%d L:%d\n", p[st[i]].id, p[st[i + 1]].id, r[i].R.id, r[i].U.id, r[i].L.id);
      Point AB = p[st[i + 1]] - p[st[i]];
      db a = sqrt(dis(AB));
      db b = (AB ^ (r[i].L - p[st[i]])) / a, c = (AB ^ (r[i].R - p[st[i + 1]])) / a;
      b = fabs(b), c = fabs(c);
      db l = a + b + c;
      db h = AB * (r[i].U - p[st[i]]) / a;
      h = fabs(h);
      db s = l * h;
      //printf("**%.3lf %.3lf %.3lf %.3lf %.3lf", a, b, c, l, h);
      //printf("@%.3Lf\n", s);
      if(s < ans - eps)
	{
	  ans = s;
	  Ans[0] = p[st[i + 1]] + AB * (c / a);
	  Ans[1] = Ans[0] + rot(AB) * (-h / a);
	  Ans[2] = Ans[1] + rot(Ans[1] - Ans[0]) * (-l / h);
	  Ans[3] = Ans[2] + rot(Ans[2] - Ans[1]) * (-h / l);
	  /*Ans[1] = Ans[0] + (r[i].R - Ans[0]) * (h / dis(Ans[0] - r[i].R));  //这就是我原来的写法，点重合会$gg$
	  Ans[2] = Ans[1] + (r[i].U - Ans[1]) * (l / dis(r[i].U - Ans[1]));
	  Ans[3] = Ans[2] + (r[i].L - Ans[2]) * (h / dis(r[i].L - Ans[2]));*/
	}
    }
  printf("%.5lf\n", ans);
  int id = 0;
  for(int i = 1; i < 4; ++i)
    if(Ans[i].y < Ans[id].y - eps || (fabs(Ans[i].y - Ans[id].y) < eps && Ans[i].x < Ans[id].x - eps)) id = i;
  for(int i = 0; i < 4; ++i)
    {
      db x = Ans[(id + i) % 4].x, y = Ans[(id + i) % 4].y;
      if(fabs(x) < 1e-5) x = 0;
      if(fabs(y) < 1e-5) y = 0;
      printf("%.5lf %.5lf\n", x, y);
    }
  return 0;
}
