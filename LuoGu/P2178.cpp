#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 2e6 + 7;

int sa[N << 1], tmp[2][N], cnt[N];

int *getSA(string &str) {
	int n = str.size();

	int m = 128;
	int *x = tmp[0], *y = tmp[1];
	for (int i = 1; i <= n; ++i) 
		++cnt[x[i] = str[i - 1]];
	for (int i = 1; i <= m; ++i) 
		cnt[i] += cnt[i - 1];
	for (int i = n; i; --i)
		sa[cnt[x[i]]--] = i;
	
	for (int p, k = 1; k < n; k <<= 1) {
		p = 0;
		for (int i = n - k + 1; i <= n; ++i) y[++p] = i;
		for (int i = 1; i <= n; ++i) {
			if (sa[i] > k) y[++p] = sa[i] - k;
		}

		for (int i = 1; i <= m; ++i) 
			cnt[i] = 0;
		for (int i = 1; i <= n; ++i)
			++cnt[x[i]];
		for (int i = 1; i <= m; ++i)
			cnt[i] += cnt[i - 1];
		for (int i = n; i; --i)
			sa[cnt[x[y[i]]]--] = y[i], y[i] = 0;

		swap(x, y);

		p = 0;
		for (int i = 1; i <= n; ++i) {
			x[sa[i]] = (y[sa[i]] == y[sa[i - 1]] && y[sa[i] + k] == y[sa[i - 1] + k])
				? p : ++p;
		}

		if (p >= n) break;
		m = p;
	}

	return x;
}

int H[N];
void getH(string &s, int *rk) {
	int n = s.size();
	for (int k = 0, i = 1; i <= n; ++i) {
		if (k > 0) --k;
		int l = i - 1, r = sa[rk[i] - 1] - 1;
		while (s[l + k] == s[r + k])
			++k;
		H[rk[i]] = k;
	}
}

struct ANS {
	long long x, y;
} ans[N];
int tast[N];

int grp[N];
long long mx[N], mn[N], siz[N];
int find(int x) {
	return x == grp[x] ? x : grp[x] = find(grp[x]);
}

long long cur = -1e18, tot = 0;
void merge(int x, int y) {
	x = find(x), y = find(y);
	if (x == y) return ;

	tot += siz[x] * siz[y];
	cur = max(cur, max(max(mx[x] * mx[y], mx[x] * mn[y]), max(mn[x] * mx[y], mn[x] * mn[y])));
	mx[x] = max(mx[x], mx[y]);
	mn[x] = min(mn[x], mn[y]);
	siz[x] += siz[y];
	grp[y] = x;
}

void prtAt(string &str, int p) {
	printf("H[%d] = %d = LCP(%d, %d); weight = %d * %d = %lld\n", p, H[p], sa[p], sa[p - 1], tast[sa[p]], tast[sa[p - 1]], 1ll * tast[sa[p]] * tast[sa[p - 1]]);
	for (unsigned i = sa[p]; i <= str.size(); ++i) {
		putchar(str[i - 1]);
	} puts("");
}

struct Node {
	int i, v;
	bool operator < (const Node &n) const {
		return v > n.v;
	}
};

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n;
	string s;

	cin >> n >> s;

	int *rk = getSA(s);
	getH(s, rk);

	for (int i = 1; i <= n; ++i) {
		grp[i] = i;
		cin >> tast[i];
		mx[i] = mn[i] = tast[i];
		siz[i] = 1;
	}

	vector<Node> vec;
	for (int i = 1; i <= n; ++i) {
		vec.push_back({i, H[i]});
	} sort(begin(vec), end(vec));

	unsigned p = 0;
	for (int h = n - 1; ~h; --h) {
		while (p < vec.size() && vec[p].v >= h) {
			int i = vec[p].i;
			merge(sa[i], sa[i - 1]);
			++p;
		}

		ans[h] = {tot, cur};
	}

	for (int i = 0; i < n; ++i) {
		if (ans[i].x > 0) printf("%lld %lld\n", ans[i].x, ans[i].y);
		else puts("0 0");
	}

	return 0;
}

