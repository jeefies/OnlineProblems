#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;
const int M = 501, N = 501;
const long double eps = 1e-6;

struct Item {
	long double mat[M];
	int cost;
	inline long double& operator[] (const int &i) { return mat[i]; }
	inline bool operator < (const Item &i) const {
		return cost < i.cost;
	}
} a[N];

int n, m, p[N];
int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> m;
	double tmp;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) 
			cin >> tmp, a[i][j] = tmp;
	}
	for (int i = 1; i <= n; ++i)
		cin >> a[i].cost;
	sort(a + 1, a + 1 + n);

	int cnt = 0, cost = 0;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= n; ++j) {
			if (fabs(a[i][j]) < eps) continue;
			if (!p[j]) {
				p[j] = i;
				++cnt, cost += a[i].cost;
				break;
			}

			double div = a[i][j] / a[p[j]][j];
			for (int k = j; k <= m; ++k)
				a[i][k] -= div * a[p[j]][k];
		}
	}

	cout << cnt << ' ' << cost << '\n';
	return 0;
}
