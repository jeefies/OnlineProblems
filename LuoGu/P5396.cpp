#include <iostream>
#include <algorithm>
#include <vector>

// NEED O2!!!!!
// O(n logn logk) !!!!!
using namespace std;
const int N = 6e5;
const int mod = 167772161, g = 3, ig = 55924054;

typedef long long lint;
typedef vector<lint> poly; 

lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

int rev[N];
void initRev(int n) {
	int lp = 0;
	for (int i = 2; i < n; i <<= 1) ++lp;
	for (int i = 1; i < n; ++i) 
		rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << lp);
}

void NTT(poly &v, int O, int inv) {
	for (int i = 0; i < O; ++i)
		if (rev[i] < i) swap(v[i], v[rev[i]]);
	
	for (int u = 2; u <= O; u <<= 1) {
		lint omega = qpow((~inv ? g : ig), (mod - 1) / u);
		for (int i = 0; i < O; i += u) {
			lint w = 1;
			for (int j = 0, k = (u >> 1); j < k; ++j, w = w * omega % mod) {
				lint s = v[i + j], t = v[i + j + k] * w % mod;
				v[i + j] = (s + t) % mod, v[i + j + k] = (s + mod - t) % mod;
			}
		}
	}
	
	if (inv == -1) {
		lint iv = qpow(O, mod - 2);
		for (int i = 0; i < O; ++i) v[i] = v[i] * iv % mod;
	}
}

lint fac[N] = {1}, ifac[N];
inline void prepare(int n) {
	for (int i = 1; i <= n; ++i) fac[i] = fac[i - 1] * i % mod;
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i) ifac[i - 1] = ifac[i] * i % mod;
	
	/*
	clog << "prepare: " << n << '\n'; 
	for (int i = 1; i <= n; ++i) {
		clog << fac[i] << "(" << ifac[i] << ") ";
	} clog << '\n';
	*/
}

void log(const char * s, const poly &v, int n) {
	clog << s << ": [";
	for (int i = 0; i + 1 < n; ++i) {
		clog << v[i] << ", ";
	} clog << v[n - 1] << "]\n";
}

int n, k;
poly A(N), B(N);
int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	cin >> n >> k;

	int O = 1, p = k;
	while (O <= n * 2) O <<= 1;	
	prepare(O);
	
	A[0] = 1;
	int lim = O >> 1;
	for (int i = 1; i < lim; ++i) B[i] = ifac[i];
	// log("B", B, lim);
	
	initRev(O);
	while (k) {
		NTT(B, O, 1);
		if (k & 1) {
			NTT(A, O, 1);
			for (int i = 0; i < O; ++i) A[i] = A[i] * B[i] % mod;
			NTT(A, O, -1);
			fill_n(A.begin() + lim, lim, 0);
			// log("Time A to", A, lim); 
		}
		for (int i = 0; i < O; ++i) B[i] = B[i] * B[i] % mod;
		NTT(B, O, -1);
		fill_n(B.begin() + lim, lim, 0);
		k >>= 1;
		// log("one times to", B, lim);
	}
	
	for (int i = 0, ifp = ifac[p]; i <= n; ++i) {
		cout << A[i] * ifp % mod * fac[i] % mod << ' ';
	}
	return 0;
}
