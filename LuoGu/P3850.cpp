#include <iostream>
#include <algorithm>
#include <vector>
#include <random>
#include <ctime>
#include <string>

using namespace std;
const int N = 2e5;
typedef unsigned int uint;

std::mt19937 gen;
std::uniform_int_distribution<uint> dist(1, 0xFFFFFFFF);

class Treap {
private:
	uint lc[N], rc[N];
	uint siz[N], pri[N];
	uint usage, root;
	string val[N];

	inline void update(int p) {
		siz[p] = siz[lc[p]] + siz[rc[p]] + 1;
	}

	inline void splitByRank(uint p, uint k, uint &x, uint &y) {
		if (!p) return (void)(x = y = 0);
		if (siz[lc[p]] < k)
			x = p, splitByRank(rc[p], k - siz[lc[p]] - 1, rc[p], y);
		else
			y = p, splitByRank(lc[p], k, x, lc[p]);
		update(p);
	}

	inline uint merge(uint x, uint y) {
		if (!x | !y) return x | y;
		if (pri[x] > pri[y]) {
			rc[x] = merge(rc[x], y);
			return update(x), x;
		} else {
			lc[y] = merge(x, lc[y]);
			return update(y), y;
		}
		return -1; // NOT REACHED
	}
public:
	void init(int p, const string &s) {
		val[p] = s, lc[p] = rc[p] = 0;
		siz[p] = 1, pri[p] = dist(gen);
	}
	Treap() : usage(0), root(0) {}
	inline void insert(uint k, const string &s) {
		uint p = ++usage, x = 0, y = 0; init(p, s);
		splitByRank(root, k, x, y);
		root = merge(x, merge(p, y));
	}

	inline void push(const string &s) {
		uint p = ++usage; init(p, s);
		root = merge(root, p);
	}

	string& kth(uint k) {
		uint x = 0, y = 0;
		splitByRank(root, k, x, y);
		uint p = x;
		while (rc[p]) p = rc[p];
		return merge(x, y), val[p];
	}
} treap;

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	string tmp;
	while (n--) {
		cin >> tmp; treap.push(tmp);
	}

	int m, x; cin >> m;
	while (m--) {
		cin >> tmp >> x;
		treap.insert(x, tmp);
	}

	int q; cin >> q;
	while (q--) {
		cin >> x;
		cout << treap.kth(x + 1) << '\n';
	}
	return 0;
}
