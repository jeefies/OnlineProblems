#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int K = 52;

void logS(const char *s, int a[K][K], int n) {
	cout << s << '\n';
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j)
			cout << a[i][j] << ' ';
		cout << '\n';
	}
}

int A[K][K], R[K][K], T[K][K];
int main() {
	int n, p, s, r;
	cin >> n >> p >> s >> r;
	const int mod = p;
	// make Matrix
	for (int i = 0; i < s; ++i) ++A[i][i], ++A[i][i ? i - 1 : s - 1], R[i][i] = 1;
	long long exp = 1ll * n * s;
	while (exp) {
		if (exp & 1) {
			for (int i = 0; i < s; ++i) fill_n(T[i], s, 0);
			for (int i = 0; i < s; ++i) for (int j = 0; j < s; ++j) for (int k = 0; k < s; ++k) 
				T[i][j] = (T[i][j] + 1ll * A[i][k] * R[k][j] % mod) % mod;
			for (int i = 0; i < s; ++i) copy_n(T[i], s, R[i]);
		}
		for (int i = 0; i < s; ++i) fill_n(T[i], s, 0);
		for (int i = 0; i < s; ++i) for (int j = 0; j < s; ++j) for (int k = 0; k < s; ++k)
			T[i][j] = (T[i][j] + 1ll * A[i][k] * A[k][j] % mod) % mod;
		for (int i = 0; i < s; ++i) copy_n(T[i], s, A[i]);
		exp >>= 1;
	}
	cout << R[0][r] << '\n';
	return 0;
}
