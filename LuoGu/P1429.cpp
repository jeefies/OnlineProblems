#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;
const int N = 5e5 + 7;

struct Point {
    int x, y;
} p[N], tmp[N];

double dis(const Point &a, const Point &b) {
    double dx = a.x - b.x, dy = a.y - b.y;
    return sqrt(dx * dx + dy * dy);
}

double divide(int l, int r) {
    if (r - l < 2) return 1e18;
    if (r - l == 2) return dis(p[l], p[l + 1]);

    int mid = (l + r) >> 1;
    double d = min(divide(l, mid), divide(mid, r));

    int idx = 0;
    for (int i = l; i < r; ++i) {
        if (fabs(p[i].x - p[mid].x) < d)
            tmp[idx++] = p[i];
    }

    sort(tmp, tmp + idx, [](const Point &a, const Point &b) {
        return a.y < b.y;
    });

    for (int i = 0; i < idx; ++i) {
        for (int j = i + 1; j < idx && tmp[i].y + d > tmp[j].y; ++j)
            d = min(d, dis(tmp[i], tmp[j]));
    }

    return d;
}

int main() {
    // freopen("test.in", "r", stdin);
    cin.tie(0)->sync_with_stdio(false);

    int n; cin >> n;
    for (int x, y, i = 0; i < n; ++i) {
        cin >> x >> y; p[i] = {x, y};
    }
    
    sort(p, p + n, [](const Point &a, const Point &b) {
        return a.x < b.x;
    });

    cout << divide(0, n) << '\n';
    return 0;
}