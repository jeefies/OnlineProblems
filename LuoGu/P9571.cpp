#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>

using namespace std;

map<int, multiset<int> > but;

int main() {
    cin.tie(0)->sync_with_stdio(false);

    int n; cin >> n;

    int tot = 0;
    int c, k, b;
    for (int i = 1; i <= n; ++i) {
        cin >> c >> k >> b;
        if (c == 1) {
            but[k].insert(b);
            ++tot;
        } else if (c == 2) {
            cout << tot - but[k].size() << '\n';
        } else {
            for (auto it = begin(but); it != end(but);) {
                if (it->first != k) {
                	tot -= it->second.size();
                    it = but.erase(it);
                } else {
                	tot -= it->second.count(b);
                	it->second.erase(b);
                    ++it;
                }
            }
        }
    }
}
