#include <iostream>
#include <algorithm>
#include <sstream>

using namespace std;
const int N = 1e7 + 3;

bool notp[N];
long long phi[N];
int prm[N], siz = 0;
void getPhi(int n) {
	register int i, j;
	for (i = 1; i <= n; ++i) {
		phi[i] = i;
	}
	for (i = 2; i * i <= n; ++i) {
		for (j = i * i; j <= n; j += i)
			notp[j] = 1;
	}

	for (int i = 2; i <= n; ++i) {
		if (!notp[i]) {
			prm[siz++] = i;
			for (j = i; j <= n; j += i)
				phi[j] = phi[j] / i * (i - 1);
		}
	}

	// prefix
	for (i = 1; i <= n; ++i) {
		phi[i] += phi[i - 1];
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	getPhi(n);
	long long ans = 0;
	for (int i = 0; i < siz; i++) {
		ans += 2 * phi[n / prm[i]] - 1;
	} cout << ans << '\n';
	return 0;
}
