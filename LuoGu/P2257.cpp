#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e7 + 3;

vector<int> prms;
int mob[N], notp[N];
void getMob(int n) {
	mob[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i])
			mob[i] = -1, prms.push_back(i);

		for (int p : prms) {
			int ip = i * p;
			if (ip > n) break;
			notp[ip] = 1;
			mob[ip] = -mob[i];
			if (i % p == 0) {
				mob[ip] = 0; 
				break;
			}
		}
	}
}

int f[N];
void getF(int n) {
	for (int p : prms) {
		for (int i = 1; i * p <= n; ++i)
			f[i * p] += mob[i];
	}

	for (int i = 2; i <= n; ++i)
		f[i] += f[i - 1];
}

void work() {
	int n, m; cin >> n >> m;
	int u = n < m ? n : m;
	long long v = 0;
	for (int l = 1, r; l <= u; l = r + 1) {
		r = min(n / (n / l), m / (m / l));
		v += 1ll * (f[r] - f[l - 1]) * (n / l) * (m / l);
	}
	cout << v << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	getMob(1e7), getF(1e7);
	int T; cin >> T;
	while (T--) work();
}
