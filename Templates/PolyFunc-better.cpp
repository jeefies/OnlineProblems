#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;
const int mod = 998244353, g = 3, ig = 332748118;
const int N = 5e5;
typedef long long lint;
typedef vector<lint> poly;

inline lint qpow(lint a, int x) {
	lint r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

lint fac[N] = {1}, ifac[N], invx[N] = {1};
void prepare(int n) {
	for (int i = 1; i <= n; ++i)
		fac[i] = fac[i - 1] * i % mod;
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i) {
		ifac[i - 1] = ifac[i] * i % mod;
		invx[i] = ifac[i] * fac[i - 1] % mod;
	}
}

int rev[N];
void initRev(int n) {
	int lp = 0;
	for (int i = 2; i < n; i <<= 1) ++lp;
	for (int i = 0; i < n; ++i) rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << lp);
}

void NTT(poly &v, int O, int inv) {
	for (int i = 0; i < O; ++i) {
		if (i < rev[i]) swap(v[i], v[rev[i]]);
	}

	for (int u = 2; u <= O; u <<= 1) {
		lint omega = qpow((~inv ? g : ig), (mod - 1) / u);
		for (int i = 0, k = u >> 1; i < O; i += u) {
			lint w = 1;
			for (int j = 0; j < k; ++j, w = w * omega % mod) {
				lint s = v[i + j], t = v[i + j + k] * w % mod;
				v[i + j] = (s + t) % mod, v[i + j + k] = (s + mod - t) % mod;
			}
		}
	}

	if (inv == -1) {
		lint iv = qpow(O, mod - 2);
		for (int i = 0; i < O; ++i)
			v[i] = v[i] * iv % mod;
	}
}

poly invC(N), invT(N);
void polyInv(poly &A, poly &ret, int n) {
	int O = 1;
	while (O < n * 2) O <<= 1;
	ret.assign(O, 0);

	ret[0] = qpow(A[0], mod - 2);
	for (int p = 2, lp = 2; p < O; p <<= 1, ++lp) {
		for (int i = 0; i < p; ++i)
			invC[i] = 2 * ret[i] % mod;

		invT.assign(ret.begin(), ret.begin() + (p >> 1));
		NTT(invT, p, 1);
		for (int i = 0; i < p; ++i) invT[i] = invT[i] * invT[i] % mod;
		NTT(invT, p, -1);

		NTT(ret, p << 1, 1), NTT(invT, p << 1, 1);
		for (int i = 0; i < (p << 1); ++i)
			invT[i] = invT[i] * ret[i] % mod;
		NTT(invT, p << 1, -1);
	}
}
