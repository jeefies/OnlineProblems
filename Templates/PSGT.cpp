#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e6 + 3;

struct Node {
	Node *lp, *rp;
	int siz, fact;
	int val, exi;
};

class SGT {
private:
	Node pool[N];
	Node *root, poolptr;
public:
	SGT() { poolptr = pool; }
	void insert(int x) { insert(root, x); };
	void insert(Node* &p, int x) {
		if (p == nullptr) {
			p = ++poolptr;
		}
	}
};
