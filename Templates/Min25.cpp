#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <cmath>

using namespace std;
const int N = 2e5 + 3, mod = 1e9 + 7;
typedef long long lint;

int sq, n;

vector<bool> notp(N);
lint p[N], psiz = 0;
lint psum[N], p2sum[N];
void getPrime(int n) {
	for (int i = 2; i <= n; ++i) {
		if (!notp[i]) 
			p[++psiz] = i;
		for (int j = 1; j <= psiz && p[j] * i <= n; ++j) {
			notp[p[j] * i] = true;
			if (i % p[j] == 0) break;
		}
	}

	for (int i = 1; i <= psiz; ++i) {
		psum[i] = (psum[i - 1] + p[i]) % mod;
		p2sum[i] = (p2sum[i - 1] + p[i] * p[i] % mod) % mod;
	}
}

lint id1[N], id2[N];
lint w[N]; // the wber need to be proceeded.
lint& getID(lint x) {
	return (x <= sq) ? id1[x] : id2[n / x];
}

// x prefix sum
inline lint f1(lint x) {
	x %= mod;
	return x * (x + 1) / 2 % mod;
}

// x^2 prefix sum
inline lint f2(lint x) {
	x %= mod;
	return x * (x + 1) % mod * (2 * x % mod + 1) % mod * 166666668 % mod;
}

lint g1[N], g2[N];
lint S(lint x, int j) {
	if (p[j] > x) return 0;
	lint ans = ((g2[getID(x)] - g1[getID(x)] + mod) % mod -
			(p2sum[j] - psum[j] + mod) % mod + mod) % mod;
	for (int k = j + 1; k <= psiz && p[k] * p[k] <= x; ++k) {
		for (lint e = 1, pe = p[k]; pe <= x; pe *= p[k], ++e) {
			ans += pe % mod * (pe % mod - 1) % mod * (S(x / pe, k) + (e > 1)) % mod;
			ans %= mod;
		}
	}
	// printf("S(%lld, %d) got %lld\n", x, j, ans);
	return ans;
}

int main(void) {
	cin >> n; sq = sqrt(n);
	getPrime(sq);

	// prepare g
	int m = 0;
	for (lint l = 1, r; l <= n; l = r + 1) {
		r = (n / (n / l)), w[++m] = n / l;
		g1[m] = f1(n / l) - 1, g2[m] = f2(n / l) - 1; // init g(n / l, 0);
		getID(n / l) = m;
	}

	// dp init g
	for (int i = 1; i <= psiz; ++i) {
		// here n = w[j].
		for (int j = 1; j <= m && p[i] * p[i] <= w[j]; ++j) {
			g1[j] = (g1[j] - p[i] * (g1[getID(w[j] / p[i])] - psum[i - 1]) % mod + mod) % mod;
			g2[j] = (g2[j] - p[i] * p[i] % mod * (g2[getID(w[j] / p[i])] - p2sum[i - 1]) % mod + mod) % mod;
			// printf("g At %d, %d(%lld): {%lld, %lld}\n", i, j, w[j], g1[j], g2[j]);
		}
	}

	cout << (S(n, 0) + 1) % mod << '\n';
}
