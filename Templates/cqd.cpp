#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 2e5 + 3;

struct node {
	int x, y, z, id, same, ans;
} a[N], uni[N];

#define lowbit(i) ((i) & -(i))
class BIT {
private:
	int b[N], mx;
public:
	inline void init(int upv) { mx = upv; }
	void clear() { fill(b, b + mx + 1, 0); }
	inline void update(int i, int v) {
		for (; i <= mx; i += lowbit(i)) b[i] += v;
	}
	inline int query(int i, int res = 0) {
		for (; i; i -= lowbit(i)) res += b[i];
		return res;
	}
} bit;

inline bool cmpByX(const node &a, const node &b) {
	return (a.x ^ b.x) ? (a.x < b.x) : (a.y ^ b.y ? (a.y < b.y) : a.z < b.z);
}
inline bool cmpByY(const node &a, const node &b) {
	return (a.y ^ b.y) ? (a.y < b.y) : (a.z < b.z);
}
inline bool operator == (const node &a, const node &b) {
	return a.x == b.x && a.y == b.y && a.z == b.z;
}

inline void cdq(int l, int r) {
	if (l == r) return;
	int mid = (l + r) >> 1;
	cdq(l, mid), cdq(mid + 1, r);
	sort(a + l, a + mid + 1, cmpByY); sort(a + mid + 1, a + r + 1, cmpByY);
	int j = l;
	for (int i = mid + 1; i <= r; ++i) {
		while (j <= mid && a[j].y <= a[i].y) {
			bit.update(a[j].z, a[j].same), ++j;
		} a[i].ans += bit.query(a[i].z);
	}
	for (int i = l; i < j; ++i) bit.update(a[i].z, -a[i].same);
}

int but[N];
int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	int n, k, mx = 0; cin >> n >> k;
	for (int x, y, z, i = 1; i <= n; ++i) {
		cin >> x >> y >> z; uni[i] = {x, y, z, i, 0};
		mx = max(mx, z);
	} sort(uni + 1, uni + 1 + n, cmpByX);
	bit.init(mx);
	
	int m = 0;
	for (int cnt = 0, i = 1; i <= n; ++i) {
		++cnt;
		if (uni[i] == uni[i + 1]) continue;
		a[++m] = uni[i];
		a[m].same = cnt, cnt = 0;
	}
	
	cdq(1, m);
	for (int i = 1; i <= m; ++i) {
		but[a[i].ans + a[i].same - 1] += a[i].same;
	}
	for (int i = 0; i < n; ++i) {
		cout << but[i] << '\n';
	}
	return 0;
}