#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int N = 303, mod = 1e9 + 7;

int qpow(int a, int x) noexcept {
	int r = 1;
	for (; x; x >>= 1, a = 1ll * a * a % mod)
		if (x & 1) r = 1ll * r * a % mod;
	return r;
}

int M[N][N]; // M = D - A
int det(int n) noexcept {
	int ans = 1;
	for (int i = 2; i < n; ++i) {
		// get none zero line
		for (int j = i + 1; j <= n; ++j) {
			if (!M[i][i] && M[j][i]) 
				ans = -ans, swap(M[i], M[j]);
		}

		int inv = qpow(M[i][i], mod - 2);
		for (int j = i + 1; j <= n; ++j) {
			// clear row i
			int fact = 1ll * M[j][i] * inv % mod;
			for (int k = i; k <= n; ++k) {
				M[j][k] -= 1ll * M[i][k] * fact % mod;
				M[j][k] %= mod;
			}
		}
	}

	for (int i = 2; i <= n; ++i)
		ans = 1ll * ans * M[i][i] % mod;
	return ans < 0 ? ans + mod : ans;
}

int main(void) noexcept {
	cin.tie(0)->sync_with_stdio(false);

	int n, m, t; cin >> n >> m >> t;
	for (int u, v, w, i = 1; i <= m; ++i) {
		cin >> u >> v >> w;
		if (t == 1) {
			// D[v][v] += w, A[u][v] += w;
			(M[v][v] += w) %= mod;
			(M[u][v] -= w) %= mod;
		} else {
			(M[u][u] += w) %= mod;
			(M[v][u] -= w) %= mod;
			(M[v][v] += w) %= mod;
			(M[u][v] -= w) %= mod;
		}
	} cout << det(n) << '\n';
	return 0;
}
