#include <iostream>
#include <algorithm>
#include <vector>
#include <random> 
#include <ctime> 

using namespace std;
const int N = 2e6 + 3, DC = 1e5 + 3, INF = 0x7FFFFFF0;

struct Node {
	int lp, rp;
	int siz, fact;
	int val, exi;
};

class SGT {
private:
	int lc[N], rc[N], siz[N], val[N], cnt[N];
	int usage; vector<int> del; 
	
	std::mt19937 gen;
	std::uniform_real_distribution<> rd;
public:
	SGT() : gen(time(NULL)), rd(0.0, 1.0) {}
	inline int pull(int x) {
		int p = del.empty() ? ++usage : del.back();
		if (del.size()) del.pop_back();
		lc[p] = rc[p] = 0, siz[p] = cnt[p] = 1, val[p] = x;
		return p;
	}
	
	inline void update(int p) {
		siz[p] = siz[lc[p]] + siz[rc[p]] + cnt[p];
	}
	
	vector<int> FTP, FTV, FTS; 
	inline void flatten(int p) {
		if (lc[p]) flatten(lc[p]);
		if (cnt[p] > 0) FTP.push_back(p), FTV.push_back(val[p]), FTS.push_back(cnt[p]);
		else del.push_back(p);
		if (rc[p]) flatten(rc[p]);
	}
	
	inline int rebuild(int l, int r) {
		if (l > r) return 0;
		if (l == r) {
			int p = FTP[l]; lc[p] = rc[p] = 0, cnt[p] = siz[p] = FTS[l], val[p] = FTV[l];
			return p;
		} int mid = (l + r) >> 1;
		int p = FTP[mid]; cnt[p] = FTS[mid], val[p] = FTV[mid];
		lc[p] = rebuild(l, mid - 1), rc[p] = rebuild(mid + 1, r);
		return update(p), p;
	}
	void tryRebuild(int &p) {
		if (siz[p] == 1 || rd(gen) * siz[p] * 3 >= 1) return;
		FTP.clear(), FTV.clear(), FTS.clear();
		flatten(p);
		p = rebuild(0, FTP.size() - 1);
	}
	
	inline void insert(int &p, int x) {
		if (!p) p = pull(x);
		else if (val[p] == x) ++cnt[p];
		else insert(x < val[p] ? lc[p] : rc[p], x);
		update(p); tryRebuild(p); 
	}
	
	inline void remove(int &p, int x) {
		if (!p) return;
		if (val[p] == x) { if (cnt[p] > 0) --cnt[p]; }
		else remove(x < val[p] ? lc[p] : rc[p], x);
		update(p);
	}
	
	inline int count(int p, int x) {
		if (!p) return 0;
		if (x < val[p]) return count(lc[p], x);
		else if (x > val[p]) return siz[lc[p]] + cnt[p] + count(rc[p], x);
		return siz[lc[p]]; // cnt[p];
	}
	
	inline int rank(int p, int x) {
		return count(p, x) + 1;
	}
	
	inline int kth(int p, int k) {
		if (k <= siz[lc[p]]) return kth(lc[p], k);
		else if (k <= siz[lc[p]] + cnt[p]) return val[p];
		return kth(rc[p], k - siz[lc[p]] - cnt[p]);
	}
	
	inline int pre(int p, int k) {
		return kth(p, count(p, k));
	}
	
	inline int post(int p, int k) {
		return kth(p, count(p, k + 1) + 1);
	}
} sgt;

/*
int main() {
	freopen("P3369_7.in", "r", stdin);
	freopen("P3369.out", "w", stdout);
	cin.tie(0)->sync_with_stdio(false);
	
	int q, rt = 0; cin >> q;
	sgt.insert(rt, -2147483647), sgt.insert(rt, 2147483647);
	for (int op, x, i = 1; i <= q; ++i) {
		cin >> op >> x;
//		cout << "OP " << op << ' ' << x << '\n';
		switch (op) {
			case 1: sgt.insert(rt, x); break;
			case 2: sgt.remove(rt, x); break;
			case 3: cout << sgt.rank(rt, x) - 1 << '\n'; break;
			case 4: cout << sgt.kth(rt, x + 1) << '\n'; break;
			case 5: cout << sgt.pre(rt, x) << '\n'; break;
			case 6: cout << sgt.post(rt, x) << '\n'; break;
		}
	}
	return 0;
}
*/

int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	int s, n, last = 0, ans = 0, rt = 0;
	sgt.insert(rt, -2147483647), sgt.insert(rt, 2147483647);
	
	cin >> s >> n;
	for (int x, i = 0; i < s; ++i) {
		cin >> x; sgt.insert(rt, x);
	}
	
	for (int op, x, i = 0; i < n; ++i) {
		cin >> op >> x; x ^= last;
		switch (op) {
			case 1: sgt.insert(rt, x); break;
			case 2: sgt.remove(rt, x); break;
			case 3: ans ^= last = sgt.rank(rt, x) - 1; break;
			case 4: ans ^= last = sgt.kth(rt, x + 1); break;
			case 5: ans ^= last = sgt.pre(rt, x); break;
			case 6: ans ^= last = sgt.post(rt, x); break;
			default: clog << "Error!\n";
		}
	} cout << ans << '\n';
}
