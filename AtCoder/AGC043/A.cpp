#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;

char s[110][110];
int n, m, f[110][110];

int main() {
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i)
        scanf("%s", s[i] + 1);

    f[1][1] = s[1][1] == '#';
    for (int i = 2; i <= m; ++i)
        f[1][i] = f[1][i - 1] + (s[1][i - 1] == '.' && s[1][i] == '#');
    for (int i = 2; i <= n; ++i)
        f[i][1] = f[i - 1][1] + (s[i - 1][1] == '.' && s[i][1] == '#');
    for (int i = 2; i <= n; ++i)
        for (int j = 2; j <= m; ++j)
            f[i][j] = min(f[i - 1][j] + (s[i - 1][j] == '.' && s[i][j] == '#'),
                          f[i][j - 1] + (s[i][j - 1] == '.' && s[i][j] == '#'));
    printf("%d\n", f[n][m]);
    return 0;
}
