#include <iostream>
#include <algorithm>

using namespace std;
const int N = 2e6 + 3;

char s[N];

#define C(n, m) (((n) & (m)) == (m))

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n; scanf("%d%s", &n, s + 1);

	if (n == 1)
		return puts(s + 1), 0;

	int cnt[3] = {0, 0, 0};
	// diff one time
	for (int i = 1; i < n; ++i) {
		s[i] = s[i] - s[i + 1];
		if (s[i] < 0) s[i] = -s[i];
		++cnt[s[i]];
	}

	if (!cnt[1]) {
		for (int i = 1; i < n; ++i)
			s[i] >>= 1;
	}

	int ans = 0;
	for (int i = 1; i < n; ++i)
		ans ^= C(n - 2, i - 1) * (s[i] & 1);
	cout << (cnt[1] ? ans : (ans << 1)) << '\n';
}
