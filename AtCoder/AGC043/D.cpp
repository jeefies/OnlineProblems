#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 6e3 + 3, OFFSET = 6e3 + 3;

int f[N][N + OFFSET];

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n, mod; cin >> n >> mod;
	int n3 = n * 3;

	f[0][OFFSET] = 1;
	for (int i = 0; i <= n3; ++i) {
		for (int j = OFFSET - n3; j <= OFFSET + n3; ++j) {
			(f[i + 1][j + 1] += f[i][j]) %= mod;
			(f[i + 2][j - 1] += 1ll * f[i][j] * (i + 1) % mod) %= mod;
			(f[i + 3][j] += 1ll * f[i][j] * (i + 1) % mod * (i + 2) % mod) %= mod;
		}
	}

	int ans = 0;
	for (int i = OFFSET; i <= OFFSET + n3; ++i) {
		(ans += f[n3][i]) %= mod;
	}
	cout << ans << '\n';
}
