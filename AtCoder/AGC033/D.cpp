#include <stdio.h>
#include <math.h>
#include <algorithm>

#define N 186

inline int max(int a, int b) { return a > b ? a : b; }
inline int min(int a, int b) { return a < b ? a : b; }

int sum[N][N], tmp[2][N][N][N];
char s[N][N];

inline int getsum(int a, int b, int x, int y) {
	return sum[x][y] - sum[x][b - 1] - sum[a - 1][y] + sum[a - 1][b - 1];
}

inline int issame(int a, int b, int x, int y) {
	int siz = (x - a + 1) * (y - b + 1), cnt = getsum(a, b, x, y);
	return cnt == 0 || cnt == siz;
}

int main(void) {
	freopen("tmp.out", "w", stdout);

	int n, m, K; scanf("%d %d", &n, &m);
	K = ceil(log2(n)) + ceil(log2(m));

	for (int i = 1; i <= n; ++i)
		scanf("%s", s[i] + 1);

	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j)
			sum[i][j] = sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1] + (s[i][j] == '.');
	}

	auto f = tmp[0], g = tmp[1];
	for (int i = 1; i <= n; ++i) {
		for (int k = 1; k <= m; ++k) {
			int r = m;
			for (int j = i; j <= n; ++j) {
				while (!issame(i, k, j, r)) --r;
				f[i][j][k] = r;
				// printf("f[%d][%d][%d] = %d\n", i, j, k, r);
			}
		}
	}

	if (f[1][n][1] == m) {
		return puts("0"), 0;
	}

	for (int x = 1; ; ++x) {
		std::swap(f, g);
		for (int i = 1; i <= n; ++i) { // top
			for (int k = 1; k <= m; ++k) { // left
				int pos = i;
				for (int j = i; j <= n; ++j) { // down
					f[i][j][k] = max(g[i][j][k], g[i][j][g[i][j][k] + 1]);
					auto val = [&](int m) { return min(g[i][m][k], g[m + 1][j][k]); };
					while (pos < j && val(pos) <= val(pos + 1)) ++pos;
					f[i][j][k] = max(f[i][j][k], val(pos));
					// printf("f[%d][%d][%d] update to %d\n", i, j, k, f[i][j][k]);
				}
			}
		}
		// puts("");
		if (f[1][n][1] >= m) {
			return printf("%d\n", x) & 0;
		}
	}
}
