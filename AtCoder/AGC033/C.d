import std.stdio;
import std.algorithm;

int[] dep;
int[][] g;

int gdeep(int x, int f) {
	dep[x] = dep[f] + 1;
	int dp = x;
	foreach(y; g[x]) {
		if (y == f) continue;
		int z = gdeep(y, x);
		if (dep[z] > dep[dp]) dp = z;
	}
	return dp;
}

int gdis(int x, int f) {
	dep[x] = dep[f] + 1;
	int d = dep[x];
	foreach(y; g[x]) {
		if (y == f) continue;
		d = max(d, gdis(y, x));
	}
	return d;
}

void main() {
	int n;
	readf("%d", n);

	g.length = n + 1;
	dep.length = n + 1;

	for (int u, v, i = 1; i < n; i++) {
		readf(" %d %d", u, v);
		g[u] ~= [v]; g[v] ~= [u];
	}

	int deepest = gdeep(1, 0);
	int dis = gdis(deepest, 0);
	writeln(dis % 3 == 2 ? "Second" : "First");
}
