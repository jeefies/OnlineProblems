#include <iostream>
#include <algorithm>

using namespace std;
const int N = 2e5 + 3;

int h, w, n, x, y;
char s[N], t[N];

int main(void) {
	scanf("%d%d%d%d%d", &h, &w, &n, &x, &y);
	scanf("%s%s", s + 1, t + 1);

	int l = 1, u = 1, r = w, d = h;
	for (int i = n; i; --i) {
		switch (t[i]) {
			case 'U': 
				d = min(d + 1, h); break;
			case 'D':
				u = max(u - 1, 1); break;
			case 'R':
				l = max(l - 1, 1); break;
			case 'L':
				r = min(r + 1, w); break;
		}

		switch (s[i]) {
			case 'U': ++u; break;
			case 'D': --d; break;
			case 'L': ++l; break;
			case 'R': --r; break;
		}

		if (u > d || l > r) {
			return puts("NO"), 0;
		}
	}

	if (x < u || x > d || y < l || y > r)
		puts("NO");
	else
		puts("YES");
	return 0;
}

