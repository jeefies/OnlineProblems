#include <iostream>
#include <queue>
#include <iostream>
#include <string>

using namespace std;
const int N = 1002;

string map[N];
int vis[N][N];

struct Tup {
	int x, y, t;
}; queue<Tup> q;

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int h, w; cin >> h >> w;
	for (int i = 0; i < h; ++i)
		cin >> map[i];

	for (int i = 0; i < h; ++i) {
		for (int j = 0; j < w; ++j)
			if (map[i][j] == '#')
				vis[i][j] = 1, q.push({i, j, 0});
	}

	int dx[] = {0, 1, 0, -1}, dy[] = {1, 0, -1, 0};
	int ut = 0;
	while (q.size()) {
		Tup tp = q.front(); q.pop();
		ut = max(ut, tp.t);
		for (int i = 0; i < 4; ++i) {
			int x = tp.x + dx[i], y = tp.y + dy[i];
			if (x < 0 || y < 0 || x >= h || y >= w || vis[x][y]) continue;
			vis[x][y] = 1;
			q.push({x, y, tp.t + 1});
		}
	}

	cout << ut << '\n';
}
