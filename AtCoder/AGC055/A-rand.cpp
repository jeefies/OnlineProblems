#include <iostream>
#include <random>
#include <ctime>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 6e5 + 3;

char s[N];
int main(int argc, char* argv[]) {
	freopen("A.in", "w", stdout);

	int x = time(0);
	if (argc > 1) {
		char *v = argv[1];
		for (int i = 0, ie = strlen(v); i < ie; ++i)
			x = x * 10 + v[i] - '0';
	}

	std::mt19937 g(x);

	int n = 5;
	char c = 'A';
	for (int i = 1; i <= n * 3; ++i) {
		s[i] = c;
		if (i % n == 0) ++c;
	}

	std::shuffle(s + 1, s + 1 + n*3, g);


	clog << n << '\n' << (s + 1) << '\n';

	printf("%d\n", n);
	printf("%s\n", s + 1);
	return 0;
}
