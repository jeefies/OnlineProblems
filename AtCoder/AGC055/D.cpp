#include <iostream>
#include <algorithm>

using namespace std;
const int N = 16, mod = 998244353;

char s[N * 4];

// f[i][a][b][c];
int f[N][N][N][N][N][N];

inline void upd(int &x, int y) {
	x += y; if (x > mod) x -= mod;
}

int main() {
	int n = 0;
	cin >> n >> (s + 1);

	f[0][0][0][0][0][0] = 1;
	for (int i = 1; i <= n * 3; ++i) {
		for (int a = min(n, i - 1); ~a; --a) for (int b = min(n, i - 1); ~b; --b) for (int c = min(n, i - 1); ~c; --c)
			for (int x = 0; x <= a; ++x) for (int y = 0; y <= b; ++y) for (int z = 0; z <= c; ++z) {
				if (a < n && (s[i] == '?' || s[i] == 'A')) upd(f[a + 1][b][c][max(x, a + 1 - c)][y][z], f[a][b][c][x][y][z]);
				if (b < n && (s[i] == '?' || s[i] == 'B')) upd(f[a][b + 1][c][x][max(y, b + 1 - a)][z], f[a][b][c][x][y][z]);
				if (c < n && (s[i] == '?' || s[i] == 'C')) upd(f[a][b][c + 1][x][y][max(z, c + 1 - b)], f[a][b][c][x][y][z]);
			}
	}

	int ans = 0;
	for (int x = 0; x <= n; ++x) for (int y = 0; y <= n; ++y) for (int z = 0; z <= n; ++z)
		if (x + y + z <= n) upd(ans, f[n][n][n][x][y][z]);
	cout << ans << '\n';
	return 0;
}
