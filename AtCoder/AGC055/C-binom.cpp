#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e4 + 3;
int mod;

int qpow(int a, int x) {
	int r = 1;
	for (; x; x >>= 1, a = 1ll * a * a % mod) 
		if (x & 1) r = 1ll * a * r % mod;
	return r;
}

int fac[N] = {1}, ifac[N];
void prepare(int n) {
	for (int i = 1; i <= n; ++i) fac[i] = 1ll * fac[i - 1] * i % mod;
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i) ifac[i - 1] = 1ll * ifac[i] * i % mod;
}

int C(int n, int m) {
	if (n < m) return 0;
	return 1ll * fac[n] * ifac[m] % mod * ifac[n - m] % mod;
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n, m; cin >> n >> m >> mod;
	prepare(1e4);

	int ans = min(m, n / 2) - 1 + (m >= n - 1);
	for (int x = 1, xe = min(m, n - 1); x <= xe; ++x) {
		for (int y = 0, ye = (n - x) / 2; y <= ye; ++y)
			if (min(m, x + y) >= max(x, 3))
				(ans += 1ll * C(x + y, x) * C(x + 1, n - x - 2 * y) % mod * (min(m, x + y) - max(x, 3) + 1) % mod) %= mod;
	} cout << ans << '\n';
}
