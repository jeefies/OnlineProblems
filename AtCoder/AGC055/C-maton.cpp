#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <array>

using namespace std;
const int N = 5005;

#define MUST 0
#define MAY 1
#define CAN 2
#define LESS 3

int n, m, mod;

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> m >> mod;
	vector< array<int, 4> > f(m + 2), g = f;
	f[0][0] = 1;
	// f: current state, g: previous state.
	// f[k][state], trans n times
	for (int t = 1; t <= n; ++t) {
		swap(f, g);
		for (auto &a : f) fill(begin(a), end(a), 0);

		for (int i = 0; i <= m; ++i) {
#define flushto(k, T, S) ((f[k][T] += g[i][S]) %= mod)
			// state 0, all can put except CAN
			// state 1, only CAN
			// state 2, only MUST or MAY (last put USELESS)
			// state 3, only MUST or MAY (no CAN can put)
			flushto(i + 1, 0, 0), flushto(i + 1, 1, 0), flushto(i, 2, 0);
			flushto(i, 0, 1);
			flushto(i + 1, 0, 2), flushto(i, 3, 2);
			flushto(i + 1, 3, 3), flushto(i, 3, 3);
		}
	}

	int res = 1;
	for (int k = 3; k <= m; ++k) {
		(res += f[k][MUST]) %= mod;
		(res += f[k][LESS]) %= mod;
		(res += f[k][CAN]) %= mod;
	}

	if (n != 3 && m == n - 1)
		++res; // consider 1, 2, 3, ..., n
	cout << res << '\n';
	return 0;
}
