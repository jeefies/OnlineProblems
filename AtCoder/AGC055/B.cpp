#include <iostream>
#include <algorithm>
#include <string>
#include <stack>
#include <vector>

using namespace std;
const int N = 5e5 + 3;

int n;

bool needDel(char a, char b, char c) {
	if ((a == 'A' && b == 'B' && c == 'C') ||
			(a == 'B' && b == 'C' && c == 'A') ||
			(a == 'C' && b == 'A' && c == 'B')) return true;
	return false;
}

int clear(char *s) {
	vector<char> v;

#define siz (v.size())
	for (int i = 1; i <= n; ++i) {
		v.push_back(s[i]);
		while (siz >= 3 && needDel(v[siz - 3], v[siz - 2], v[siz - 1]))
			v.pop_back(), v.pop_back(), v.pop_back();
	}

	for (int i = 0; i < siz; ++i) {
		s[i] = v[i];
	}
	return siz;
}

char A[N], B[N];
int main(void) {
	scanf("%d", &n);
	scanf("%s%s", A + 1, B + 1);

	int lA = clear(A);
	int lB = clear(B);

	if (lA != lB) {
		cout << "NO" << '\n';
		return 0;
	}

	for (int i = 0; i < lA; ++i) {
		if (A[i] != B[i]) {
			cout << "NO" << '\n';
			return 0;
		}
	}
	cout << "YES" << '\n';
	return 0;
}
