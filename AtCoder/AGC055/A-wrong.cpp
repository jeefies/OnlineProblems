#include <iostream>
#include <algorithm>
#include <assert.h>

using namespace std;
const int N = 6e5 + 3;

int n, n3;
char s[N];
inline void read() {
	scanf("%d", &n); n3 = n * 3;
	scanf("%s", s + 1);
}

int mark[N];
char A, B, C;

bool check(int i, int len, char a, char b) {
	int ca = 0, cb = 0;
	for (; ca < len && i <= n3; ++i)
		ca += !mark[i] && s[i] == a;
	if (i > n3) return false;
	for (; cb < len && i <= n3; ++i)
		cb += !mark[i] && s[i] == b;
	if (i > n3 && cb < len) return false;
	B = a, C = b;
	return true;
}

bool check(int len) {
	int i = 1;
	while (i < n3 && mark[i]) 
		++i; // find first with out mark
	char f = s[i];
	int cnt = 0;
	for (; cnt < len && i <= n3; ++i)
		cnt += !mark[i] && s[i] == f;

	if (i > n3) return false;
	A = f;

	// now find other two.
	if (f == 'A') return check(i, cnt, 'B', 'C') || check(i, cnt, 'C', 'B');
	else if (f == 'B') return check(i, cnt, 'A', 'C') || check(i, cnt, 'C', 'A');
	return check(i, cnt, 'A', 'B') || check(i, cnt, 'B', 'A');
}

void wipe(int len, int t) {
	int i = 1, cnt = 0;
	for (cnt = 0; cnt < len && i <= n3; ++i)
		if (!mark[i] && s[i] == A)
			++cnt, mark[i] = t;
	for (cnt = 0; cnt < len && i <= n3; ++i)
		if (!mark[i] && s[i] == B)
			++cnt, mark[i] = t;
	for (cnt = 0; cnt < len && i <= n3; ++i)
		if (!mark[i] && s[i] == C)
			++cnt, mark[i] = t;
}

int longest(int t) {
	int len = 0;
	for (int w = 1 << 19; w; w >>= 1) {
		if (len + w <= n && check(len + w))
			len += w;
	}

	wipe(len, t);
	return len;
}

int main(void) {
	freopen("A.in", "r", stdin);

	cin.tie(0)->sync_with_stdio(false);
	read();
	int flag = 0;
	int make = 0;

RUN:
	for (int t = 1; t <= 6; ++t) {
		make += longest(t);
		if (make == n) break;
	}

	if (make != n) {
		flag = 1, make = 0;
		reverse(s + 1, s + 1 + n3);
		fill_n(mark + 1, n3, 0);
		goto RUN;
	}

	if (flag) reverse(mark + 1, mark + 1 + n3);

	for (int i = 1; i <= n3; ++i) {
		cout << mark[i];
	} cout << '\n';
}
