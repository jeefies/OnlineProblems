#include <iostream>
#include <algorithm>

using namespace std;
const int N = 6e5 + 3;

#define lowbit(i) ((i) & -(i))
int b[N];
inline void update(int i, int x) {
	for (; i < N; i += lowbit(i)) b[i] += x;
}

inline int query(int i) {
	int r = 0; for (; i; i -= lowbit(i)) r += b[i];
	return r;
}

int n;
int perm[N], exi[N];
int L[N], R[N];

inline void error(int cond) {
	if (!cond) {
		cout << "-1\n";
		exit(0);
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> L[i] >> R[i];
		if (i > 1 && (R[i - 1] > R[i] || L[i - 1] > L[i]))
			return puts("-1"), 0;
	}

	for (int i = n; i; --i) {
		if (L[i] != L[i + 1]) {
			perm[i] = L[i];
		}
	}

	for (int i = 1; i <= n; ++i) {
		if (R[i] != R[i - 1]) {
			if (perm[i] && perm[i] != R[i])
				return puts("-1"), 0;
			perm[i] = R[i];
		}
	}

	for (int i = 1; i <= n; ++i) {
		if (exi[perm[i]]) return puts("-1"), 0;
		++exi[perm[i]];
	}

	for (int cur = 1, i = 1; i <= n; ++i) {
		if (!perm[i]) {
			while (exi[cur])
				++cur;
			perm[i] = cur++;
		}
	}

	for (int mx = 0, i = 1; i <= n; ++i) {
		if (perm[i] > mx) mx = perm[i];
		if (mx != perm[i]) return puts("-1"), 0;
	}

	for (int mx = N, i = n; i; --i) {
		if (perm[i] < mx) mx = perm[i];
		if (mx != L[i]) return puts("-1"), 0;
	}

	for (int i = 1; i <= n; ++i) {
		clog << perm[i] << " \n"[i == n];
	}

	long long ans = 0;
	for (int i = n; i; --i) {
		ans += query(perm[i]);
		update(perm[i], 1);
	}

	cout << ans << '\n';
}
