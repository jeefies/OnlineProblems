#include <iostream>
#include <algorithm>
#include <cstring>
#include <string>
#include <vector>

using namespace std;

char s[10000000];
int cnt[3][3], n;
int main(void) {
	scanf("%d%s", &n, s);

	for (int i = 0; i < 3; ++i) {
		for (int j = i * n, je = i * n + n; j < je; ++j)
			++cnt[i][s[j] - 'A'];
	}

	int perm[6][3] = {
		{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 1, 0}, {2, 0, 1},
	};

	int id = 0;
	vector<int> mark[3][3];
	for (int *p : perm) {
#define cp(i) (cnt[i][p[i]])
		int x = min(cp(0), min(cp(1), cp(2)));
		if (x) ++id;

		cp(0) -= x, cp(1) -= x, cp(2) -= x;
		while (x--) {
#define mk(i) (mark[i][p[i]].push_back(id))
			mk(0), mk(1), mk(2);
		}
	}

	for (int i = 0; i < 3; ++i) {
		for (int j = i * n, je = j + n; j < je; ++j)
			cout << mark[i][s[j] - 'A'].back(), mark[i][s[j] - 'A'].pop_back();
	} cout << '\n';
}
