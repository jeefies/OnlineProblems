#include <iostream>
#include <algorithm>
#include <stack>
#include <cstring>
#include <queue>

using namespace std;
const int N = 6e5 + 3, INF = 1e9;

int n;
char s[N];

int id[3][3] = {
	{0, 1, 2},
	{3, 0, 4},
	{5, 6, 0},
};

class ISAP {
private:
	struct Edge {
		int to, rev;
		int flow;
		Edge(int to, int rev, int flow) : to(to), rev(rev), flow(flow) {}
	};
	vector<int> gap, dep;
	vector<Edge> G[50];
public:
	int n, m, s, t;
	inline void add(int u, int v, int w) {
		G[u].push_back(Edge(v, G[v].size(), w));
		G[v].push_back(Edge(u, G[u].size() - 1, 0));
	}

	inline void init() {
		vector<int> q(n + 2);
		gap.assign(n * 2, 0), dep.assign(n + 1, -1);
		dep[t] = 0, gap[0] = 1;
		
		int head(0), tail(1), x, y;
        q[head] = t;
		while (head ^ tail) {
			x = q[head++];
			for (auto &e : G[x]) {
				if (dep[(y = e.to)] == -1)
					++gap[(dep[y] = dep[x] + 1)], q[tail++] = y;
			}
		}
	}
	
	inline int sap(int x, int flow) {
		if (x == t) return flow;
		
		int rest = flow;
		for (auto &e : G[x]) {
			if (dep[e.to] + 1 == dep[x] && e.flow) {
				int f = sap(e.to, min(rest, e.flow));
				if (f) {
					e.flow -= f, G[e.to][e.rev].flow += f;
					rest -= f;
				}
				if (!rest) return flow;
			}
		}
		
		if (--gap[dep[x]] == 0) dep[s] = n + 1;
		return ++gap[++dep[x]], flow - rest;
	}
	
	inline int calc() {
		int maxflow = 0, INF = numeric_limits<int>::max();
		while (dep[s] <= n) {
			maxflow += sap(s, INF);
		}
		return maxflow;
	}

	typedef stack<int> stk;
	void build(stk st1[3], stk st2[3], stk st3[3]) {
		for (int x = 4; x < 7; ++x) {
			for (auto e : G[x]) {
				if (e.to == 7) continue;
				int cnt = e.flow, d = id[e.to - 1][x - 4];
				// cout << x << " to " << e.to << " flow " << cnt << '\n';
				// cout << "Push " << e.to - 1 << ' ' << x - 4 << ' ' << 3 - (e.to - 1) - (x - 4) << " in " << d << '\n';
				while (cnt--) {
					st1[e.to - 1].push(d);
					st2[x - 4].push(d);
					st3[3 - (e.to - 1) - (x - 4)].push(d);
				}
			}
		}
	}
} isap;

int main(void) {
	freopen("A.in", "r", stdin);

	scanf("%d%s", &n, (s + 1));

	int c1[3] = {0, 0, 0}, c2[3] = {0, 0, 0};
	for (int i = 1; i <= n; ++i) {
		++c1[s[i] - 'A'];
	}

	for (int i = n + 1; i <= n + n; ++i) {
		++c2[s[i] - 'A'];
	}

	stack<int> st1[3], st2[3], st3[3];
	{
		int s = 0, t = 7;
		isap.add(s, 1, c1[0]);
		isap.add(s, 2, c1[1]);
		isap.add(s, 3, c1[2]);

		isap.add(1, 2 + 3, INF);
		isap.add(1, 3 + 3, INF);
		isap.add(2, 1 + 3, INF);
		isap.add(2, 3 + 3, INF);
		isap.add(3, 1 + 3, INF);
		isap.add(3, 2 + 3, INF);

		isap.add(4, t, c2[0]);
		isap.add(5, t, c2[1]);
		isap.add(6, t, c2[2]);

		isap.s = 0, isap.t = 7, isap.n = 10;
		isap.init();
		isap.calc();

		isap.build(st1, st2, st3);
	}

	for (int i = 1; i <= n; ++i) {
		cout << st1[s[i] - 'A'].top();
		st1[s[i] - 'A'].pop();
	}
	for (int i = n + 1; i <= n * 2; ++i) {
		cout << st2[s[i] - 'A'].top();
		st2[s[i] - 'A'].pop();
	}
	for (int i = n * 2 + 1; i <= n * 3; ++i) {
		cout << st3[s[i] - 'A'].top();
		st3[s[i] - 'A'].pop();
	}
}
