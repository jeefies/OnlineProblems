import std.stdio, std.string, std.array;
import std.traits, std.conv, std.algorithm;
import std.math, std.regex;
import std.container;

immutable int N = cast(int)3e5 + 7;

int n, m, p, q;
int[][] flt;
string[N] map;

pragma(inline) bool ok(int i, int j) {
	return !(i < 0 || j < 0 || i >= n || j >= m || map[i][j] == '#');
}

struct Edge {
	int to, next, w;
}; Edge[N << 2] edge;
int tot = 0;
int[N] head, vis;
pragma(inline) void add(int u, int v, int w) {
	edge[++tot] = Edge(v, head[u], w);
	head[u] = tot;
}

long[N] dis;
struct Node {
	int x;
	long v;
};
BinaryHeap!(Node[], "a.v > b.v") heap;
void djk() {
	while (!heap.empty()) {
		auto node = heap.front; heap.removeFront();
		int x = node.x;
		if (vis[x]) continue;
		vis[x] = true;

		for (int i = head[x]; i; i = edge[i].next) {
			auto e = edge[i];
			if (!vis[e.to] && dis[e.to] > dis[x] + e.w) {
				dis[e.to] = dis[x] + e.w;
				heap.insert(Node(e.to, dis[e.to]));
			}
		}
	}
}

void main() {
	readf("%s %s\n%s %s\n", n, m, p, q);

	for (int i = 0; i < n; ++i) {
		map[i] = readln();
	}

	flt = new int[][](n, m);
	for (int cur = 0, i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			flt[i][j] = ++cur;
			dis[cur] = cast(long)1e18;
		}
	}

	heap = heapify!"a.v > b.v"(new Node[](0));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (map[i][j] == 'L') {
				if (ok(i - 1, j + 1)) add(flt[i - 1][j + 1], flt[i][j], p);
				if (ok(i + 1, j + 1)) add(flt[i + 1][j + 1], flt[i][j], p);
				if (ok(i, j + 2)) add(flt[i][j + 2], flt[i][j], q);
			} else if (map[i][j] == 'R') {
				if (ok(i - 1, j - 1)) add(flt[i - 1][j - 1], flt[i][j], p);
				if (ok(i + 1, j - 1)) add(flt[i + 1][j - 1], flt[i][j], p);
				if (ok(i, j - 2)) add(flt[i][j - 2], flt[i][j], q);
			} else if (map[i][j] == 'U') {
				if (ok(i + 1, j - 1)) add(flt[i + 1][j - 1], flt[i][j], p);
				if (ok(i + 1, j + 1)) add(flt[i + 1][j + 1], flt[i][j], p);
				if (ok(i + 2, j)) add(flt[i + 2][j], flt[i][j], q);
			} else if (map[i][j] == 'D') {
				if (ok(i - 1, j - 1)) add(flt[i - 1][j - 1], flt[i][j], p);
				if (ok(i - 1, j + 1)) add(flt[i - 1][j + 1], flt[i][j], p);
				if (ok(i - 2, j)) add(flt[i - 2][j], flt[i][j], q);
			} else if (map[i][j] == '.') {
				dis[flt[i][j]] = 0;
				heap.insert(Node(flt[i][j], 0));
			}
		}
	}

	djk();
	long ans = cast(long)1e18;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (!ok(i, j)) continue;
			if (ok(i + 1, j)) ans = min(ans, dis[flt[i][j]] + dis[flt[i + 1][j]]);
			if (ok(i, j + 1)) ans = min(ans, dis[flt[i][j]] + dis[flt[i][j + 1]]);
		}
	} writeln(ans == cast(long)1e18 ? -1 : ans);
}
