from random import randint

with open("1.in", "w") as f:
    T = 100
    print(T, file = f)
    for i in range(T):
        n = randint(5, 10)
        print(n, file = f)
        for j in range(n):
            print(randint(0, 1), file = f, end = ' ')
        print(file = f)
