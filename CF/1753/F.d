import std.stdio, std.string, std.array;
import std.traits, std.conv, std.algorithm;
import std.math, std.regex;

immutable int N = cast(int)(1e6 + 7), BLO = cast(int)(1e3 + 3);

struct MexBut {
	int[N] but, blk;

	pragma(inline) void add(int x) {
		but[(x - 1) / BLO] += !blk[x]++;
	}

	pragma(inline) void del(int x) {
		but[(x - 1) / BLO] -= !--blk[x];
	}

	int qmex() {
		int b = 0;
		while (but[b] == BLO) ++b;
		b = b * BLO + 1;
		while (blk[b]) ++b;
		return b;
	}
};
MexBut mexp, mexn;

int[][][] map;
pragma(inline) void update(int i, int j, int op) {
	// writefln("Update at {%d, %d} (op %d)", i, j, op);
	if (op == 1) {
		foreach (v; map[i][j]) {
			if (v < 0) mexn.add(-v);
			else mexp.add(v);
		}
	} else {
		foreach (v; map[i][j]) {
			if (v < 0) mexn.del(-v);
			else mexp.del(v);
		}
	}
}

int n, m, k, t;
long not = 0, all = 0;
void start(int x, int y) {
	// mexp.clear, mexn.clear;
	// if (!mexp.check || !mexn.check) writeln("ERROR");

	int s = 0;
	for (; x <= n && y <= m; ++x, ++y, --s) {
		while (x + s <= n && y + s <= m && (mexp.qmex() + mexn.qmex() - 1 < t || s < 0)) {
			++s;
			for (int i = x; i < x + s; ++i)
				update(i, y + s - 1, 1);
			for (int i = y; i + 1 < y + s; ++i)
				update(x + s - 1, i, 1);
		}

		if (mexp.qmex() + mexn.qmex() - 1 >= t) not += s ? s - 1 : 0;
		else not += s;

		// writefln("At %d %d", x, y);
		// writeln("Final s ", s, " not update to ", not);

		for (int i = x; i < x + s; ++i)
			update(i, y, -1);
		for (int i = y + 1; i < y + s; ++i)
			update(x, i, -1);
	}
}

void main() {
	// stdin.reopen("1.in", "r");

	readf(" %s %s %s %s", n, m, k, t);

	map = new int[][][](n + 1, m + 1, 0);
	for (int x, y, w, i = 0; i < k; ++i) {
		readf(" %s %s %s", x, y, w);
		if (abs(w) < N) map[x][y] ~= w;
	}

	for (int i = 1; i <= n; ++i) start(i, 1);
	for (int i = 2; i <= m; ++i) start(1, i);

	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j)
			all += i < j ? i : j;
	}
	// writeln(all);
	writeln(all - not);
}
