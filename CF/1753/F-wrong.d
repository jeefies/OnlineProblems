import std.stdio, std.string, std.array;
import std.traits, std.conv, std.algorithm;
import std.math, std.regex;

immutable int N = cast(int)(1e6 + 7), BLO = cast(int)(1e3 + 3);

// The Complexity of this program is not correct !!!!

struct MexBut {
	int[N] but, blk;

	pragma(inline) void add(int x) {
		but[(x - 1) / BLO] += !blk[x]++;
	}

	pragma(inline) void del(int x) {
		but[(x - 1) / BLO] -= !--blk[x];
	}

	int qmex() {
		int b = 0;
		while (but[b] == BLO) ++b;
		b = b * BLO + 1;
		while (blk[b]) ++b;
		return b;
	}
};
MexBut mexp, mexn;

int[][][] map;
pragma(inline) void update(int i, int j, int op) {
	// writefln("Update at {%d, %d} (op %d)", i, j, op);
	if (op == 1) {
		foreach (v; map[i][j]) {
			if (v < 0) mexn.add(-v);
			else mexp.add(v);
		}
	} else {
		foreach (v; map[i][j]) {
			if (v < 0) mexn.del(-v);
			else mexp.del(v);
		}
	}
}

void main() {
	int n, m, k, t;
	readf(" %s %s %s %s", n, m, k, t);

	map = new int[][][](n + 1, m + 1, 0);
	for (int x, y, w, i = 0; i < k; ++i) {
		readf(" %s %s %s", x, y, w);
		if (abs(w) < N) map[x][y] ~= w;
	}

	long not = 0, all;
	for (int line = 1; line <= n; ++line) {
		int l = 1, s = 0;
		for (; l <= m; ++l) {
			all += line < l ? line : l;
			while (l + s <= m && line + s <= n && (mexp.qmex() + mexn.qmex() - 1 < t || s < 0)) {
				++s;
				for (int i = l; i < l + s; ++i) update(line + s - 1, i, 1);
				for (int i = line; i + 1 < line + s; ++i) update(i, l + s - 1, 1);
			}

			if (mexp.qmex() + mexn.qmex() - 1 >= t) not += s ? s - 1 : 0;
			else not += s;
			// writeln("Final s ", s, " not update to ", not);

			// clear for next point.
			for (int i = l; i < l + s; ++i) update(line + s - 1, i, -1);
			for (int i = line; i + 1 < line + s; ++i) update(i, l, -1);
			--s;
		}
	}
	// writeln(all);
	writeln(all - not);
}
