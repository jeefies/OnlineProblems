import std.stdio;

immutable int mod = 998244353, N = 200000 + 7;

long qpow(long a, int x) {
	a %= mod;
	long r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

int n, invn, invn1, inv2;
int[N] a;
long[N] f;

long P(int k) {
	return 1L * k * k % mod * invn % mod * invn1 % mod * 2 % mod;
}

void work() {
	readf(" %s", n);
	invn = cast(int)qpow(cast(long)n, mod - 2);
	invn1 = cast(int)qpow(cast(long)n - 1, mod - 2);

	int co = 0, cz = 0;
	for (int i = 0; i < n; ++i) {
		readf(" %s", a[i]);
		cz += a[i] == 0;
	} co = n - cz;

	int k = 0;
	for (int i = 0; i < cz; ++i) {
		if (a[i] == 1) ++k;
	}

	f[k] = 0;
	for (; k; --k) {
		// f[k - 1] = qpow(1L + mod - P(k), mod - 2) + f[k];
		// writefln("P(%s) = %s", k, P(k));
		f[k - 1] = (qpow(P(k), mod - 2) + f[k]) % mod;
	} writeln(f[0]);
}

void main() {
	inv2 = cast(int)qpow(2, mod - 2);
	int T; readf(" %s", T);
	while (T--) work();
}
