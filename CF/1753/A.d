import std.stdio, std.string, std.array;
import std.traits, std.conv, std.algorithm;
import std.math, std.regex;

immutable int N = cast(int)2e5 + 7;

int[N] a, f;
int[N] l, r;

void work() {
	int n; readf(" %s", n);

	int cp = 0, cn = 0;
	for (int i = 0; i < n; ++i) {
		readf(" %s", a[i]);

		if (a[i] == 1) ++cp;
		else if (a[i] == -1) ++cn;
	}

	if ((cp + cn) & 1) {
		writeln("-1");
		return ;
	}

	int tg = cp > cn ? 1 : -1, tc = abs(cp - cn) / 2;

	for (int i = 1; i < n; ++i) {
		f[i] = f[i - 1];
		if (a[i] == tg)
			f[i] = max(i >= 2 ? f[i - 2] : 0, i >= 3 ? f[i - 3] : 0) + 1;
	}

	if (f[n - 1] < tc) {
		writeln("-1");
		return ;
	}

	int cnt = 0;
	for (int i = 0; i < n;) {
		if (i + 1 < n && f[i + 1] > f[i] && f[i + 1] <= tc) {
			l[cnt] = i, r[cnt] = i + 1;
			++cnt, i += 2;
		} else {
			l[cnt] = r[cnt] = i;
			++cnt, ++i;
		}
	}

	writeln(cnt);
	for (int i = 0; i < cnt; ++i) {
		writeln(l[i] + 1, ' ', r[i] + 1);
	}
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
