import std.stdio, std.string, std.array;
import std.traits, std.conv, std.algorithm;
import std.math, std.regex;

immutable int N = cast(int)1e6 + 7;

int n, b, p, m;
long ans = 1;

int idx = 0;
int[40] mul, pick;
int[][N] add, sum;
long[40] pre;

void check(long upb, out long ocnt, out long osum) {
	long suf = 1; // for calcing delta (donation if move to front)
	for (int i = 1; i <= idx; ++i) {
		if (pick[i]) suf *= mul[i];
	}
	for (int i = idx; i; --i) {
		int len = -1;
		long ofs = (pre[i] - 1) * suf;
		// got the 
		for (int w = 1 << cast(int)log2(add[i].length); w; w >>= 1) {
			if (len + w < add[i].length
					&& add[i][len + w] * ofs >= upb)
				len += w;
		} ocnt += len + 1, osum += ~len ? ofs * sum[i][len] : 0;

		if (!pick[i]) suf *= mul[i];
	}		
}

long work() {
	// process answer is no movement of +
	long rest = b, prod = 1;
	long ori = 1 + (sum[0].length ? sum[0][$ - 1] : 0);
	for (int i = 1; i <= idx; ++i) {
		if (pick[i]) {
			rest -= m, prod *= mul[i];
		} else
			ori *= mul[i];
		ori += sum[i].length ? sum[i][$ - 1] : 0;
	} ori *= prod;

	pre[0] = 1;
	for (int i = 1; i <= idx; ++i) {
		pre[i] = pick[i] ? pre[i - 1] : pre[i - 1] * mul[i];
	}

	if (rest < 0) return ans;
	rest /= p;

	long delta = 0, dcnt, dsum;
	// find last dcnt > rest, thus, dcnt of delta-1 can <= rest
	// and the extra part has the donation of (dcnt - rest) * delta.
	// so we can calc exact rest numbers to get the donation.
	// dsum can be the total donation (with the extra part included).
	for (long w = 1L << 60; w; w >>= 1) {
		check(delta + w, dcnt, dsum);
		if (dcnt > rest) delta += w;
	} check(delta, dcnt, dsum);
	return ans = max(ans, ori + dsum - (dcnt - rest) * delta);
}

long dfs(int id) {
	work();

	for (int i = id + 1; i <= idx; ++i) {
		int canpick = 1;
		for (int j = 1; j < i; ++j)
			if (mul[j] == mul[i])
				canpick &= pick[j];
		if (!canpick) continue;

		pick[i] = true, dfs(i), pick[i] = false;
	} return ans;
}

void main() {
	readf(" %s %s %s %s", n, b, p, m);

	for (int i = 0; i < n; ++i) {
		char op; int x;
		readf(" %s %s", op, x);
		if (op == '*') {
			if (x == 1) continue;
			mul[++idx] = x;
			ans *= x;
		} else {
			add[idx] ~= x;
			ans += x;
		}
	}

	for (int i = 0; i <= idx; ++i) {
		add[i].sort!"a > b";
		foreach (v; add[i])
			sum[i] ~= sum[i].length ? sum[i][$ - 1] + v : v;
	}

	writeln(dfs(0));
}
