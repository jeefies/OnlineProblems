#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
const int N = 1e6 + 7, BLO = 1e3;

int lab[N];

struct MexBut {
	int but[N], blk[N];
	inline void add(int x) {
		but[lab[x]] += blk[x]++ == 0;
	}

	inline void del(int x) {
		but[(x - 1) / BLO] -= --blk[x] == 0;
	}

	inline int qmex() {
		int b = 0;
		while (but[b] == BLO) ++b;
		b = b * BLO + 1;
		while (blk[b]) ++b;
		// printf("Mex got %d\n", b); 
		return b;
	}
} mexp, mexn;

std::vector< std::vector<int> > mp[40005];

inline void update(int i, int j, int op) {
	if (op == 1) {
		for (int v : mp[i][j])
			v < 0 ? mexn.add(-v) : mexp.add(v);
	} else {
		for (int v : mp[i][j])
			v < 0 ? mexn.del(-v) : mexp.del(v);
	}
}

int n, m, k, t;
long long nt = 0, all = 0, cnt = 0;
void start(int x, int y) {
	int nx = x, ny = y;
	update(x, y, 1);
	for (; x <= n && y <= m; ++x, ++y) {
		while (nx < n && ny < m && mexp.qmex() + mexn.qmex() - 1 < t) {
			++nx, ++ny;
			for (int i = y; i <= ny; ++i)
				update(nx, i, 1);
			for (int i = x; i <= nx - 1; ++i)
				update(i, ny, 1);
		}

		if (mexp.qmex() + mexn.qmex() - 1 >= t)
			cnt += min(n - nx + 1, m - ny + 1);
/*
		if (mexp.qmex() + mexn.qmex() - 1 >= t) nt += s ? s - 1 : 0;
		else nt += s;
*/
		for (int i = y; i <= ny; ++i)
			update(x, i, -1);
		for (int i = x + 1; i <= nx; ++i)
			update(i, y, -1);
	}
}

int main(void) {
	for (int i = 1; i < N; ++i) 
		lab[i] = (i - 1) / BLO;
		
	cin.tie(0)->sync_with_stdio(false);
	// freopen("1.in", "r", stdin);
	
	cin >> n >> m >> k >> t;
	// scanf("%d %d %d %d", &n, &m, &k, &t);
	
	// mp.resize(n + 1);
	for (int i = 1; i <= n; ++i)
		mp[i].resize(m + 1);

	for (int x, y, w, i = 0; i < k; ++i) {
		cin >> x >> y >> w;
		// scanf("%d %d %d", &x, &y, &w);
		if (w != 0 && abs(w) <= k) mp[x][y].emplace_back(w);
	}
	
	for (int i = 1; i <= n; ++i) start(i, 1);
	for (int i = 2; i <= m; ++i) start(1, i);
	
	cout << cnt << '\n';
}

