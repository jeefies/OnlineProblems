from random import randint as rd

with open("1.in", "w") as f:
    n, m, k, t = 200, 200, 1000000, 20
    print(n, m, k, t, file = f)
    for i in range(k):
        print(rd(1, n), rd(1, m), rd(-50, 50), file = f)
        # print(101, 101, rd(-100000, 100000), file = f)
