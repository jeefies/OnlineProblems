#include <iostream>
#include <algorithm>

using namespace std;
const int N = 5e5 + 7;

int a[N], but[N], f[N];

void work() {
	int n, k; cin >> n >> k;

	fill_n(but, n + 2, 0);
	for (int i = 0 ; i < n; ++i) {
		cin >> a[i];
		++but[a[i]];
	}

	f[0] = 0, f[1] = but[1];
	for (int i = 2; i <= k; ++i) {
		// type 1. just use last one
		int uc = f[i - 1] / i;
		if (uc * i == f[i - 1]) {
			f[i] = but[i] + uc;
			continue;
		}

		// type 2. use two last and use last.
		uc = min(f[i - 2] / (i - 1), but[i - 1] / (i - 1));
		int cp = (but[i - 1] - uc * (i - 1)) / i;
		if (uc * (i - 1) == f[i - 2] && uc * (i - 1) + cp * i == but[i - 1]) {
			f[i] = but[i] + cp + uc;
			continue;
		}

		cout << "No\n";
		return ;
	}

	cout << "Yes\n";
}

int main(void) {
	cin.tie(nullptr)->sync_with_stdio(false);
	int T = 1;
	while (T--) work();
}
