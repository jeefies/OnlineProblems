#include <iostream>
#include <algorithm>

using namespace std;
using lint = long long;

const int N = 2e5 + 7;

int n, x[N];
lint pre[N];

double calc(int i, int j) {
	return (pre[j] - pre[i]) / (double)(j - i + 1);
}

double f(int x, int l) {
	lint sum = pre[n] - pre[n - l] + pre[x] - pre[x - l - 1];
	// printf("f(%d, %d) is %lf - %d\n", x, l, (double)sum / (2 * l + 1), ::x[x]);
	return (double)sum / (2 * l + 1) - ::x[x];
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> x[i];
		
	sort(x + 1, x + 1 + n);
	for (int i = 1; i <= n; ++i)
		pre[i] = pre[i - 1] + x[i];
	
	// mid at something
	int ansi = 1, ansl = 0; double ansv = 0;
	for (int i = 1; i <= n; ++i) {
		int l = 0, r = min(i - 1, n - i);
		
		while (l + 3 < r) {
			int ml = (l + l + r) / 3, mr = (l + r + r) / 3;
			double vl = f(i, ml), vr = f(i, mr);
			if (vl < vr) l = ml;
			else r = mr;
		}
		
		for (int j = l; j <= r; ++j) {
			double fij = f(i, j);
			if (ansv < fij)
				ansv = fij, ansi = i, ansl = j;
		}
	}
	
	cout << (2 * ansl + 1) << '\n';
	for (int i = ansi - ansl; i <= ansi; ++i) {
		cout << x[i] << ' ';
	}
	
	for (int i = n - ansl + 1; i <= n; ++i) {
		cout << x[i] << ' ';
	}
	return 0;
}
