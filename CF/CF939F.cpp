#include <iostream>
#include <algorithm>
#include <deque>

using namespace std;
const int N = 30e4, K = 2e2, INF = 0x3F3F3F3F;

int tmp[2][N], *f = tmp[0], *g = tmp[1];
int L[K], R[K];

int main() {
	// freopen("test.in", "r", stdin);
	cin.tie(0)->sync_with_stdio(false);

	int n, k; cin >> n >> k;
	for (int i = 1; i <= k; ++i) cin >> L[i] >> R[i];
	// int n, k; scanf("%d%d", &n, &k);
	// for (int i = 1; i <= k; ++i) scanf("%d%d", L + i, R + i);

	fill_n(f + 1, n, INF);
	f[0] = g[0] = 0;
	for (int i = 1; i <= k; ++i) {
		swap(f, g); // prev is g
		deque<int> once, twice; // once in [L[i] - j, R[i] - j], twice in [j + L[i] - R[i], j]
		int l = L[i], r = R[i];
		copy_n(g, n + 1, f);

		// proc once
		for (int j = r; ~j; --j) {
			while (once.size() && once.front() < l - j) once.pop_front();
			while (once.size() && g[once.back()] > g[r - j]) once.pop_back();
			once.push_back(r - j);
			f[j] = min(f[j], g[once.front()] + 1);
		}

		// proc twice
		for (int j = 0; j <= r; ++j) {
			// pop j + l - r, push j
			while (twice.size() && twice.front() < j + l - r) twice.pop_front();
			while (twice.size() && g[twice.back()] > g[j]) twice.pop_back();
			twice.push_back(j);
			f[j] = min(f[j], g[twice.front()] + 2);
		}
	}

	if (f[n] == INF) return puts("Hungry"), 0;
	printf("Full\n%d\n", f[n]);
}
