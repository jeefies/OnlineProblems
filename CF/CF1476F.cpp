#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 4e5;

int f[N], p[N], col[N];
int lg2[N];
int st[N][21], fr[N];

#define lsh(k) (1<<(k))
void initST(int n) {
	for (int i = 1; i <= n; ++i)
		st[i][0] = i + p[i];

	int t = lg2[n] + 1;
	for (int k = 1; k <= t; ++k) {
		for (int i = 1; i + lsh(k) - 1 <= n; ++i)
			st[i][k] = max(st[i][k - 1], st[i + lsh(k-1)][k - 1]);
	}
}

int queryST(int l, int r) {
	if (l > r) return 0;
	int t = lg2[r - l + 1];
	int mx = max(st[l][t], st[r - lsh(t) + 1][t]);
	// printf("Query [%d, %d] got %d\n", l, r, mx);
	return mx;
}
void work() {
	int n; cin >> n;

	for (int i = 1; i <= n; ++i) {
		cin >> p[i];
	} initST(n);

	fr[1] = 1, fill_n(f + 1, n, 0);
	for (int i = 2; i <= n; ++i) {
		if (!p[i]) {
			f[i] = f[i - 1], fr[i] = i;
			continue;
		}
		// printf("proc at index %d\n", i);
		// try direct left
		int t = lower_bound(f, f + i, i - p[i] - 1) - f;
		// printf("\tgot t %d\n", t);
		fr[i] = t;
		if (t == i) {
			f[i] = f[i - 1]; continue;
		} 
		f[i] = max(i - 1, queryST(t + 1, i - 1));
		// try direct right
		if (f[i - 1] > f[i]) f[i] = f[i - 1], fr[i] = i;
		if (f[i - 1] >= i && i + p[i] > f[i])
			f[i] = i + p[i], fr[i] = i;
	}

	/*
	for (int i = 1; i <= n; ++i) {
		printf("%d(%d) ", f[i], fr[i]);
	} puts("");
	*/

	if (f[n] < n) return (void)puts("NO");
	puts("YES");

	static char direct[N]; fill_n(direct + 1, n, 'R');
	for (int cur = n; cur > 0; ) {
		// if (fr[cur] == cur) direct[cur] = 'R', --cur;
		// else direct[cur] = 'L', fill(direct + fr[cur] + 1, direct + cur, 'R'), cur = fr[cur];
		if (fr[cur] == cur) --cur;
		else direct[cur] = 'L', cur = fr[cur];
	} direct[n + 1] = '\0';
	puts(direct + 1);
}

int main() {
	// cin.tie(0)->sync_with_stdio(false);
	for (int i = 2; i <= (int)3e5; ++i) lg2[i] = lg2[i >> 1] + 1;
	
	int T; cin >> T;
	while (T--) work();
}
