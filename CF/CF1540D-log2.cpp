#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e5 + 3, BLO = 512, BC = N / BLO + 1;
// const int N = 100, BLO = 20, BC = N / BLO + 1;

int n, m;
#define lowbit(i) ((i) & -(i))
class BIT {
private:
	int b[N];
public:
	inline void update(int i, int x) {
		for (++i; i <= n + 2; i += lowbit(i)) b[i] += x;
	}
	inline void clear(int i) {
		for (++i; i <= n + 2; i += lowbit(i)) b[i] = 0;
	}
	inline int query(int i) {
		int r = 0; ++i;
		for (; i; i -= lowbit(i)) r += b[i];
		return r;
	}
} bit[BC];
vector<int> modi[BC];

int b[N];
bool changed[BC];
inline void buildBlock(int i, int l, int r) {
	changed[i] = false;
	fprintf(stderr, "Build at %d [%d, %d]\n", i, l, r);
	// clear first
	BIT &bt = bit[i];
	for (int j : modi[i]) {
		bt.clear(j);
	} modi[i].clear();
	// fprintf(stderr, "clear done\n");
	
	for (int k = l; k <= r; ++k) {
		// fprintf(stderr, "\tproc at b[%d] = %d\n", k, b[k]);
		// find bt[whr] < b[k]
		int whr = -1;
		// maybe 16 is ok??
		for (int w = 1 << 17; w; w >>= 1) {
			// if (whr + w <= n) fprintf(stderr, "\tQue at %d got: %d\n", whr + w, bt.query(whr + w)); 
			if (whr + w <= n && bt.query(whr + w) + whr + w < b[k]) 
				whr += w;
		} bt.update(whr + 1, 1), modi[i].push_back(whr + 1);
		fprintf(stderr, "\t\tUpdate from %d\n", whr + 1);
	} // fprintf(stderr, "done\n");
}

void preBuild() {
	for (int i = 0, bl = 1, br = BLO; bl <= n; ++i, bl += BLO, br += BLO) {
		buildBlock(i, bl, min(br, n));
	} // fprintf(stderr, "pre build done\n");
}

#define AT(x) ((x-1)/BLO)
#define BL(i) ((i)*BLO+1)
#define BR(i) (((i)+1)*BLO)
inline int Q(int x) {
	// fprintf(stderr, "Q at %d\n", x);
	int r = b[x], bend = BR(AT(x));
	if (bend >= n) {
		for (int i = x + 1; i <= n; ++i)
			r += (r >= b[i]);
		// fprintf(stderr, "\tFinal to %d\n", n - r);
		return n - r;
	}
	
	for (int i = x + 1; i <= bend; ++i)
		r += (r >= b[i]);
	// fprintf(stderr, "r: %d. block jump start at [%d (bidx %d)\n", r, BL(AT(x) + 1), AT(x));
	for (int b = AT(x) + 1; BL(b) <= n; ++b) {
		// fprintf(stderr, "\tQuery got: %d\n", bit[b].query(r));
		if (changed[b]) buildBlock(b, BL(b), min(BR(b), n));
		r = r + bit[b].query(r);
		// fprintf(stderr, "\tJump TO %d r to %d (bidx %d)\n", BR(b), r, b);
	} // fprintf(stderr, "\tFinal to %d\n", n - r);
	return n - r;
}

int main() {
	freopen("permutation.in", "r", stdin);
	freopen("permutation.ans", "w", stdout);
	freopen("permutation.log2", "w", stderr);
	cin.tie(0)->sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> b[i];
	preBuild();
	
	cin >> m;
	int op, x, y;
	for (int i = 1; i <= m; ++i) {
		cin >> op >> x;
		if (op == 2) cout << Q(x) << '\n';
		else {
			cin >> y, b[x] = y, changed[AT(x)] = true;
			// fprintf(stderr, "%d changed to %d\n", x, y);
			// buildBlock(AT(x), BL(AT(x)), min(BR(AT(x)), n));
		}
	}
}
