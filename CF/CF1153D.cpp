#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int N = 4e5;

int f[N], tp[N], k = 0;
vector<int> G[N];

void dfs(int x) {
	if (G[x].size() == 0) {
		f[x] = 1, ++k; return;
	} else f[x] = (tp[x] == 1) ? 0x7FFFFFF : 0;

	for (int y : G[x]) {
		dfs(y);
		if (tp[x] == 1) f[x] = min(f[x], f[y]);
		else f[x] += f[y];
	}
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	int n; cin >> n;
	for (int i = 1; i <= n; ++i) cin >> tp[i];
	for (int x, i = 2; i <= n; ++i) {
		cin >> x; G[x].push_back(i);
	} dfs(1);
	cout << k + 1 - f[1] << '\n';
}
