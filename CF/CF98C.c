#include <stdio.h>
#include <math.h>
#define ldb double

int a, b, l;
ldb calc(ldb theta) {
    ldb c = cos(theta), s = sin(theta);
    return a * s - l * c * s + b * c;
}

int main() {
    scanf("%d %d %d", &a, &b, &l);

    if (l <= b) {
        printf("%.10lf\n", (ldb)(a < l ? a : l));
        return 0;
    }

    if (l <= a) {
        printf("%.10lf\n", (ldb)(b < l ? b : l));
        return 0;
    }

    ldb L = 0, R = acos(-1) / 2;
    const ldb eps = 1e-10;
    while (L + eps < R) {
        ldb ml = (L + L + R) / 3, mr = (L + R + R) / 3;
        ldb vl = calc(ml), vr = calc(mr);
        
        if (vl < vr) R = mr;
        else L = ml;
    }

    ldb ans = calc(L);

    if (l < ans) ans = l;

    if (ans < eps) puts("My poor head =(");
    else printf("%.10lf\n", ans);

    return 0;
}