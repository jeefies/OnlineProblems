#include <iostream>
#include <algorithm>

using namespace std;
const int N = 2e5;
typedef long long lint;

inline lint mul(lint x, lint y, lint mod) {
	lint r = 0;
	for (; y; y >>= 1, x = (x + x) % mod) {
		if (y & 1) r = (r + x) % mod;
	}
	return r;
}

inline lint gcd(lint x, lint y) {
	return y ? gcd(y, x % y) : x;
}

inline lint lcm(lint x, lint y) {
	return x / gcd(x, y) * y;
}

inline lint exgcd(lint x, lint y, lint &s, lint &t) {
	if (!y) {
		s = 1, t = 0;
		return x;
	}

	lint r = exgcd(y, x % y, t, s);
	t -= (x / y) * s;
	return r;
}

struct Equation {
	lint a, p;
} equals[N];

bool merge(const Equation &e1, const Equation &e2, Equation &res) {
	lint s, t;
	lint d = exgcd(e1.p, e2.p, s, t);
	lint lcm = e1.p / d * e2.p;

	lint c = (e2.a - e1.a) % lcm;
	if (c < 0) c += lcm;
	if (c % d) return false;

	s = mul(s, c / d, lcm);
	if (s < 0) s += lcm;

	lint x = (e1.a + mul(s, e1.p, lcm)) % lcm;
	if (x <= 0) x += lcm;
	res = {x, lcm};
	return true;
}

int main() {
	cin.tie(0)->sync_with_stdio(false);

	lint n, m, k;
	cin >> n >> m >> k;

	lint x = 1;
	for (lint p, i = 1; i <= k; ++i) {
		cin >> p; equals[i].p = p;
		x = lcm(x, p);
		if (x > n) return puts("NO"), 0;
		equals[i].a = ((p - i + 1) % p + p) % p;
	}

	Equation e = equals[1];
	for (lint i = 2; i <= k; ++i) {
		if (!merge(e, equals[i], e)) return puts("NO"), 0;
	}

	lint i = e.p, j = e.a;
	j = (j + i - 1) % i + 1;
	fprintf(stderr, "Final i, j to {%lld, %lld}\n", i, j);
	
	if (j + k - 1 > m) return puts("NO"), 0;

	// i is e.p, j is e.a
	for (lint l = 1; l <= k; ++l) {
		if (gcd(i, j + l - 1) != equals[l].p)
			return puts("NO"), 0;
	}

	return puts("YES"), 0;
}
