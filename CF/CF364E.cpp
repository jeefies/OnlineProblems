#include <iostream>
#include <algorithm>

using namespace std;
const int N = 2500 + 3;

string mp[N];
int sum[N][N];
int fl[8], fr[8];
long long ans = 0;

int n, m, K;

int area(int i, int j, int x, int y) {
    return sum[x][y] - sum[x][j - 1] - sum[i - 1][y] + sum[i - 1][j - 1];
}

void solve(int nl, int nr, int ml, int mr) {
    if (nl > nr || ml > mr) return;
    if (nl == nr && ml == mr) return (void)(ans += area(nl, ml, nr, mr) == K);

    if (nr - nl < mr - ml) {
        // 分治 m 这一维
        int mid = (mr + ml) >> 1;
        solve(nl, nr, ml, mid), solve(nl, nr, mid + 1, mr);

        for (int l = nl; l <= nr; ++l) {
            for (int k = 0; k <= K; ++k)
                fl[k] = ml, fr[k] = mr;

            for (int r = l; r <= nr; ++r) {
                for (int k = 0; k <= K; ++k) {
                    while (fl[k] <= mid && area(l, fl[k], r, mid) > k) ++fl[k];
                    while (fr[k] >= mid + 1 && area(l, mid + 1, r, fr[k]) > k) --fr[k];
                }

                for (int k = 0; k <= K; ++k) {
                    int cntl = (k == 0) ? mid + 1 - fl[k] : fl[k - 1] - fl[k];
                    int cntr = (k == K) ? fr[K - k] - mid : fr[K - k] - fr[K - k - 1];
                    ans += 1ll * cntl * cntr;
                }
            }
        }
    } else {
        int mid = (nl + nr) >> 1;
        solve(nl, mid, ml, mr), solve(mid + 1, nr, ml, mr);

        for (int l = ml; l <= mr; ++l) {
            for (int k = 0; k <= K; ++k)
                fl[k] = nl, fr[k] = nr;
            
            for (int r = l; r <= mr; ++r) {
                for (int k = 0; k <= K; ++k) {
                    while (fl[k] <= mid && area(fl[k], l, mid, r) > k) ++fl[k];
                    while (fr[k] >= mid + 1 && area(mid + 1, l, fr[k], r) > k) --fr[k];
                }

                for (int k = 0; k <= K; ++k) {
                    int cntl = (k == 0) ? mid + 1 - fl[0] : fl[k - 1] - fl[k];
                    int cntr = (k == K) ? fr[0] - mid : fr[K - k] - fr[K - k - 1];
                    ans += 1ll * cntl * cntr;
                }
            }
        }
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(false);

    cin >> n >> m >> K;
    for (int i = 1; i <= n; ++i)
        cin >> mp[i];

    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j)
            sum[i][j] = sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1] + (mp[i][j - 1] == '1');
    }

    solve(1, n, 1, m);
    cout << ans << '\n';
}