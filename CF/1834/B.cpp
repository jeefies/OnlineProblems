#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>

using namespace std;
const int N = 1000;

char tmp[2][N], *a = tmp[0], *b = tmp[1];

void work() {
	memset(a, 0, N); memset(b, 0, N);

	scanf("%s %s", a, b);
	int lena = strlen(a), lenb = strlen(b);

	if (lena != lenb) {
		if (lena > lenb) {
			cout << a[0] - '0' + 9 * (lena - 1) << '\n';
		} else {
			cout << b[0] - '0' + 9 * (lenb - 1) << '\n';
		}
		return ;
	}

	for (int i = 0; i < lena; ++i) {
		if (a[i] != b[i]) {
			cout << abs(a[i] - b[i]) + 9 * (lena - i - 1) << '\n';
			return ;
		}
	}


	cout << "0" << '\n';
}

int main(void) {
	int T; cin >> T;
	while (T--) work();
}
