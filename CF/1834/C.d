import std.stdio, std.string, std.array;
import std.traits, std.conv, std.algorithm;
import std.math, std.regex;

void work() {
	int n;
	readf(" %s\n", n);
	string a = strip(readln()), b = strip(readln());

	int insame = 0;
	for (int i = 0; i < n; ++i) {
		if (a[i] ^ b[i]) ++insame;
	}

	int incnt = insame / 2 * 4 + insame % 2;

	int resame = 0;
	for (int i = 0; i < n; ++i) {
		if (a[i] ^ b[n - i - 1]) ++resame;
	}

	int recnt = resame == 0 ? 2 : 2 * resame - (1 - resame % 2);
	writeln(min(incnt, recnt));
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
