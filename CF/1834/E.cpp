#include <iostream>	
#include <algorithm>
#include <set>

using namespace std;
const int N = 3e5 + 3;
typedef long long lint;

inline lint gcd(lint x, lint y) {
	return y ? gcd(y, x % y) : x;
}

inline lint lcm(lint x, lint y) {
	return x / gcd(x, y) * y;
}

lint n;
lint a[N];

void work() {
	set<lint> all, cur, prv;
	all.clear(); cur.clear(), prv.clear();

	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	}

	for (int i = 1; i <= n; ++i) {
		swap(cur, prv); cur.clear();
		for (auto v : prv) {
			lint add = lcm(v, a[i]);
			if (add < n * n) cur.insert(add);
		} cur.insert(a[i]);

		for (auto v : cur) all.insert(v);
	}

	lint mex = 1;
	for (auto v : all) {
		if (mex != v) break;
		if (mex == v) ++mex;
	}
	 cout << mex << '\n';
}

int main(void) {
	cin.tie(nullptr)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
