import std.stdio;

void main() {
	int[2] but;

	int T; readf(" %s", T);
	while (T--) {
		int n; readf(" %s", n);
		but[] = 0;
		for (int x, i = 1; i <= n; ++i) {
			readf(" %s", x);
			but[x == 1 ? 1 : 0] += 1;
		}

		int cnt = 0;
		while (but[1] < but[0]) --but[0], ++but[1], ++cnt;
		if (but[0] & 1) ++cnt;
		writeln(cnt);
	}
}
