#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e5 + 3;
char a[N], b[N];

void work() {
	int n;
	scanf("%d%s%s", &n, a, b);

	int isame = 0, resame = 0;
	for (int i = 0; i < n; ++i) {
		if (a[i] ^ b[i]) ++isame;
		if (a[i] ^ b[n - i - 1]) ++resame;
	}

	int incnt = isame == 0 ? 0 : (isame * 2 - isame % 2);
	int recnt = resame == 0 ? 2 : (resame * 2 - (resame - 1) % 2);

	cout << min(incnt, recnt) << '\n';
}

int main(void) {
	int T; cin >> T;
	while (T--) work();
}
