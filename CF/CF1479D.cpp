#include <iostream>
#include <algorithm>
#include <vector>
#include <random>
#include <ctime>

using namespace std;
const int N = 3e5 + 3;
typedef long long lint;

std::mt19937 gen(time(NULL));
std::uniform_int_distribution<lint> rd(0, 0x7FFFFFFFFFFFFFFFll);

static int n, m;
std::vector<int> G[N];
int rts[N];

int lc[N << 4], rc[N << 4], usage = 0;
lint clab[N], val[N << 4];
inline int clone(int p) {
	++usage;
	lc[usage] = lc[p], rc[usage] = rc[p], val[usage] = val[p];
	return usage;
}

#define update(p) ((val[p] = val[lc[p]] ^ val[rc[p]]))

lint insert(int c, int &p, int L = 1, int R = n) {
	p = clone(p);
	if (L == R)
		return val[p] ^= clab[L];
	int mid = (L + R) >> 1;
	return (c <= mid) ? insert(c, lc[p], L, mid) : insert(c, rc[p], mid + 1, R), update(p);
}

int query(int x, int y, int c, int l, int r, int L = 1, int R = n) {
	if (l > R || r < L) return -1;

	int mid = (L + R) >> 1;
	lint v = val[x] ^ val[y] ^ ((L <= c && c <= R) ? clab[c] : 0);
	if (L == R) return v != 0 ? L : -1;
	else if (!v) return -1;

	if ((v = query(lc[x], lc[y], c, l, r, L, mid)) != -1) return v;
	if ((v = query(rc[x], rc[y], c, l, r, mid + 1, R)) != -1) return v;
	return -1;
}

int col[N], fa[N][20], dep[N];
void dfs(int x, int p) {
	insert(col[x], rts[x] = rts[p]);
	dep[x] = dep[p] + 1;

	fa[x][0] = p;
	for (int i = 1; i < 20; ++i)
		fa[x][i] = fa[fa[x][i - 1]][i - 1];

	for (int y : G[x]) {
		if (y == p) continue;
		dfs(y, x);
	}
}

int getLCA(int x, int y) {
	if (dep[x] < dep[y]) swap(x, y);
	if (dep[x] != dep[y]) {
		for (int i = 19; ~i; --i) {
			if (dep[fa[x][i]] > dep[y]) x = fa[x][i];
		} x = fa[x][0];
	}

	if (x == y) return x;

	for (int i = 19; ~i; --i) {
		if (fa[x][i] ^ fa[y][i]) x = fa[x][i], y = fa[y][i];
	}

	return fa[x][0];
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> m;
	for (int i = 1; i <= n; ++i) {
		cin >> col[i];
		clab[i] = rd(gen);
	}

	for (int u, v, i = 1; i < n; ++i) {
		cin >> u >> v;
		G[u].push_back(v), G[v].push_back(u);
	} dfs(1, 0);

	for (int x, y, l, r, i = 1; i <= m; ++i) {
		cin >> x >> y >> l >> r;
		cout << query(rts[x], rts[y], col[getLCA(x, y)], l, r) << '\n';
	}
}
