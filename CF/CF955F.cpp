#include <iostream>
#include <assert.h>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 3e5 + 7;

#define ALL(x) begin(x), end(x)

int n;
std::vector<int> G[N];
int f[N][20], hei[N];

long long ans = 0;

void workf(int x, int p) {
	hei[x] = 1;
	for (int y : G[x]) {
		if (y ^ p) 
			workf(y, x), hei[x] = max(hei[x], hei[y] + 1);
	} ans += hei[x];

	f[x][1] = n;
	std::vector<int> vec;
	for (int d = 2; d < 20; ++d) {
		vec.clear();
		for (int y : G[x]) {
			if (y ^ p) vec.push_back(f[y][d - 1]);
		}
		sort(ALL(vec), [](int a, int b){ return a > b; });
		int cur = 0, ce = vec.size();
		while (cur < ce && vec[cur] /* kth >= k (vec[cur] is (cur+1)th) */ >= cur + 1)
			++cur;
		f[x][d] = cur;
	}
}

void workm(int x, int p) {
	for (int y : G[x]) {
		if (y == p) continue;
		workm(y, x);
		for (int d = 1; d < 20; ++d)
			f[x][d] = max(f[x][d], f[y][d]);
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n;
	for (int u, v, i = 1; i < n; ++i) {
		cin >> u >> v;
		G[u].push_back(v), G[v].push_back(u);
	}

	workf(1, 0), workm(1, 0);
	for (int i = 1; i <= n; ++i) {
		for (int d = 1; d < 20; ++d)
			ans += (f[i][d] > 0 ? f[i][d] - 1 : 0);
	}
	cout << ans << '\n';
}
