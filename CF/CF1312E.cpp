#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 600;

int a[N];
int f[N][N], w[N][N];

int main() {
	// cin.tie(0)->sync_with_stdio(false);
	int n; cin >> n;
	for (int i = 1; i <= n; ++i) cin >> a[i];

	memset(w, 0xFF, sizeof(w));
	memset(f, 0x7F, sizeof(f));
	for (int l = 0; l < n; ++l) {
		for (int i = 1; i + l <= n; ++i) {
			int j = i + l;
			if (i == j) f[i][j] = 1, w[i][j] = a[i];
			else for (int k = i; k < j; ++k) {
				if (~w[i][k] && w[i][k] == w[k + 1][j]) f[i][j] = 1, w[i][j] = w[i][k] + 1;
				else f[i][j] = min(f[i][j], f[i][k] + f[k + 1][j]);
			}
		}
	} cout << f[1][n] << '\n';
}
