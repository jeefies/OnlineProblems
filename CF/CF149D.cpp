#include <iostream>
#include <algorithm>
#include <stack>
#include <cstring>

using namespace std;
const int N = 1000, mod = 1000000007;

struct mint {
	int v;
	mint operator + (const mint o) {
		return (mint){(v + o.v) % mod};
	}
	mint operator * (const mint o) {
		return (mint){(int)((1ll * v * o.v) % mod)};
	}
};

mint f[N][N][3][3];
int match[N];
char brack[N];

void prepare(int n) {
	memset(f, 0, sizeof(f));
	stack<int> stk;
	for (int i = 1; i <= n; ++i) {
		if (brack[i] == '(') stk.push(i);
		else match[i] = stk.top(), match[stk.top()] = i, stk.pop();
	}
}

#define one(a, b) (!a ^ !b)
void dfs(int l, int r) {
	if (l + 1 == r) {
		f[l][r][0][1] = f[l][r][1][0] = f[l][r][2][0] = f[l][r][0][2] = {1};
		return;
	}
	
	if (match[l] == r) {
		dfs(l + 1, r - 1);
		auto lr = f[l][r], ilr = f[l + 1][r - 1];
		for (int lc = 0; lc < 3; ++lc) for (int rc = 0; rc < 3; ++rc) {
			if (lc != 1) lr[1][0] = lr[1][0] + ilr[lc][rc];
			if (lc != 2) lr[2][0] = lr[2][0] + ilr[lc][rc];
			if (rc != 1) lr[0][1] = lr[0][1] + ilr[lc][rc];
			if (rc != 2) lr[0][2] = lr[0][2] + ilr[lc][rc];
		}
	} else {
		int m = match[l];
		dfs(l, match[l]), dfs(match[l] + 1, r);
		auto lr = f[l][r], lm = f[l][m], rm = f[m + 1][r];
		for (int lc = 0; lc < 3; ++lc) for (int rc = 0; rc < 3; ++rc) {
			for (int ml = 0; ml < 3; ++ml) for (int mr = 0; mr < 3; ++mr) {
				if (!(ml == 1 && mr == 1) && !(ml == 2 && mr == 2)) lr[lc][rc] = lr[lc][rc] + lm[lc][ml] * rm[mr][rc];
			}
		}
	}
}

int main() {
	scanf("%s", brack + 1);
	int n = strlen(brack + 1);
	prepare(n);
	mint ans = {0};

	dfs(1, n);
	for (int lc = 0; lc < 3; ++lc) {
		for (int rc = 0; rc < 3; ++rc)
			ans = ans + f[1][n][lc][rc];
	} cout << ans.v << '\n';
}
