#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>

using namespace std;
const int N = 2e5 + 7;

int grp[N];
inline int find(int x) {
	// return x == grp[x] ? x : grp[x] = find(grp[x]);
	return grp[x] ? grp[x] = find(grp[x]) : x;
}

#define LG 30
#define bit(x, i) (((x) >> (i)) & 1)
int ch[N * LG][2], siz[N * LG], idx[N * LG], use = 0;

inline void insert(int &_p, int v, int i) {
	if (!_p) _p = ++use;
	int p = _p;
	++siz[p];
	for (int i = LG; ~i; --i) {
		int c = bit(v, i);
		if (!ch[p][c]) ch[p][c] = ++use;
		p = ch[p][c], ++siz[p];
	} idx[p] = i;
}

inline int merge(int x, int y) {
	if (!x || !y) return x | y;
	ch[x][0] = merge(ch[x][0], ch[y][0]), ch[x][1] = merge(ch[x][1], ch[y][1]);
	siz[x] = siz[ch[x][0]] + siz[ch[x][1]], idx[x] = idx[y];
	return x;
}

struct Q { int x, v; };
inline Q query(int x, int y, int v) {
	// x is all Trie
	int r = 0;
	for (int i = LG; ~i; --i) {
		int c = bit(v, i);
		if (ch[x][c] && siz[ch[x][c]] - siz[ch[y][c]] > 0)
			x = ch[x][c], y = ch[y][c];
		else
			r |= 1 << i, x = ch[x][c ^ 1], y = ch[y][c ^ 1];
	}

	return (Q){idx[x], r};
}

int a[N];
int rt[N], all = 0;
int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	for (int i = 1; i <= n; ++i) {
		grp[i] = i;
		cin >> a[i];
	}

	sort(a + 1, a + 1 + n);
	n = unique(a + 1, a + 1 + n) - a - 1;

	for (int i = 1; i <= n; ++i) {
		insert(all, a[i], i);
		insert(rt[i] = 0, a[i], i);
	}

	long long ans = 0;
	vector<int> minv, link;
	while (true) {
		bool flag = false;

		minv.assign(n + 1, 0x3FFFFFFF);
		link.assign(n + 1, 0);

		for (int i = 1; i <= n; ++i) {
			int x = find(i);
			Q q = query(all, rt[x], a[i]);

			int y = q.x, v = q.v;
			if (find(x) != find(y)) {
				if (v < minv[x]) minv[x] = v, link[x] = y;
				if (v < minv[y]) minv[y] = v, link[y] = x;
			}
		}

		for (int i = 1; i <= n; ++i) {
			if (link[i] && find(i) != find(link[i])) {
				ans += minv[i], flag = true;
				rt[find(i)] = merge(rt[find(i)], rt[find(link[i])]);
				grp[find(link[i])] = find(i);
			}
		}

		if (!flag) break;
	}

	cout << ans << '\n';
}
