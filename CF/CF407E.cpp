#include <iostream>
#include <stack>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;
const int N = 2e5 + 3;

typedef long long lint;

static int n;
class Seg {
private:
	lint minv[N << 2], lazy[N << 2];
public:
#define lp(p) (p>>1)
#define rp(p) (lp(p)|1)
	inline void sync(int p) {
		if (lazy[p]) {
			minv[lp(p)] += lazy[p], minv[rp(p)] += lazy[p];
			lazy[lp(p)] = lazy[rp(p)] = lazy[p];
			lazy[p] = 0;
		}
	}
	inline void update(int p) {
		minv[p] = min(minv[lp(p)], minv[rp(p)]);
	}
	void add(int l, int r, int v, int L = 1, int R = n, int p = 1) {
		if (l <= L && R <= r) {
			minv[p] += v, lazy[p] = v;
			return;
		} sync(p); int mid = (L + R) >> 1;
		if (l <= mid) add(l, r, v, L, mid, lp(p));
		if (r > mid) add(l, r, v, mid+1, R, rp(p));
		update(p);
	}
} tree;

int a[N], pre[N];
int main() {
	int n, k, d;
	cin >> n >> k >> d;

	map<int, int> pos;
	for (int x, i = 1; i <= n; ++i) {
		cin >> x; a[i] = (x % d + d) % d;
		pre[i] = pos[x]; pos[x] = i;
	}

	stack<int> maxlr, minlr;
	for (int i = 1; i <= n; ++i) {

	}
}
