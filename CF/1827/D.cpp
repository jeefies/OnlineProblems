#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 5e5 + 7;

vector<int> G[N];
vector<int> udfns[N];
int dfn[N], edfn[N], cdfn;

void dfs(int x) {
	dfn[x] = ++cdfn;
	for (int y : G[x]) {
		if (dfn[y]) continue;
		dfs(y);
		udfns[x].push_back(dfn[y]);
	}
	edfn[x] = cdfn;
}

inline bool isIn(int x, int y) {
	return dfn[y] <= dfn[x] && dfn[x] <= edfn[y];
}

int getCh(int x, int y) {
	auto it = lower_bound(begin(udfns[x]), end(udfns[x]), dfn[y]);
	if (it == end(udfns[x]) || *it > dfn[y]) --it;
	return G[x][it - begin(udfns[x])];
}

#define lowbit(i) ((i) & -(i))
int bit[N];
inline int query(int i) {
	int r = 0; for (; i; i -= lowbit(i)) r += bit[i];
	return r;
}

int gsize(int x) {
	return query(edfn[x]) - query(dfn[x] - 1);
}

int fa[N];
void work() {
	int n; cin >> n;

	cdfn = 0;
	for (int i = 1; i <= n; ++i) {
		G[i].clear(), udfns[i].clear();
		bit[i] = dfn[i] = 0;
	}

	for (int i = 2; i <= n; ++i) {
		cin >> fa[i]; G[fa[i]].push_back(i);
	} dfs(1);

	int cent = 1, mx = 0;
	for (int x = 2; x <= n; ++x) {
		for (int i = dfn[x]; i <= n; i += lowbit(i)) ++bit[i];

		if (isIn(x, cent)) {
			int s = getCh(cent, x);
			int siz = gsize(s);
			if (siz >= (x + 1) / 2)
				cent = s, mx = x / 2;
			else
				mx = max(mx, siz);
		} else {
			int siz = gsize(cent);
			if (siz < (x + 1) / 2)
				cent = fa[cent], mx = x / 2;
			else
				mx = max(mx, x - siz);
		}
		cout << x - 2 * mx << " \n"[x == n];
	}
}

int main(void) {
	cin.tie(nullptr)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
