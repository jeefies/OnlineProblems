import std.stdio;
import std.algorithm;

immutable int mod = cast(int)1e9 + 7;

int[] a, b;
void work() {
	int n; readf(" %s", n);

	a.length = b.length = n;
	for (int i = 0; i < n; ++i) {
		readf(" %s", a[i]);
	} a.sort;

	for (int i = 0; i < n; ++i) {
		readf(" %s", b[i]);
	} b.sort;

	long ans = 1;
	int ptr = 0;
	for (int i = 0; i < n; ++i) {
		while (ptr < n && b[ptr] < a[i]) ++ptr;
		ans = ans * (ptr - i) % mod;
	} writeln(ans);
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
