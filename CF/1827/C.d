import std.stdio, std.string, std.array;
import std.traits, std.conv, std.algorithm;
import std.math, std.regex;

int[] P, nxt, left, f;
char[] str;

int prepare(ref string s) {
	str.length = s.length * 2 + 3;

	int idx = 0;
	str[idx++] = '^';
	str[idx++] = '#';
	foreach (c; s) {
		str[idx++] = c;
		str[idx++] = '#';
	} str[idx++] = '$';
	return idx;
}

pragma(inline) int findNext(int i) {
	return i == nxt[i] ? i : (nxt[i] = findNext(nxt[i]));
}

void work() {
	int n; readf(" %s\n", n);
	string s = strip(readln());
	int len = prepare(s);

	P.length = f.length = left.length = nxt.length = len + 1;
	P[] = left[] = f[] = 0;

	int M = 0, R = 0;
	for (int i = 1; i < len; ++i) {
		int p = R > i ? min(R - i + 1, P[M * 2 - i]) : 1;
		while (i + p < len && str[i + p] == str[i - p]) ++p;
		if (i + p - 1 > R) M = i, R = i + p - 1;
		P[i] = p;
	}

	for (int i = 0; i <= n + 1; ++i) {
		nxt[i] = i;
	}

	for (int i = len - 2; i > 0; i -= 2) {
		int l = P[i] / 2, ids = (i + 1) / 2;
		for (int idx = ids; idx < ids + l; idx = findNext(idx + 1)) {
			left[idx] = ids * 2 - 1 - idx;
			nxt[idx] = nxt[idx + 1];
		}
	}

	long ans = 0;
	for (int i = 1; i <= n; ++i) {
		f[i] = (left[i] ? f[left[i] - 1] + 1 : 0);
		ans += f[i];
	}
	writeln(ans);
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
