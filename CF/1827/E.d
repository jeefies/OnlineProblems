import std.stdio, std.container;
import std.algorithm;

immutable int N = cast(int)5e5 + 10;

int LG, leafC, cdfn;
int[N] lg, fa, dep, dfn, edfn, high, isLeaf;
SList!int[N] G, R;

pragma(inline)
void dfs(int x, int p) {
	dfn[x] = ++cdfn;
	dep[x] = dep[fa[x] = p] + 1;
	isLeaf[x] = 0;
	foreach (y; G[x]) {
		++isLeaf[x];
		if (y == p)
			continue;
		dfs(y, x);
	} leafC += isLeaf[x] = isLeaf[x] == 1;
	edfn[x] = cdfn;
}

pragma(inline) bool isIn(int x, int y) {
	return dfn[y] <= dfn[x] && dfn[x] <= edfn[y];
}

int[][] st;
int[] pre = [1, 1 << 1, 1 << 2, 1 << 3, 1 << 4, 1 << 5, 1 << 6, 1 << 7, 1 << 8, 1 << 9, 1 << 10, 1 << 11, 1 << 12, 1 << 13, 1 << 14, 1 << 15, 1 << 16, 1 << 17, 1 << 18, 1 << 19];
pragma(inline) int Max(int x, int y) {
	return dep[x] < dep[y] ? x : y;
}

pragma(inline) void preST(int n) {
	int t = lg[n] + 1;
	for (int i = 1; i <= n; ++i)
		st[dfn[i]][0] = i;

	for (int k = 1; k <= t; ++k) {
		for (int i = 1; i + pre[k] - 1 <= n; ++i)
			st[i][k] = Max(st[i][k - 1], st[i + pre[k - 1]][k - 1]);
	}
}

pragma(inline) int LCA(int x, int y) {
	if (x == y) return x;
	if (dfn[x] > dfn[y]) swap(x, y);
	x = dfn[x] + 1, y = dfn[y];
	int t = lg[y - x + 1];
	return fa[Max(st[x][t], st[y - pre[t] + 1][t])];
}

pragma(inline)
void work() {
	int n, m; readf(" %d %d", n, m);
	leafC = 0, LG = lg[n];

	for (int i = 1; i <= n; ++i) {
		G[i].clear(); R[i].clear();
	}

	for (int u, v, i = 1; i < n; ++i) {
		readf(" %d %d", u, v);
		G[u].insert(v), G[v].insert(u);
	}
	
	cdfn = 0, dfs(1, 0); preST(n);

	for (int u, v, i = 1; i <= m; ++i) {
		readf(" %d %d", u, v);
		R[u].insert(v), R[v].insert(u);
	}

	int[] leafs; leafs.reserve(leafC);
	for (int i = 1; i <= n; ++i) {
		if (isLeaf[i]) {
			leafs ~= i;
			high[i] = i;

			foreach (j; R[i]) {
				int lca = LCA(i, j);
				if (dep[lca] < dep[high[i]]) high[i] = lca;
			}
		}
	}

	leafs.sort!((x, y) => dep[high[x]] > dep[high[y]]);
	for (int i = 1; i < leafC; ++i) {
		if (!isIn(high[leafs[i - 1]], high[leafs[i]])) {
		// if (LCA(high[leafs[i - 1]], high[leafs[i]]) != high[leafs[i]]) {
			writefln("NO\n%d %d", leafs[i - 1], leafs[i]);
			return ;
		}
	}

	int newRoot = high[leafs[0]];
	cdfn = 0, dfs(newRoot, 0); preST(n);

	foreach (leaf; leafs) {
		int top = leaf;
		foreach (x; R[leaf]) {
			int lca = LCA(leaf, x);
			if (dep[lca] < dep[top])
				top = lca;
		}

		if (top != newRoot) {
			writefln("NO\n%d %d", leaf, leafs[0]);
			return ;
		}
	}

	writeln("YES");
}

void main() {
	st = new int[][](N, 19);
	for (int i = 2; i < N; ++i)
		lg[i] = lg[i >> 1] + 1;

	int T; readf(" %s", T);
	while (T--) work();
}
