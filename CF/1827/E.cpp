#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int N = 5e5 + 10;

int LG;
int lg[N], dep[N], low[N];
vector<int> G[N], R[N];
int fa[N][19];

inline void dfs(int x, int p) {
	dep[x] = dep[p] + 1;
	fa[x][0] = p;
	for (int i = 1; i <= LG; ++i)
		fa[x][i] = fa[ fa[x][i - 1] ][i - 1];

	for (int y : G[x]) {
		if (y == p) continue;
		dfs(y, x);
	}
}

inline int LCA(int x, int y) {
	if (dep[x] < dep[y]) swap(x, y);

	while (dep[x] != dep[y])
		x = fa[x][lg[dep[x] - dep[y]]];
	if (x == y) return x;

	for (int k = lg[dep[x]]; ~k; --k) {
		if (fa[x][k] ^ fa[y][k])
			x = fa[x][k], y = fa[y][k];
	} return fa[x][0];
}

inline bool cmp(int x, int y) {
	return dep[low[x]] > dep[low[y]];
}

vector<int> leafs;
void work() {
	int n, m; cin >> n >> m;
	LG = lg[n];

	for (int i = 1; i <= n; ++i) {
		G[i].clear(), R[i].clear();
	}

	for (int u, v, i = 1; i < n; ++i) {
		cin >> u >> v;
		G[u].push_back(v), G[v].push_back(u);
	} dfs(1, 0);

	for (int u, v, i = 1; i <= m; ++i) {
		cin >> u >> v;
		R[u].push_back(v), R[v].push_back(u);
	}

	leafs.clear();
	for (int i = 1; i <= n; ++i) {
		if (G[i].size() == 1) {
			leafs.push_back(i);
			low[i] = i;
			for (int j : R[i]) {
				int lca = LCA(i, j);
				if (dep[lca] < dep[low[i]]) low[i] = lca;
			}
		}
	} sort(begin(leafs), end(leafs), cmp);

	int leafC = leafs.size();
	for (int prv, cur = low[leafs[0]], i = 1; i < leafC; ++i) {
		prv = cur, cur = low[leafs[i]];
		if (LCA(prv, cur) != cur) {
			cout << "NO\n" << leafs[i - 1] << ' ' << leafs[i] << '\n';
			return;
		}
	}

	int newRoot = low[leafs[0]];
	dfs(newRoot, 0);
	for (int leaf : leafs) {
		int top = leaf;
		for (int x : R[leaf]) {
			int lca = LCA(leaf, x);
			if (dep[lca] < dep[top])
				top = lca;
		}

		if (top != newRoot) {
			cout << "NO\n" << leaf << ' ' << leafs[0] << '\n';
			return;
		}
	}

	cout << "YES\n";
}

int main(void) {
	for (int i = 2; i < N; ++i)
		lg[i] = lg[i >> 1] + 1;

	cin.tie(nullptr)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
