#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 203, mod = 1e9 + 7;
typedef long long lint;
struct Point {
	int x, y;
	Point() {}
	Point(int x, int y) : x(x), y(y) {}
	inline lint operator * (const Point &p) {
		return (1ll * x * p.y - 1ll * y * p.x);
	}

	inline Point operator - (const Point &p) {
		return Point(x - p.x, y - p.y);
	}
} p[N];

lint dp[N][N];

int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n; cin >> n;
	for (int x, y, i = 1; i <= n; ++i) {
		cin >> x >> y;
		p[i] = Point(x, y);
	}

	lint clockwiser = 0;
	for (int i = 2; i < n; ++i) {
		clockwiser += (p[i] - p[1]) * (p[i + 1] - p[1]);
	}

	clog << "Clockwiser: " << clockwiser << '\n';
	if (clockwiser > 0) // if is positive, the it is counterclockwise
		clog << "Reversed\n",
		reverse(p + 1, p + 1 + n);

	// lint clockwiser = p[n] * p[1];
	// for (int i = 1; i < n; ++i) 
	// 	clockwiser += p[i] * p[i + 1];
	// if (clockwiser > 0) // if is positive, then it is counterclockwise, we need to reverse it.
	// 	clog << "Clockwiser: reversed!\n",
	// 	reverse(p + 1, p + 1 + n);

	for (int i = 1; i < n; ++i)
		dp[i][i + 1] = 1;

	for (int len = 2; len < n; ++len) {
		for (int l = 1, r = len + 1; r <= n; ++l, ++r) {
			for (int k = l; k <= r; ++k) {
				if ((p[r] - p[l]) * (p[k] - p[l]) > 0)
					dp[l][r] = (dp[l][r] + 1ll * dp[l][k] * dp[k][r] % mod) % mod;
			}
		}
	}

	cout << dp[1][n] << '\n';
}
