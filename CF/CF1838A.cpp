#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <algorithm>

using namespace std;
const int N = 200;

vector<int> nums;

bool findTwoSub(int t) {
	if (t < 0 || nums.size() < 2) 
		return false;

	int r = 1, e = nums.size();
	for (int l = 0; l < e; ++l) {
		while (r < e && nums[r] - nums[l] < t) ++r;
		if (nums[r] - nums[l] == t)
			return true;
	}
	return false;
}

void work(void) {
	nums.clear();

	int n; cin >> n;

	for (int x, i = 1; i <= n; ++i) {
		cin >> x; nums.push_back(x);
	} sort(nums.begin(), nums.end());

	cout << (nums[0] < 0 ? nums[0] : nums.back()) << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int T; cin >> T;
	while (T--) work();
}
