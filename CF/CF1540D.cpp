#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;
const int N = 1e5 + 3, BC = 400;
// const int N = 100, BLO = 20, BC = N / BLO + 1;

int n, m;
int LG, BLO;
#define lowbit(i) ((i) & -(i))
class BIT {
public:
	int b[N];
	inline void update(int i, int x) {
		for (++i; i <= n + 2; i += lowbit(i)) b[i] += x;
	}
	inline void clear(int i) {
		for (++i; i <= n + 2; i += lowbit(i)) b[i] = 0;
	}
	inline int query(int i) {
		int r = 0; ++i;
		for (; i; i -= lowbit(i)) r += b[i];
		return r;
	}
} bit[BC];
vector<int> modi[BC];

int b[N];
bool changed[BC];
inline void buildBlock(int i, int l, int r) {
	changed[i] = false;
	// clear first
	BIT &bt = bit[i];
	for (int j : modi[i]) {
		bt.clear(j);
	} modi[i].clear();
	
	for (int k = l; k <= r; ++k) {
		int whr = 0, sum = 0;
		for (int bk = b[k], w = 1 << LG; w; w >>= 1) {
			int nxt = whr + w;
			if (nxt < n && sum + bt.b[nxt] + nxt - 1 < bk)
				whr = nxt, sum += bt.b[nxt];
		}
		bt.update(whr, 1), modi[i].push_back(whr);
	}
}

#define AT(x) ((x-1)/BLO)
#define BL(i) ((i)*BLO+1)
#define BR(i) (((i)+1)*BLO)
inline int Q(int x) {
	int r = b[x], bend = BR(AT(x));
	if (bend >= n) {
		for (int i = x + 1; i <= n; ++i)
			r += (r >= b[i]);
		return n - r;
	}
	
	for (int i = x + 1; i <= bend; ++i)
		r += (r >= b[i]);

	for (int b = AT(x) + 1; BL(b) <= n; ++b) {
		if (changed[b]) buildBlock(b, BL(b), min(BR(b), n));
		r = r + bit[b].query(r);
	}
	return n - r;
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> b[i];
	LG = log2(n), BLO = pow(n, 0.5);

	// pre build
	for (int i = 0, bl = 1, br = BLO; bl <= n; ++i, bl += BLO, br += BLO) {
		buildBlock(i, bl, min(br, n));
	}
	
	cin >> m;
	int op, x, y;
	for (int i = 1; i <= m; ++i) {
		cin >> op >> x;
		if (op == 2) 
			cout << Q(x) << '\n';
		else
			cin >> y, b[x] = y, changed[AT(x)] = true;
	}
}
