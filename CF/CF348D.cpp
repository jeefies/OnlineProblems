#include <iostream>
#include <algorithm>

using namespace std;
const int N = 3003, mod = 1e9 + 7;
typedef long long lint;

int n, m;
char mp[N][N];
int f[4][N][N];

void read() {
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= n; ++i) {
		scanf("%s", mp[i] + 1);
	}
}

int F(int x, int y, int s, int t) {
	static int id = -1;
	auto g = f[++id];
	g[x][y] = 1;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j)
			if (mp[i][j] == '#') g[i][j] = 0;
			else g[i][j] += (g[i - 1][j] + g[i][j - 1]) % mod;
	}
	fprintf(stderr, "F (%d, %d) => (%d, %d) = %d\n", x, y, s, t, g[s][t]);
	return g[s][t];
}

int main(void) {
	read();

	// F(1, 2, n, m - 1)
	int ans = 1ll * F(1, 2, n - 1, m) * F(2, 1, n, m - 1) % mod - 1ll * F(1, 2, n, m - 1) * F(2, 1, n - 1, m) % mod;
	cout << ((ans % mod + mod) % mod) << '\n';
}
