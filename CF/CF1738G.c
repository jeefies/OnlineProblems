#include <stdio.h>
#include <string.h>

#define N 1002

int n, k;
char ways[N][N];
char result[N][N];

#define NOTREACHED 0
#define VISITED 1
#define CHOOSED 2
int state[N][N];

inline void findPath(int i, int j, int x, int y) {
	int up = j;
	for (int c = i; c <= x; ++c) {
		state[up][c] = CHOOSED;
		for (int r = up - 1; r >= y; --r) {
			if (state[r][c] != NOTREACHED) break;
			state[r][c] = VISITED;
			if (ways[r][c] == '0') {
				for (int k = up - 1; k >= r; --k)
					state[k][c] = CHOOSED;
				up = r, --c;
				break;
			}
		}
	}

	for (int k = up; k >= y; --k)
		state[k][x] = CHOOSED;
}

inline void work() {
	scanf("%d %d", &n, &k);
	int t = k - 1;

	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
			state[i][j] = NOTREACHED;

	for (int i = 1; i <= n; ++i)
		scanf("%s", ways[i] + 1);

	for (int i = 1; i <= t; ++i) {
		findPath(i, n - t + i, n - t + i, i);
	}

	int valid = 1;

	for (int i = 1; i <= n && valid; ++i) {
		for (int j = 1; j <= n && valid; ++j) {
			if (j - i >= n - t || i - j >= n - t) 
				state[j][i] = CHOOSED;
			if (ways[j][i] == '0' && state[j][i] != CHOOSED)
				valid = 0;
			result[j][i] = (state[j][i] == CHOOSED) + '0';
		} result[i][n + 1] = '\0';
	}

	if (!valid) {
		puts("NO");
		return;
	} puts("YES");
	for (int i = 1; i <= n; ++i)
		puts(result[i] + 1);
}

int main(void) {
	int T; scanf("%d", &T);
	while (T--) work();
}
