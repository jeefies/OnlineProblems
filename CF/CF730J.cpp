#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 102;

int a[N], b[N], tmp[N];
int f[N][N * N];
int main() {
	cin.tie(0)->sync_with_stdio(false);

	int n, tot = 0; cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> a[i], tot += a[i];
	for (int i = 1; i <= n; ++i)
		cin >> b[i], tmp[i] = b[i];

	sort(tmp + 1, tmp + 1 + n, [](const int a, const int b) { return a > b; });
	int sum = 0, k;
	for (k = 1; k <= n; ++k) {
		sum += tmp[k];
		if (sum >= tot) break;
	}

	int ans = 0;
	for (int i = 1; i <= n; ++i) {
		for (int u = tot; u >= b[i]; --u) {
			for (int j = 1; j <= k; ++j)
				f[j][u] = max(f[j][u], f[j - 1][u - b[i]] + a[i]);
		}
	}
}
