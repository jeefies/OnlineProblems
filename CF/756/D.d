import std.stdio, std.string;

immutable int N = cast(int)5e3 + 7, mod = cast(int)1e9 + 7;

char[N] s;

long qpow(long a, long x) {
	long r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

long[N] fac, ifac;
void prepare(int n) {
	for (int i = fac[0] = 1; i <= n; ++i)
		fac[i] = fac[i - 1] * i % mod;
	ifac[n] = qpow(fac[n], mod - 2);
	for (int i = n; i; --i)
		ifac[i - 1] = ifac[i] * i % mod;
}

long C(int n, int m) {
	return fac[n] * ifac[m] % mod * ifac[n - m] % mod;
}

void main() {
	int n; scanf("%d%s", &n, s.ptr);

	int[][] f = new int[][](n + 1, 26);
	int[] g = new int[](n + 1);

	prepare(n);
	for (int i = 0; i < n; ++i) {
		if (s[i] == s[i + 1]) continue;

		int x = cast(int)s[i] - 97;
		for (int l = i + 1; l > 1; --l) {
			g[l] -= 
				f[l][x];
			if (g[l] < 0) g[l] += mod;

			f[l][x] = g[l - 1] - f[l - 1][x];
			if (f[l][x] < 0) f[l][x] += mod;

			g[l] += f[l][x];
			if (g[l] >= mod) g[l] -= mod;
		}

		if (!f[1][x])
			f[1][x] = 1, ++g[1];
	}

	long ans = 0;
	for (int l = 1; l <= n; ++l) {
		ans = (ans + g[l] * C(n - 1, l - 1) % mod) % mod;
	}
	writeln(ans);
}
