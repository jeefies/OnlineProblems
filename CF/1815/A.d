import std.stdio;

immutable int N = cast(int)3e5 + 7;

long[N] arr;
void work() {
	int n; readf(" %s", n);
	for (int i = 0; i < n; ++i) {
		readf(" %s", arr[i]);
	}

	for (int i = 1; i + 1 < n; ++i) {
		long dif = arr[i] - arr[i - 1];
		arr[i] -= dif, arr[i + 1] -= dif;
	}

	if (arr[n - 1] >= arr[n - 2]) {
		writeln("YES");
	} else if ((n - 1) & 1) {
		writeln("NO");
	} else {
		writeln("YES");
	}
} 

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
