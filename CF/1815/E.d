import std.stdio;
import std.string;

immutable int N = cast(int)1e6 + 7, mod = 998244353;

long qpow(long a, int x) {
	long r = 1;
	for (; x; x >>= 1, a = a * a % mod)
		if (x & 1) r = r * a % mod;
	return r;
}

int[N] kmp;
string removeRepeat(string s) {
	uint len = cast(uint)s.length;
	kmp[0] = -1;
	for (int i = 0, j = -1; i < len;) {
		if (j == -1 || s[i] == s[j]) {
			kmp[++i] = ++j;
			// writefln("kmp[%d] = %d", i, j);
		} else j = kmp[j];
	}

	if (len % (len - cast(uint)kmp[len]) == 0) return s[0 .. cast(uint)(len - kmp[len])];
	return s;
}

int[N] a, b;
void calcAB(int id, ref string s) {
	int cur = 0, direct = 1, idx = 0, len = cast(int)s.length;
	do {
		if (cur == 0 && direct == 1) ++a[id];
		if (cur == 2 && direct == -1) ++b[id];
		cur += direct;

		if (cur == 1) {
			if (s[idx] == '1') direct = -direct;
			idx = (idx + 1) % len;
		} else if (cur == 0) direct = 1;
		else if (cur == 2) direct = -1;
	} while (idx != 0 || cur != 0);
}

immutable int M = cast(int)2e6 + 7;
int[M] lst, prm;
int ps = 0;
void getPrime(int n = cast(int)2e6) {
	for (uint i = 2; i <= n; ++i) {
		if (!lst[i])
			lst[i] = cast(int)i, prm[ps++] = cast(int)i;
		for (int j = 0; j < ps && prm[j] * i <= n; ++j) {
			lst[prm[j] * i] = prm[j];
			if (i % prm[j] == 0) break;
		}
	}
}

void main() {
	getPrime();

	int n; readf(" %s\n", n);

	long f0 = 1;
	int[M] exi;
	for (int i = 1; i <= n; ++i) {
		string cur = strip(readln());
		cur = removeRepeat(cur);
		calcAB(i, cur);

		int x = a[i];
		while (x > 1) {
			int p = lst[x];
			x /= p;
			if (!exi[p]) f0 = f0 * p % mod;
			else --exi[p];
		}

		x = b[i];
		if (!x) break;
		while (x > 1) {
			int p = lst[x];
			x /= p;
			++exi[p];
		}
	}

	long ans = 0;
	for (int i = 1; i <= n + 1; ++i) {
		ans = (ans + f0) % mod;
		if (!b[i]) break;
		if (i <= n) f0 = f0 * b[i] % mod * qpow(cast(long)a[i], mod - 2) % mod;
	}
	writeln(ans * 2 % mod);
}
