#include <iostream>
#include <assert.h>

using namespace std;
const int N = 1e6;

void work() {
	int n; cin >> n;

	int resp; 
	cout << "+ " << n + 1 << endl; cin >> resp;
	cout << "+ " << n + 2 << endl; cin >> resp;


	int mdis = -1, mdi;
	for (int i = 2; i <= n; ++i) {
		cout << "? 1 " << i << endl;
		int dis; cin >> dis;
		if (dis > mdis) mdis = dis, mdi = i;
	}

	// here p[mdi] should be 1 or n / 2 + 1
	static int save[N]; save[mdi] = 0;
	for (int i = 1; i <= n; ++i) {
		if (i == mdi) continue;
		cout << "? " << i << " " << mdi << endl;
		cin >> save[i];
	}

	static int perm[N]; int l = 1, r = n;
	for (int i = 1; i <= n; ++i) {
		perm[i] = (i & 1) ? l++ : r--;
	}

	cout << "!";
	// status 1, if mdi is 1
	for (int i = 1; i <= n; ++i) {
		cout << ' ' << perm[1 + save[i]];
	}

	// status 2, if mdi is n / 2 + 1;
	for (int i = 1; i <= n; ++i) {
		cout << ' ' << perm[n - save[i]];
	} cout << endl;

	cin >> resp;
}

int main(void) {
	int T; cin >> T;
	while (T--) work();
	return 0;
}
