#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;
const int N = 2e6 + 3;

int dep[N], res[N], q[N];
std::vector<int> G[1507];

void work() {
	int n, m; cin >> n >> m;

	fill_n(dep, n + 1, -1);
	for (int i = 1; i <= n; ++i) {
		G[i].clear();
	}

	for (int a, b, i = 0; i < m; ++i) {
		cin >> a >> b; G[b].push_back(a);
	}

	int head = 0, tail = 0;
	q[tail++] = dep[1] = 1;
	while (head ^ tail) {
		int x = q[head++];
		for (int y : G[x]) {
			if (dep[y] == -1) {
				dep[y] = dep[x] + 1;
				q[tail++] = y;
			}
		}
	}

	for (int i = 1; i <= n; ++i) {
		if (dep[i] == -1) {
			cout << "INFINITE" << '\n';
			return ;
		}
	}

	int idx = 0;
	reverse(q, q + tail);
	bool done = false;
	do {
		done = true;
		for (int i = 0; i < tail; ++i) {
			if (dep[q[i]] > 0) {
				res[idx++] = q[i];
				done &= !(--dep[q[i]]);
			}
		}
	} while (!done);

	cout << "FINITE\n" << idx << '\n';
	for (int i = 0; i < idx; ++i) {
		cout << res[i] << " \n"[i + 1 == idx];
	}
}

int main(void) {
	cin.tie(nullptr)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
