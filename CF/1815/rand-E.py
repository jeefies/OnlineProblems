from random import randint as rd

n = 1000000
with open("r.in", "w") as f:
    print(n, file = f)
    for i in range(n):
        print(rd(0, 1), file = f)
