import std.stdio;

immutable int mod = 998244353;

long[long] f, g;

long solve(long n) {
	if (g.get(n, 0)) {
		return f[n];
	}

	if (n & 1) {
		solve(n / 2);
		g[n] = g[n / 2];
		f[n] = (f[n / 2] * 2 + g[n]) % mod;
	} else {
		solve(n / 2), solve(n / 2 - 1);
		g[n] = g[n / 2] + g[n / 2 - 1];
		f[n] = (f[n / 2] + f[n / 2 - 1]) * 2 % mod;
	}

	return f[n];
}

void main() {
	f[0] = 0, g[0] = 1;
	
	int T; readf(" %s", T);
	while (T--) {
		long n, m; readf(" %s %s", n, m);

		if (m == 1) {
			writeln(n % mod);
		} else if (m == 2) {
			writeln(solve(n));
		} else if (n & 1) {
			writeln(((n / 2 + 1) % mod) * ((n / 2 + 1) % mod) % mod);
		} else {
			writeln(((n / 2) % mod) * ((n / 2 + 1) % mod) % mod);
		}
	}
}
