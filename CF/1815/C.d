import std.stdio;
import std.container : SList;

immutable int N = cast(int)2e6 + 3;

int[] dep;
int[N] res, q;
SList!int[1502] G;

void work() {
	int n, m; readf(" %s %s", n, m);
	dep.length = n + 1;
	dep[] = -1;

	for (int i = 1; i <= n; ++i) {
		G[i].clear();
	}

	for (int a, b, i = 0; i < m; ++i) {
		readf(" %s %s", a, b);
		G[b].insert(a);
	}

	int head = 0, tail = 0;
	dep[1] = q[tail++] = 1;
	while (head ^ tail) {
		int x = q[head++];
		foreach (y; G[x]) {
			if (dep[y] == -1) {
				dep[y] = dep[x] + 1;
				q[tail++] = y;
			}
		}
	}

	for (int i = 1; i <= n; ++i) {
		if (dep[i] == -1) {
			writeln("INFINITE");
			return ;
		}
	}

	int idx = 0;
	bool done;
	do {
		done = true;
		for (int i = tail - 1; ~i; --i) {
			if (dep[q[i]] > 0) {
				res[idx++] = q[i];
				done &= !(--dep[q[i]]);
			}
		}
	} while (!done);

	writeln("FINITE\n", idx);
	for (int i = 0; i < idx; ++i) {
		writef("%d ", res[i]);
	} writeln();
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
