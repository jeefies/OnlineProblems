#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	int v[4];
	for (int i = 0; i < 4; ++i) {
		sscanf(argv[i + 1], "%d", v + i);
	}

	char op;
	int x;
	while (true) {
		cin >> op;
		if (op == '!') break;
		cin >> x;
		int cnt = 0;
		for (int a, b, i = 1; i <= x; ++i) {
			cin >> a >> b;
			if (v[0] <= a && a <= v[1] && v[2] <= b && v[3] <= b) 
				++cnt;
		}
		cout << cnt << endl;
	}
}
