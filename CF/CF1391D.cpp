#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 1e6 + 3;

int n, m, sits;
char mp[5][N];
int dp[N][10];

int valid[8][8];
int cnter[8] = {0, 1, 1, 2, 1, 2, 2, 3};

const char *bs[] = {"000", "001", "010", "011", "100", "101", "110", "111"};

void prepare() {
	for (int s = 0; s < sits; ++s) {
		for (int t = 0; t < sits; ++t) {
#define check(s, t) (((cnter[s] + cnter[t]) & 1) == 1)
			if (n == 2) {
				valid[s][t] = check(s, t);
			} else {
				valid[s][t] = check(s >> 1, t >> 1) & check(s & 0b11, t & 0b11);
			}
			// printf("valid[%s][%s] = %d\n", bs[s], bs[t], valid[s][t]);
		}
	}
}

inline int diff(int i, int s) {
	return cnter[s ^ (mp[1][i]) ^ (mp[2][i] << 1) ^ (mp[3][i] << 2)];
}

void work() {
	memset(dp, 0x3F, sizeof(dp));
	for (int s = 0; s < sits; ++s) 
		dp[0][s] = 0;

	for (int i = 1; i <= m; ++i) { // enum row
		for (int s = 0; s < sits; ++s) { // enum cur status
			for (int t = 0; t < sits; ++t) { // enum prev status
				if (!valid[s][t]) continue;
				// printf("dp[%d][%s] = min(dp[%d][%s] + %d (diff(%d, %s))) = ", i, bs[s], i - 1, bs[t], diff(i, s), i, bs[s]);
				dp[i][s] = min(dp[i][s], dp[i - 1][t] + diff(i, s));
				// printf("%d\n", dp[i][s]);
			}
		}
	}
}

int main(void) {
	scanf("%d %d", &n, &m);

	if (n == 1)
		return puts("0"), 0;
	else if (n >= 4)
		return puts("-1"), 0;

	for (int i = 1; i <= n; ++i) {
		char *s = mp[i];
		scanf("%s", s + 1);
		for (int j = 1; j <= m; ++j)
			s[j] = (s[j] == '1' ? 1 : 0);
	}

	sits = (n == 2 ? 4 : 8);
	prepare();
	work();
	int ans = n * m;
	for (int s = 0; s < sits; ++s)
		ans = min(ans, dp[m][s]);
	printf("%d\n", ans);
}
