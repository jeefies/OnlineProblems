#include <iostream>

using namespace std;

#define discrete(x) (((x) < 0 ? -1 : ((x) == 0 ? 0 : 1)))

void work() {
	int n; cin >> n;
	
	long long sum = 0, cnt = 0, blk = 0;
	for (int x, i = 1; i <= n; ++i) {
		cin >> x; sum += x < 0 ? -x : x;
		x = discrete(x);
		if (blk * x >= 0) blk += x;
		else cnt += blk < 0, blk = x;
	}

	cout << sum << ' ' << cnt + (blk < 0) << '\n';
}

int main(void) {
	cin.tie(nullptr)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
