#include <iostream>
#include <vector>

using namespace std;
const int N = 2e5 + 7;

std::vector<int> G[N];
int mx[N], mxsuf[N], mn[N], mnsuf[N];

void work() {
	int n; cin >> n;

	int nx = 1;
	mn[1] = mnsuf[1] = 0;
	mx[1] = mxsuf[1] = 1;
	
	char op;
	for (int u, v, x, i = 1; i <= n; ++i) {
		cin >> op;
		if (op == '+') {
			cin >> v >> x; ++nx;
			mnsuf[nx] = min(mnsuf[v] + x, 0);
			mn[nx] = min(mnsuf[nx], mn[v]);

			mxsuf[nx] = max(mxsuf[v] + x, 0);
			mx[nx] = max(mxsuf[nx], mx[v]);
		} else {
			cin >> u >> v >> x; // here u is always 1
			cout << (mn[v] <= x && x <= mx[v] ? "YES" : "NO") << '\n';
		}
	}
}

int main(void) {
	cin.tie(nullptr)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
