#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 2e5 + 7;

vector<int> G[N];

int c[N];
void dfs(int x, int p) {
	c[x] = 0;
	for (int y : G[x]) {
		if (y ^ p) dfs(y, x), c[x] += c[y];
	}
	if (!c[x]) c[x] = 1;
}

void work() {
	int n; cin >> n;

	for (int i = 1; i <= n; ++i) G[i].clear();

	for (int u, v, i = 1; i < n; ++i) {
		cin >> u >> v;
		G[u].push_back(v), G[v].push_back(u);
	} dfs(1, 0);

	int q; cin >> q;
	for (int x, y, i = 1; i <= q; ++i) {
		cin >> x >> y;
		cout << 1ll * c[x] * c[y] << '\n';
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
