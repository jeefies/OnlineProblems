#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1e3;

int a[N];
void work() {
	int n; cin >> n;
	for (int i = 0; i < n; ++i)
		cin >> a[i];
	sort(a, a + n);

	int ans = 0;
	for (int l = 0, r = n - 1; l <= r; ++l, --r) {
		ans += a[r] - a[l];
	} cout << ans << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
