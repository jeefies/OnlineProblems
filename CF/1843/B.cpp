#include <iostream>
#include <cmath>

using namespace std;
const int N = 2e5 + 3;

int a[N];
long long f[N];
void work() {
	int n; cin >> n;

	long long sum = 0, tot = 0;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
		sum += 1ll * (a[i] > 0 ? a[i] : -a[i]);
	}

	bool flag = 0;
	for (int i = 1; i <= n; ++i) {
		if (a[i] < 0 && !flag)
			tot += (flag = 1);
		else if (a[i] > 0) flag = 0;
	}

	cout << sum << ' ' << tot << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
