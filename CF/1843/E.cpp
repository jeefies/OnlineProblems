#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e5 + 7;

int x[N];
vector<int> dots[N];

vector<int> pre(N);
inline bool check(int n, int k) {
	fill_n(pre.begin(), n + 1, 0);
	
	for (int i = 1; i <= k; ++i)
		++pre[x[i]];

	bool found = false;
	for (int i = 1; !found && i <= n; ++i) {
		pre[i] += pre[i - 1];
		for (int l : dots[i]) {
			if ((pre[i] - pre[l - 1]) * 2 > (i - l + 1))
				found = true;
		}
	}

	// clog << "Check in k " << k << (found ? " found" : " not found") << '\n';
	return found;
}

void work() {
	int n, m; cin >> n >> m;

	for (int i = 1; i <= n; ++i) {
		dots[i].clear();
	}

	for (int l, r, i = 1; i <= m; ++i) {
		cin >> l >> r;
		dots[r].push_back(l);
	}

	int q; cin >> q;
	for (int i = 1; i <= q; ++i) {
		cin >> x[i];
	}

	int time = 0;
	for (int w = 1 << 17; w; w >>= 1) {
		if (time + w <= q && !check(n, time + w)) time += w;
	}

	cout << (time == q ? -1 : time + 1) << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
	return 0;
}
