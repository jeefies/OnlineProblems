#include <iostream>

using namespace std;

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int T; cin >> T;
	while (T--) {
		long long n, ans = 0;
		for (cin >> n; n; n >>= 1)
			ans += n;
		cout << ans << '\n';
	}
}
