from random import randint as rd

with open("permutation.in", "w") as f:
    n = 200
    print(n, file = f)
    for i in range(n):
        print(rd(0, i), file = f, end = ' ')
    print(file = f)

    q = 200
    print(q, file = f)
    for i in range(q):
        op = rd(1, 2)
        if op == 1:
            i = rd(1, n)
            x = rd(1, i)
            print(op, i, x, file = f)
        else:
            print(op, rd(1, n), file = f)
