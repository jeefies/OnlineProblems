#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1002;

int f[N];
void work(void) {
	int n, m; cin >> n >> m;

	int cur = 0;
	for (int i = 2; i <= n; i += 2)
		f[i] = ++cur;
	for (int i = 1; i <= n; i += 2)
		f[i] = ++cur;

	for (int i = 1; i <= n; ++i) {
		int s = f[i] * m;
		for (int j = 0; j < m; ++j)
			cout << s + j << ' ';
		cout << '\n';
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int T; cin >> T;
	while (T--) work();
}
