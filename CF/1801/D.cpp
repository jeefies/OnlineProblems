#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <queue>

using namespace std;
const int N = 800 + 7;

typedef long long lint;
const lint INF = (lint)1e18;

lint n, m, p;
lint w[N];
lint dis[N][N];

lint uti[N], rem[N];

void work() {
	cin >> n >> m >> p;

	for (int i = 1; i <= n; ++i) {
		fill_n(dis[i] + 1, n, INF);
		dis[i][i] = 0;
		cin >> w[i];
	}

	for (lint u, v, c, i = 1; i <= m; ++i) {
		cin >> u >> v >> c;
		dis[u][v] = min(dis[u][v], (lint)c);
	}

	vector<lint> order(n);
	for (lint i = 0; i < n; ++i) {
		order[i] = i + 1;
	} sort(order.begin(), order.end(), [&](const lint x, const lint y) { return w[x] < w[y]; });

	for (int k = 1; k <= n; ++k)
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= n; ++j) {
				dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
		}
	}

	if (dis[1][n] == INF) {
		cout << "-1\n";
		return ;
	}

	fill_n(uti + 1, n, INF), fill_n(rem + 1, n, 0);
	uti[1] = 0, rem[1] = p;

	auto flush = [&](const lint x, const lint t, const lint r) {
		if (t < uti[x]) uti[x] = t, rem[x] = r;
		else if (t == uti[x] && r > rem[x]) rem[x] = r;
	};

	// use path 1->x, x->y to update path 1->y
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			int x = order[i], y = order[j];
			if (dis[x][y] == INF || x == y || uti[x] == INF) continue;

			lint t = uti[x], r = rem[x];
			if (r >= dis[x][y])
				flush(y, t, r - dis[x][y]);
			else {
				lint ned = dis[x][y] - r;
				lint perf = (ned + w[x] - 1) / w[x];
				r += perf * w[x];
				flush(y, t + perf, r - dis[x][y]);
			}
		}
	} cout << uti[n] << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
