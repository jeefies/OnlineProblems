#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 2e5 + 7;

int n;

namespace TREE {
	std::vector<int> G[N];
	int fa[N], dep[N], dfn[N], top[N], son[N], siz[N], cdfn = 0;

	void read() {
		for (int i = 2; i <= n; ++i) {
			cin >> fa[i];
			G[fa[i]].push_back(i);
		}
	}

	int workSD(int x) {
		siz[x] = 1, dep[x] = dep[fa[x]] + 1;
		for (int y : G[x]) {
			siz[x] += workSD(y);
			if (siz[son[x]] < siz[y]) son[x] = y;
		} return siz[x];
	}

	void workDT(int x, int p) {
		top[x] = p, dfn[x] = ++cdfn;
		if (son[x]) workDT(son[x], p);

		for (int y : G[x]) {
			if (y ^ son[x]) workDT(y, y);
		}
	}

	void init() {
		workSD(1);
		workDT(1, 1);
	}

	int getLCA(int x, int y) {
		while (top[x] != top[y]) {
			if (dep[top[x]] < dep[top[y]]) swap(x, y);
			x = fa[top[x]];
		} return dep[x] < dep[y] ? x : y;
	}

	struct P {
		int l, r;
	};

	void getArea(int x, int y, vector<P> &la, vector<P> &ra) {
		while (top[x] != top[y]) {
			if (dep[top[x]] > dep[top[y]]) { // jump x
				la.push({x,})
			}
		}
	}
};

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	cin >> n;

	TREE::read(), TREE::init();
}
