#include <iostream>
#include <set>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;
const int N = 2e5 + 7;

std::vector<int> vals[N];

void work() {
	int n; cin >> n;

	for (int i = 0; i < n; ++i) {
		vals[i].clear();
		int c; cin >> c;
		for (int x, j = 0; j < c; ++j) {
			cin >> x;
			if (vals[i].empty() || x > vals[i].back())
				vals[i].emplace_back(x);
		}
	}

	map<int, vector<int> > but;
	for (int i = 0; i < n; ++i) {
		for (auto x : vals[i]) {
			but[x].emplace_back(i);
		}
	}

	vector<int> f(n); // 表示当前值域下，的答案。

	int reach = 0; // 块最大值不超过当前值域下可以达到的最多。
	for (auto &p : but) {
		int v = p.first; // 当前考虑到的值

		int cur = 0;
		for (int i : p.second) { // 拥有这个值的序列。
			f[i] = max(f[i] + 1, reach + 1);
			if (v == vals[i].back())
				cur = max(cur, f[i]);
		}

		reach = max(reach, cur);
	}

	cout << reach << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
