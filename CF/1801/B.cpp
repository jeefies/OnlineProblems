#include <iostream>
#include <algorithm>
#include <set>
#include <cmath>

using namespace std;
const int N = 5e5 + 7;
typedef long long lint;

struct Tup {
	lint a, b;
	bool operator < (const Tup &t) {
		return (a ^ t.a) ? a < t.a : b < t.b;
	}
} tp[N];
lint suf[N];

void work() {
	int n; cin >> n;

	for (int a, b, i = 0; i < n; ++i) {
		cin >> a >> b;
		tp[i] = {a, b};
	} sort(tp, tp + n);

	suf[n] = -1e17;
	for (int i = n - 1; ~i; --i) {
		suf[i] = max(suf[i + 1], tp[i].b);
	}

	lint ans = 0x7FFFFFFF;
	set<lint> vals;
	for (int i = 0; i < n; ++i) {
		lint mx = suf[i + 1];
		lint v = tp[i].a;

		// 前面无论怎么选都不会更优秀。
		if (i + 1 < n && tp[i].a <= mx) {
			ans = min(ans, mx - v);
			vals.insert(tp[i].b);
			continue;
		}

		// 在前面寻找最接近
		// auto it = lower_bound(begin(vals), end(vals), v);
		auto it = vals.lower_bound(v);

		// 找第一个比他小的
		if (it != begin(vals)) {
			auto cit = std::prev(it);
			if (*cit > mx && abs(*cit - v) < abs(mx - v))
				mx = *cit;
		}

		// 找第一个比他大的(或者等于)
		if (it != end(vals)) {
			if (*it > mx && abs(*it - v) < abs(mx - v))
				mx = *it;
		}
		
		ans = min(ans, abs(mx - v));
		vals.insert(tp[i].b);
	}

	cout << ans << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int T; cin >> T;
	while (T--) work();
}

/*
1 5
1 9
9 1
9 2
9 3
19 4
*/
