#include <iostream>
#include <algorithm>

using namespace std;

void work() {
	int n, m; cin >> n >> m;

	cout << n * m << '\n';

	int start = 0;
	for (int i = 1; i <= n; ++i) {
		int val = start;
		for (int j = 1; j <= m; ++j) {
			cout << val << ' ';
			val += 2 + ((j & 1) ? -1 : 1);
		} cout << '\n';
		start += (i & 1) ? 2 : 510;
	}
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	int T; cin >> T;
	while (T--) work();
}
