#include <iostream>
#include <algorithm>

using namespace std;
const int N = 2e5 + 3;

void work() {
	int n; cin >> n;

	int id1, id2, idn;
	for (int x, i = 1; i <= n; ++i) {
		cin >> x;
		if (x == 1) id1 = i;
		else if (x == 2) id2 = i;
		else if (x == n) idn = i;
	}

	if (id1 > id2) 
		swap(id1, id2);
	// case 1.
	if (idn < id1 && idn < id2) 
		cout << idn << ' ' << id1 << '\n';
	// case 2.
	if (idn > id1 && idn > id2)
		cout << id2 << ' ' << idn << '\n';
	// case 3.
	if (id1 < idn && idn < id2)
		cout << idn << ' ' << idn << '\n';
}

int main() {
	// cin.tie(0)->sync_with_stdio(false);

	int T; cin >> T;
	while (T--) work();
}
