#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;
const int N = 600, INF = 0x7FFFFFF;

char s[N];
int dp[N][N];

int main() {
	int n; scanf("%d%s", &n, s + 1);

	for (int l = 0; l < n; ++l) {
		for (int i = 1; i + l <= n; ++i) {
			int j = i + l;
			int cur = INF;
			if (i == j) cur = 1;
			for (int k = i; k < j; ++k)
				cur = min(cur, dp[i][k] + dp[k + 1][j] - (s[k] == s[j]));
			dp[i][j] = cur;
		}
	}

	printf("%d\n", dp[1][n]);
}
