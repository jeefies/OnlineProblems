import std.stdio;
import std.algorithm;

int[3000][3000] f;

void work() {
	int n, k, g;
	readf(" %s %s %s", n, k, g);

	f[0][] = -0x3F3F3F3F;
	f[0][0] = 0;
	for (int x = 1; x <= n; ++x) {
		stderr.writeln("At person ", x);
		f[x][] = -0x3F3F3F3F;
		for (int i = 0; i <= k * g; ++i) {
			for (int j = 0; j <= i; ++j) {
				f[x][i] = max(f[x][i], f[x - 1][i - j] + ((j % g < (g + 1) / 2) ? j % g : j % g - g));
			}
			stderr.writef("%d:%d ", i, f[x][i]);
		} stderr.writeln();
	} writeln(f[n][k * g]);
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
