import std.stdio;

int[102] but;

void work() {
	but[] = 0;
	int n; readf(" %s", n);
	for (int x, i = 0; i < n; ++i) {
		readf(" %s", x);
		++but[x];
	}

	bool valid = true;
	for (int i = 1; i <= 100; ++i) {
		if (but[i] > but[i - 1]) valid = false;
	}

	writeln(valid ? "YES" : "NO");
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
