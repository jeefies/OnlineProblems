import std.stdio;
import std.algorithm;

immutable int N = cast(int)(1e7 + 1);

int[10] pow;
void prepare() {
	pow[0] = 1;
	for (int i = 1; i < 10; ++i)
		pow[i] = pow[i - 1] * 10;
}

void work() {
	int a, b, c;
	long k;

	readf(" %s %s %s %s", a, b, c, k);

	int al = pow[a - 1], ar = pow[a];
	for (int i = al; i < ar; ++i) {
		int bl = max(pow[c - 1] - i, pow[b - 1]);
		int br = min(pow[c] - 1 - i, pow[b] - 1);
		if (bl > br) continue;

		// writeln("At i ", i, " bl, br = ", bl, " " , br);

		int len = br - bl + 1;
		if (k > len) k -= len;
		else {
			writeln(i, " + ", bl + k - 1, " = ", i + bl + k - 1);
			return ;
		}
	}
	writeln("-1");
}

void main() {
	int T; readf(" %s", T);
	prepare();
	while (T--) work();
}
