import std.stdio;
import std.algorithm;

void work() {
	long n, k, g;
	readf(" %s %s %s", n, k, g);

	long m = (g + 1) / 2 - 1;
	if (k * g <= n * m) {
		writeln(k * g);
		return;
	}

	long kg = k * g - n * m;
	long loop = m + g / 2 + 1;
	writeln((kg - 1 + loop) % loop + n * m - loop + 1);
}

void main() {
	int T; readf(" %s", T);
	while (T--) work();
}
