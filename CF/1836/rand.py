from random import randint

with open("t.in", "w") as f:
    print(10, file = f)
    for i in range(10):
        print(randint(1, 50), randint(1, 50), randint(1, 50), file = f)
