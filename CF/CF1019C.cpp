#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 1e6 + 7;

int n, m;

int vis[N], chs[N];
vector<int> G[N];

int main(void) {
	cin.tie(0)->sync_with_stdio(false);

	cin >> n >> m;
	for (int u, v, i = 1; i <= m; ++i) {
		cin >> u >> v;
		G[u].push_back(v);
	}

	for (int i = 1; i <= n; ++i) {
		if (!vis[i]) {
			vis[i] = chs[i] = true;
			for (int j : G[i])
				vis[j] = true;
		}
	}

	for (int i = n; i; --i) {
		if (chs[i]) {
			for (int j : G[i])
				chs[j] = false;
		}
	}

	std::vector<int> ans; ans.reserve(n);
	for (int i = 1; i <= n; ++i) {
		if (chs[i]) ans.push_back(i);
	}

	cout << ans.size() << '\n';
	for (auto x : ans) {
		cout << x << ' ';
	}

	return 0;
}
