#include <iostream>
#include <algorithm>

using namespace std;
const int N = 2e5 + 3;

int n, a[N];
long long pre[N];

inline long long calc(long long i, long long t) {
	long long mst = (a[n] + t) * (pre[i] + (i) * t) +
		(a[1] + t) * (pre[n - 1] - pre[i] + (n - i - 1) * t) -
		t * t * (n - 1);
	return mst;
}

void work() {
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> a[i];
	} sort(a + 1, a + 1 + n);

	for (int i = 1; i <= n; ++i) {
		pre[i] = pre[i - 1] + a[i];
	}

	if (a[1] * (n - 2) + pre[n] > 0 || a[n] * (n - 2) + pre[n] < 0) {
		cout << "INF\n";
		return;
	}

	long long ans = -1e18;
	for (int i = 1; i <= n; ++i) {
		ans = max(ans, calc(i, -a[i]));
	}

	// printf("%lld\n", ans);
	cout << ans << '\n';
}


int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
