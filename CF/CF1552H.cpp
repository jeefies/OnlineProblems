#include <iostream>

using namespace std;

int S, f[10];
int ask(int k) {
	if (~f[k]) return f[k];

	// seperate 2^k
	int sep = 1 << k;

	int line = 0;
	for (int i = 1; i <= 200; i += sep) ++line;
	cout << "? " << line * 200 << '\n';

	for (int i = 1; i <= 200; i += sep) {
		for (int j = 1; j <= 200; ++j)
			cout << i << ' ' << j << ' ';
	} cout << '\n' << flush;
	clog << line * 200 << '\n';

	int res; cin >> res;
	return f[k] = res;
}

int main(void) {
	for (int i = 0; i < 10; ++i) f[i] = -1;
	f[8] = 0;

	S = f[0] = ask(0);
	int lst = 8, l = 1, r = 7;
	while (l <= r) {
		int mid = (l + r) >> 1;
		if (ask(mid) * (1 << mid) == S) {
			l = mid + 1;
		} else r = mid - 1, lst = mid;
	}

	// int w = abs((S >> (lst - 1)) - (f[lst] << 1));
	int w = abs(f[lst - 1] - 2 * f[lst]);
	int h = S / w;
	cout << "! " << (w + h) * 2 - 4 << '\n';
	cout << flush;
	return 0;
}
