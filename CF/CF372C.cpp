#include <iostream>
#include <stack>
#include <deque>
#include <algorithm>
#include <cmath>

using namespace std;
const int N = 16e4, M = 4e2;
typedef long long lint;

lint tmp[2][N], *g = tmp[0], *f = tmp[1];
int a[M], b[M], t[M];

int main() {
	// freopen("2.log", "w", stdout);
	cin.tie(0)->sync_with_stdio(false);

	int n, m, d;
	cin >> n >> m >> d;
	for (int i = 1; i <= m; ++i)
		cin >> a[i] >> b[i] >> t[i];

	for (int i = 1; i <= m; ++i) {
		swap(f, g);
		lint p = min((lint)n, 1ll * d * (t[i] - t[i - 1])); // [x - p, x + p];
		// cout << ":: " << p << '\n';
		deque<int> xs;
		// prepare xs
		for (int x = 1; x <= p && x <= n; ++x) { // push x
			while (xs.size() && g[x] > g[xs.back()]) /* cout << "p pop " << xs.back() << ' ', */ xs.pop_back();
			xs.push_back(x);
			/* cout << "p in " << x << '\n'; */
		}
		for (int x = 1; x <= n; ++x) {
			while (xs.size() && xs.front() < x - p) xs.pop_front();
			if (x + p <= n) {
				while (xs.size() && g[x + p] > g[xs.back()]) /*cout << "i pop " << xs.back() << ' ' , */ xs.pop_back();
				xs.push_back(x + p); // cout << "i in " << x + p << "\n";
			} f[x] = g[xs.front()];
		}
		// for (int x = 1; x <= n; ++x) cout << f[i] << (x == n ? '\n' : ' ');
		for (int cb = b[i], ca = a[i], x = 1; x <= n; ++x) {
			f[x] = f[x] + cb - abs(ca - x);
		}
		// for (int x = 1; x <= n; ++x) cout << f[x] << (x == n ? '\n' : ' ');
	}

	lint ans = -1e18;
	for (int x = 1; x <= n; ++x) ans = max(ans, f[x]);
	cout << ans << '\n';
}
