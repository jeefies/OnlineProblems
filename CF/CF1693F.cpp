#include <iostream>
#include <algorithm>

using namespace std;
const int N = 3e5 + 3;

char str[N];
int hei[N];
int lst[N + N], nxt[N];

void work() {
	int n; cin >> n >> (str + 1);

	hei[0] = 0;
	bool ordered = true;
	for (int i = 1; i <= n; ++i) {
		hei[i] = hei[i - 1] + (str[i] == '0' ? -1 : 1);
		ordered &= str[i] >= str[i - 1];
		// printf("hei[%d] = %d\n", i, hei[i]);
	}

	if (ordered) {
		cout << 0 << '\n';
		return;
	}

	if (hei[n] > 0) 
		reverse(hei, hei + 1 + n);

	for (int i = 0; i <= n; ++i)
		lst[hei[i] + N] = i;
	for (int i = 0; i <= n; ++i)
		nxt[i] = lst[hei[i] + N];

	int r = 1, cnt = 0;
	// printf("HEI[n] = %d\n", hei[n]);
	for (int l = 0; l <= n; ++l) {
		// printf("Hei[%d] = %d\n", l, hei[l]);
		if (hei[l] == hei[n]) {
			bool inorder = true;
			for (int i = l; i < n; ++i)
				inorder &= (hei[i + 1] - hei[i] >= hei[i] - hei[i - 1]);
			cnt += !inorder; break;
		}

		if (hei[l] > hei[l + 1]) {
			continue;
		}

		int to = lst[hei[l] + N];
		// printf("To %d\n", to);
		// make l and nxt[l] in order
		if (l != to) {
			++cnt;
			int mid = (l + to) >> 1;
			// [l, mid] down, [mid, nxt[l]] up.
			for (int i = l; i < mid; ++i)
				hei[i + 1] = hei[i] - 1;
			for (int i = mid; i < to; ++i)
				hei[i + 1] = hei[i] + 1;
		} else {
			fprintf(stderr, "Error ???");
			exit(1);
		}
	}

	cout << cnt << '\n';
}

int main(void) {
	cin.tie(0)->sync_with_stdio(false);
	int T; cin >> T;
	while (T--) work();
}
