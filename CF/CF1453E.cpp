#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int N = 3e5, INF = 0x7FFFFFFF;

vector<int> G[N];
int dis[N]; // nearest leaf
int ans = 0;
void dfs(int x, int p) {
	int ch = 0, mn = INF, mx = 0, sec = 0;
	for (int y : G[x]) {
		if (y == p) continue;
		++ch, dfs(y, x);
		mn = min(mn, dis[y] + 1);
		if (dis[y] >= mx) sec = mx, mx = dis[y];
		else if (dis[y] > sec) sec = dis[y];
	}
	if (!ch) mn = 0;
	dis[x] = mn;
	if (x == 1) {
		ans = max(ans, mx + 1);
		if (ch > 1) ans = max(ans, sec + 2);
	} else if (ch > 1) ans = max(ans, mx + 2);
}


void work() {
	int n; cin >> n;
	ans = 0;
	for (int i = 1; i <= n; ++i) G[i].clear();
	for (int x, y, i = 1; i < n; ++i) {
		cin >> x >> y; G[x].push_back(y), G[y].push_back(x);
	} dfs(1, 0);
	cout << ans << '\n';
}

int main() {
	cin.tie(0)->sync_with_stdio(false);
	
	int T; cin >> T;
	while (T--) work();
}
